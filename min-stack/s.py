
# Definition for singly-linked list.
class ListNode:
    def __init__(self, x):
        self.val = x
        self.next = None

# Definition for a binary tree node.
class TreeNode:
    def __init__(self, val=0, left=None, right=None):
        self.val = val
        self.left = left
        self.right = right

import bisect
from typing import *


class MinStack:

    def __init__(self):
        """
        initialize your data structure here.
        """
        import math
        self.data = []
        self.sorted = []
        

    def push(self, x: int) -> None:
        self.data.append(x)
        bisect.insort(self.sorted, x)
        

    def pop(self) -> None:
        x = self.data.pop()
        index = bisect.bisect(self.sorted, x)-1
        self.sorted.pop(index)
        

    def top(self) -> int:
        return self.data[-1]
        

    def getMin(self) -> int:
        return self.sorted[0]
        
        


# Your MinStack object will be instantiated and called as such:
# obj = MinStack()
# obj.push(x)
# obj.pop()
# param_3 = obj.top()
# param_4 = obj.getMin()
        pass
