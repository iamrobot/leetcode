def get_all_palindrome(s: str):
    '''this algorithm uses O(N**2) time, there exists O(N) and O(NlogN) solution,
    check here: https://leetcode.com/problems/palindromic-substrings/solution/
    '''
    pals = set()
    n = len(s)
    for i,char in enumerate(s):
        p = i+1
        pals.add((i,i))

        j = 1
        while i-j>=0 and i+j<n:
            if s[i-j]==s[i+j]:
                pals.add((i-j,i+j))
            else:
                break
            j += 1

        if p<n and char==s[p]:
            pals.add((i,p))
            j = 1
            while i-j>=0 and p+j<n:
                if s[i-j]==s[p+j]:
                    pals.add((i-j, p+j))
                else:
                    break
                j += 1
    return pals

if __name__=='__main__':
    s = "juchzcedhfesefhdeczhcujzzvbmoeombv"
    r = get_all_palindrome(s)
    print(r)
    print('-'*30)

