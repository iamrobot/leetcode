
from collections import defaultdict


class TrieNode(object):
    def __init__(self):
        self.children = defaultdict(lambda:TrieNode())

class Trie(object):
    def __init__(self):
        self.root = TrieNode()

    def insert(self, word):
        cur_node = self.root
        for char in word:
            x = cur_node.children[char] 
            cur_node = x
