from collections import defaultdict

class UnionFindNode(object):
    def __init__(self, val):
        self.parent = self
        self.val = val
        self.size = 1

class UnionFindForest(object):
    def __init__(self, vals=[]):
        self.vals = set()
        self.roots = set()
        self.val_to_node_map = dict()
        for val in vals:
            self.add(val)

    def add(self, val):
        if val not in self.vals:
            self.vals.add(val)
            new_tree = UnionFindNode(val)
            self.val_to_node_map[val] = new_tree
            self.roots.add(new_tree)
        return self.val_to_node_map[val]

    def find(self, val):
        node = self.val_to_node_map[val]
        nodes_on_path = []
        while node.parent!=node:
            nodes_on_path.append(node)
            node = node.parent
        root = node
        for node in nodes_on_path:
            node.parent = root
        return root

    def union(self, val1, val2):
        tree1 = self.find(val1)
        tree2 = self.find(val2)
        if tree1==tree2:
            return

        if tree1.size>tree2.size:
            child, parent = tree2, tree1
        else:
            child, parent = tree1, tree2
        child.parent = parent
        parent.size = parent.size + child.size
        self.roots.discard(child)

    def print(self):
        trees = defaultdict(lambda:[])
        for val in self.vals:
            root = self.find(val)
            trees[root].append(val)

        for root in trees:
            tree = trees[root]
            print(f'{root.val}  -> {[x for x in tree]}')
