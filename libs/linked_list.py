#!/usr/bin/env python

import random

# Definition for singly-linked list.
class ListNode:
    def __init__(self, val=None):
        self.val = val
        self.next = None

    def __str__(self):
        s = ''
        p = self
        vals = []
        while p:
            vals.append(p.val)
            p = p.next
        vals = [str(val) for val in vals]
        s = '-> '.join(vals)
        return s

    @staticmethod
    def generate(start=0, end=10, reverse=False, shuffle=True):
        l = list(range(start,end))
        if shuffle:
            random.shuffle(l)
        if reverse:
            l = l[::-1]

        nodes = [ListNode(val) for val in l]
        for i in range(len(nodes)-1):
            nodes[i].next = nodes[i+1]
        return nodes[0]

if __name__=='__main__':
    pass
