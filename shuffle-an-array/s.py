
# load local leetcode lib
import pathlib
lc_lib_path = pathlib.Path('../lc_lib.py')
if lc_lib_path.exists():
    import sys
    sys.path.insert(0, '..')
    from lc_lib import *

old_print = print_line
def print(*args, **kwargs):
    if lc_lib_path.exists():
        old_print(*args, **kwargs)

import copy
import random
class Solution:

    def __init__(self, nums: List[int]):
        self.nums = nums
        self.original = copy.deepcopy(nums)
        

    def reset(self) -> List[int]:
        """
        Resets the array to its original configuration and return it.
        """
        self.nums = copy.deepcopy(self.original)
        self.original = copy.deepcopy(self.original)
        return self.nums
        

    def shuffle(self) -> List[int]:
        """
        Returns a random shuffling of the array.
        """
        for i in range(len(self.nums)):
            swap_i = random.randrange(i, len(self.nums))
            self.nums[i], self.nums[swap_i] = self.nums[swap_i], self.nums[i]
        return self.nums
        


# Your Solution object will be instantiated and called as such:
# obj = Solution(nums)
# param_1 = obj.reset()
# param_2 = obj.shuffle()
        pass

if __name__=='__main__':
    if lc_lib_path.exists():
        nums = [1,2,3,4]
        sol = Solution(nums)
        r = sol.shuffle()
        print(r)
        r = sol.reset()
        print(r)


        from pprint import pprint
        import collections
        c = collections.Counter()
        nums = [1,2,3,4]
        nums = [str(x) for x in nums]
        sol = Solution(nums)

        for i in range(100000):
            r = sol.shuffle()
            r = ''.join(r)
            c.update({r:1})

        pprint(c.most_common(100)) 
