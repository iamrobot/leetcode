
# load local leetcode lib
import pathlib

lc_lib_path = pathlib.Path('../lc_lib.py')
if lc_lib_path.exists():
    import sys
    sys.path.insert(0, '..')
    from lc_lib import *

import math
from collections import defaultdict
from pprint import pprint


class Solution:
    def partitionLabels(self, S: str) -> List[int]:
        d = defaultdict(lambda:(math.inf, -math.inf))
        for index,char in enumerate(S):
            a,b = d[char]
            d[char] = (min(a,index), max(b,index))
        char_start_end_indexes = d.values()

        result_l = []
        part_start = part_end = 0
        for char_start,char_end in char_start_end_indexes:
            if char_start<=part_end:
                part_end = max(part_end, char_end)
            else:
                result_l.append(part_end-part_start+1) 
                part_start = char_start
                part_end = char_end
        result_l.append(part_end-part_start+1) 
        return result_l

if __name__=='__main__':
    if lc_lib_path.exists():
        s = Solution()
        x = "ababcbacadefegdehijhklij"
        print(s.partitionLabels(x))
