
# load local leetcode lib
import pathlib
lc_lib_path = pathlib.Path('../lc_lib.py')
if lc_lib_path.exists():
    import sys
    sys.path.insert(0, '..')
    from lc_lib import *

old_print = print
def print(*args, **kwargs):
    if lc_lib_path.exists():
        old_print(*args, **kwargs)

class Solution:
    def fourSumCount(self, A: List[int], B: List[int], C: List[int], D: List[int]) -> int:
        import collections
        import itertools
        n = len(A)
        def get_sum_map(A,B):
            sum_map = collections.defaultdict(lambda:[])
            for i,j in itertools.product(range(n), range(n)):
                a = A[i]
                b = B[j]
                sum_map[a+b].append((i,j))
            return sum_map
        
        sum_map_ab = get_sum_map(A,B)
        sum_map_cd = get_sum_map(C,D)
        results = 0
        for sab,lab in sum_map_ab.items():
            if -sab in sum_map_cd:
                lcd = sum_map_cd[-sab]
                results += len(lab)*len(lcd)

        return results

if __name__=='__main__':
    if lc_lib_path.exists():
        sol = Solution()

        A = [1,2]
        B = [-2,-1]
        C = [-1,2]
        D = [0,2]
        r = sol.fourSumCount(A,B,C,D)
        print(r)
        print('-'*30)

