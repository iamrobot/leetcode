import string


class Solution:
    def detectCapitalUse(self, word: str) -> bool:
        s = word
        if len(s)==1:
            return True

        first_two_upper = s[0] in string.ascii_uppercase and s[1] in string.ascii_uppercase
        lowercase_seen = False
        
        for char in s:
            if char in string.ascii_lowercase:
                if first_two_upper:
                    return False
                lowercase_seen = True
            if lowercase_seen and char in string.ascii_uppercase:
                return False
        
        return True
