
# load local leetcode lib
import pathlib
lc_lib_path = pathlib.Path('../lc_lib.py')
if lc_lib_path.exists():
    import sys
    sys.path.insert(0, '..')
    from lc_lib import *

old_print = print
def print(*args, **kwargs):
    if lc_lib_path.exists():
        old_print(*args, **kwargs)

class Solution:
    def isValidSudoku(self, board: List[List[str]]) -> bool:
        allowd = set(range(1,10))
        allowd = set([str(x) for x in allowd])
        n = 9

        for i in range(n):
            seen = set()
            for j in range(n):
                item = board[i][j]
                if item=='.':
                    continue
                if item not in allowd:
                    return False
                if item in seen:
                    return False
                seen.add(item)

        for j in range(n):
            seen = set()
            for i in range(n):
                item = board[i][j] 
                if item=='.':
                    continue
                if item not in allowd:
                    return False
                if item in seen:
                    return False
                seen.add(item)

        for i in range(3):
            for j in range(3):
                seen = set()
                start_i = 3*i
                start_j = 3*j
                for x in range(3):
                    for y in range(3):
                        item = board[start_i+x][start_j+y] 
                        if item=='.':
                            continue
                        if item not in allowd:
                            return False
                        if item in seen:
                            return False
                        seen.add(item)

        return True
if __name__=='__main__':
    if lc_lib_path.exists():
        s = Solution()

        board = [["8","3",".",".","7",".",".",".","."],["6",".",".","1","9","5",".",".","."],[".","9","8",".",".",".",".","6","."],["8",".",".",".","6",".",".",".","3"],["4",".",".","8",".","3",".",".","1"],["7",".",".",".","2",".",".",".","6"],[".","6",".",".",".",".","2","8","."],[".",".",".","4","1","9",".",".","5"],[".",".",".",".","8",".",".","7","9"]]
        pprint(board)
        r = s.isValidSudoku(board)
        print(r)
        print('-'*30)

