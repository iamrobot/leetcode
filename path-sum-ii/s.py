
# load local leetcode lib
import pathlib

lc_lib_path = pathlib.Path('../lc_lib.py')
if lc_lib_path.exists():
    import sys
    sys.path.insert(0, '..')
    from lc_lib import *

# Definition for a binary tree node.
# class TreeNode:
#     def __init__(self, val=0, left=None, right=None):
#         self.val = val
#         self.left = left
#         self.right = right
class Solution:
    def pathSum(self, root: TreeNode, sum: int) -> List[List[int]]:
        def dfs(root, sum_before_root):
            sum_till_root = root.val + sum_before_root
            if root.left==None and root.right==None:
                if sum_till_root==sum:
                    yield [root.val]
            else:
                if root.left:
                    for path in dfs(root.left, sum_till_root):
                        yield [root.val, *path]
                if root.right:
                    for path in dfs(root.right, sum_till_root):
                        yield [root.val, *path]
        if not root:
            return []

        l = list(dfs(root, 0))
        return l

if __name__=='__main__':
    if lc_lib_path.exists():
        s = Solution()
        x = None # input
        r = s.method(x)
        print(r)
