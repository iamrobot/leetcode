
# load local leetcode lib
import pathlib
lc_lib_path = pathlib.Path('../lc_lib.py')
if lc_lib_path.exists():
    import sys
    sys.path.insert(0, '..')
    from lc_lib import *

old_print = print
def print(*args, **kwargs):
    if lc_lib_path.exists():
        old_print(*args, **kwargs)

class Solution:
    def longestCommonPrefix(self, strs: List[str]) -> str:
        min_len = min([len(s) for s in strs])

        i = 0
        while i < min_len:
            chars = set([s[i] for s in strs])
            if len(chars)>1:
                break
            i += 1
        return strs[0][:i]


if __name__=='__main__':
    if lc_lib_path.exists():
        sol = Solution()

        strs = ["flower","flow","flight"]
        r = sol.longestCommonPrefix(strs)
        print(r)
        print('-'*30)

