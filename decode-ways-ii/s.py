# load local leetcode lib
import pathlib
lc_lib_path = pathlib.Path('../lc_lib.py')
if lc_lib_path.exists():
    import sys
    sys.path.insert(0, '..')
    from lc_lib import *

old_print = print
def print(*args, **kwargs):
    if lc_lib_path.exists():
        old_print(*args, **kwargs)

import functools
def to_str_set(iterator):
    return set([str(i) for i in iterator])

digit = to_str_set(range(1,10))
double_digit = to_str_set(range(10,27))

class Solution:
    def numDecodings(self, s: str) -> int:
        return self.helper(s) % (10**9+7)

    @functools.cache
    def helper(self, s: str) -> int:
        if len(s)==0:
            r = 1
        
        elif len(s)==1:
            if s[0] in digit:
                r = 1
            elif s[0] == '*':
                r = 9
            else:
                r = 0
        
        else:
            r = 0

            if s[:2] in double_digit:
                r += self.numDecodings(s[2:])
            elif s[0] in digit and s[1] in digit:
                r += self.numDecodings(s[1:])
            elif s[0] == '*':
                for i in digit:
                    r += self.helper(f'{i}{s[1:]}')
            elif s[:2] in ['1*', '2*']:
                for i in digit:
                    r += self.helper(f'{s[0]}{i}{s[2:]}')

        print(f's:{s}, r:{r}')
        return r

if __name__=='__main__':
    if lc_lib_path.exists():
        sol = Solution()

        s = "11106"
        r = sol.numDecodings(s)
        print(r)
        print('-'*30)

        s = "*1"
        r = sol.numDecodings(s)
        print(r)
        print('-'*30)

        s = "1*"
        r = sol.numDecodings(s)
        print(r)
        print('-'*30)


        s = "2*"
        r = sol.numDecodings(s)
        print(r)
        print('-'*30)


