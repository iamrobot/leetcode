
# load local leetcode lib
import pathlib

lc_lib_path = pathlib.Path('../lc_lib.py')
if lc_lib_path.exists():
    import sys
    sys.path.insert(0, '..')
    from lc_lib import *

from collections import defaultdict
from pprint import pprint


class Solution:
    def jump(self, nums: List[int]) -> int:
        '''
        '''
        if len(nums)==1:
            return 0

        i = 0
        steps = 0
        while i<len(nums):
            next_step = 0
            next_jump_max_reach = 0
            jump = nums[i]+1
            if i+jump>=len(nums):
                return steps+1

            for k in range(1, nums[i]+1):
                j = i+k
                j_reach = j+nums[j]
                if j_reach>=next_jump_max_reach:
                    next_step = j
                    next_jump_max_reach = j_reach

            steps += 1
            i = next_step

if __name__=='__main__':
    if lc_lib_path.exists():
        s = Solution()
        x = [2,3,1,1,4]
        r = s.jump(x)
        print(r)
        x = [1,1,1,1,1]
        r = s.jump(x)
        print(r)
        x = [2,1]
        r = s.jump(x)
        print(r)
