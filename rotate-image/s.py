
# load local leetcode lib
import pathlib
lc_lib_path = pathlib.Path('../lc_lib.py')
if lc_lib_path.exists():
    import sys
    sys.path.insert(0, '..')
    from lc_lib import *

old_print = print
def print(*args, **kwargs):
    if lc_lib_path.exists():
        old_print(*args, **kwargs)

class Solution:
    def rotate(self, matrix: List[List[int]]) -> None:
        """
        Do not return anything, modify matrix in-place instead.
        """
        n = len(matrix)
        for i in range(n):
            left = i
            up = i
            right = n-1-i
            down = n-1-i
            if left>=right:
                break
            print(left, right, up, down)
            for j in range(right-left):
                print(j)
                matrix[up+j][right], matrix[down][right-j], matrix[down-j][left], matrix[up][left+j] = \
                matrix[up][left+j], matrix[up+j][right], matrix[down][right-j], matrix[down-j][left]

if __name__=='__main__':
    if lc_lib_path.exists():
        s = Solution()

        matrix = [[1,2,3],[4,5,6],[7,8,9]]
        r = s.rotate(matrix)
        print(r)
        print('-'*30)

