
# load local leetcode lib
import pathlib

lc_lib_path = pathlib.Path('../lc_lib.py')
if lc_lib_path.exists():
    import sys
    sys.path.insert(0, '..')
    from lc_lib import *

old_print = print
def print(*args, **kwargs):
    if lc_lib_path.exists():
        old_print(*args, **kwargs)

from copy import copy
from functools import lru_cache


class Solution:
    def stoneGameVII(self, stones: List[int]) -> int:
        n = len(stones)
        line = [0]*n
        sums = []
        s = 0
        for i,num in enumerate(stones):
            s += num
            sums.append(s)

        mat = [copy(line) for _ in range(n)]

        for k in range(1, n):
            for i in range(n):
                j = i+k
                if j>=n:
                    break
                if_remove_right = sums[j-1]-sums[i]+stones[i]-mat[i][j-1]
                if_remove_left = sums[j]-sums[i+1]+stones[i+1]-mat[i+1][j]
                mat[i][j] = max(if_remove_right, if_remove_left)

        return mat[0][n-1]



if __name__=='__main__':
    if lc_lib_path.exists():
        s = Solution()

        stones = [1,1,1,1,1,1,1,7,1,1,1,1,1,1,1]
        r = s.stoneGameVII(stones)
        print(r)
        print('-'*30)

        stones = [7,90,5,1,100,10,10,2]
        r = s.stoneGameVII(stones)
        print(r)
        print('-'*30)

        from testcase import x
        stones = x
        r = s.stoneGameVII(stones)
        print(r)
        print('-'*30)
