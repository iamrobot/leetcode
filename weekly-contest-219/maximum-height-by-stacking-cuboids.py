
# load local leetcode lib
import pathlib

lc_lib_path = pathlib.Path('../lc_lib.py')
if lc_lib_path.exists():
    import sys
    sys.path.insert(0, '..')
    from lc_lib import *

old_print = print
def print(*args, **kwargs):
    if lc_lib_path.exists():
        old_print(*args, **kwargs)

class TreeNode:
    def __init__(self, val):
        self.val = val
        self.max = None
        self.children = set()

class Solution:
    def maxHeight(self, cuboids: List[List[int]]) -> int:
        cuboids = [sorted(x) for x in cuboids]
        cuboids = sorted(cuboids)
        nodes = [TreeNode(c[-1]) for c in cuboids]
        top_level_nodes = set(nodes)
        for i,ci in enumerate(cuboids):
            for j in range(i+1, len(cuboids)):
                cj = cuboids[j]
                if all([ci[k]<=cj[k] for k in range(3)]):
                    nodes[i].children.add(nodes[j])
                    top_level_nodes.discard(nodes[j])

        def dfs(root):
            if not root.children:
                return root.val

            if root.max:
                return root.max

            max_child_h = 0
            for child in root.children:
                max_child_h = max(max_child_h, dfs(child))

            root.max = max_child_h + root.val
            return root.max

        result = 0
        for root in top_level_nodes:
            result = max(result, dfs(root)) 

        return result
                    

if __name__=='__main__':
    if lc_lib_path.exists():
        s = Solution()

        cuboids = [[76,25,26],[94,86,13],[72,26,13],[90,28,46],[61,27,85],[83,44,11],[74,72,34],[35,66,63],[62,76,73],[39,69,30],[34,55,98],[29,44,44],[17,98,26],[60,7,55],[39,94,51],[15,51,4],[28,82,70],[84,99,19],[12,77,16],[80,62,45],[26,57,72],[74,29,79],[40,17,58],[86,5,53],[88,31,89],[39,7,1],[58,28,75],[13,45,55],[54,72,48],[25,2,43],[73,15,83],[3,2,22],[28,80,85],[78,35,96],[63,23,8],[78,69,93],[66,88,73],[39,61,73],[8,72,70],[26,38,12],[49,20,12],[90,72,33],[54,31,51],[18,44,69],[70,25,21],[56,63,10],[10,56,20],[28,39,95],[16,21,63],[24,93,82],[76,41,100],[100,38,41],[35,8,78],[32,1,20],[31,66,8],[10,14,91],[66,2,55],[49,1,17],[59,9,51],[36,48,58],[19,35,63],[32,31,8],[94,60,10],[40,1,3],[14,24,57],[28,33,71],[78,27,83],[21,49,13],[80,51,9],[50,29,91],[79,48,50],[53,60,12],[75,97,4],[62,18,15],[6,23,38],[93,61,11],[62,30,17],[96,51,57],[96,50,16],[20,69,60],[81,36,60],[49,37,87],[39,20,1],[73,99,23],[25,58,17],[73,1,45],[50,34,69],[22,17,39],[82,15,54],[21,76,26],[38,93,95],[18,4,34],[87,39,93],[90,22,40],[53,44,3],[24,37,62],[46,13,18],[66,60,6],[70,17,39],[30,45,80]]
        r = s.maxHeight(cuboids)
        print(r)
        print('-'*30)

        cuboids = [[38,25,45],[76,35,3]]
        r = s.maxHeight(cuboids)
        print(r)
        print('-'*30)

