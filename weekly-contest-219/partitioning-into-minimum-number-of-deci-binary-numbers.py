
# load local leetcode lib
import pathlib

lc_lib_path = pathlib.Path('../lc_lib.py')
if lc_lib_path.exists():
    import sys
    sys.path.insert(0, '..')
    from lc_lib import *

old_print = print
def print(*args, **kwargs):
    if lc_lib_path.exists():
        old_print(*args, **kwargs)

class Solution:
    def minPartitions(self, n: str) -> int:
        return int(max(n))

if __name__=='__main__':
    if lc_lib_path.exists():
        s = Solution()

        n = "32"
        r = s.minPartitions(n)
        print(r)
        print('-'*30)

