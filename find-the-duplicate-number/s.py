
# load local leetcode lib
import pathlib

lc_lib_path = pathlib.Path('../lc_lib.py')
if lc_lib_path.exists():
    import sys
    sys.path.insert(0, '..')
    from lc_lib import *

from collections import Counter


class Solution:
    def findDuplicate(self, nums: List[int]) -> int:
        c = Counter(nums)
        return c.most_common(1)[0][0]

if __name__=='__main__':
    if lc_lib_path.exists():
        s = Solution()
        x = [1,3,4,2,2]
        r = s.findDuplicate(x)
        print(r)
