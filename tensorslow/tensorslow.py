#!/usr/bin/env python

import sys
import itertools
import numpy as np
from pprint import pprint
from collections import deque
import matplotlib.pyplot as plt

# numpy broadcasting, masking, slicing
# https://howtothink.readthedocs.io/en/latest/PvL_06.html
np.set_printoptions(precision=2, suppress=True)

_default_graph = None



class Graph(object):
    def __init__(self):
        self.operations = []
        self.variables = []
        self.placeholders = []

    def as_default(self):
        global _default_graph
        _default_graph = self
        return self


class Node(object):
    def __init__(self, *args, **kwargs):
        self.consumers = []


class Operation(Node):
    def __init__(self, input_nodes=[]):
        super().__init__()
        self.input_nodes = input_nodes
        for node in self.input_nodes:
            node.consumers.append(self)
        _default_graph.operations.append(self)

    def compute(self):
        pass


class Minimization(Operation):
    def __init__(self, loss, learning_rate):
        super().__init__()
        self.loss = loss
        self.learning_rate = learning_rate

    def compute(self):
        grad_table = back_propagation(self.loss) 
        for node,grad in grad_table.items():
            if type(node)==Variable:
                node.value -= self.learning_rate*grad


def back_propagation(loss):
    grad_table = {loss:1}
    dq = deque([loss])
    visited = set([loss])

    # bfs, from loss
    while dq:
        node = dq.popleft()
        if node!=loss:
            grad_table[node] = 0
            for consumer in node.consumers:
                output_loss = grad_table[consumer]
                inputs_loss = consumer.bprop(output_loss)
                if len(consumer.input_nodes)==1:
                    grad_table[node] += inputs_loss
                else:
                    grad_table[node] += \
                            inputs_loss[consumer.input_nodes.index(node)]
        if hasattr(node, 'input_nodes'):
            for input_node in node.input_nodes:
                if input_node not in visited:
                    visited.add(input_node)
                    dq.append(input_node)

    return grad_table


class add(Operation):
    def __init__(self, x, y):
        super().__init__([x,y])
    
    def compute(self, x, y):
        return x+y

    def bprop(self, grad):
        a = self.inputs[0]
        b = self.inputs[1]

        grad_wrt_a = grad
        while np.ndim(grad_wrt_a) > len(a.shape):
            grad_wrt_a = np.sum(grad_wrt_a, axis=0)
        for axis, size in enumerate(a.shape):
            if size == 1:
                grad_wrt_a = np.sum(grad_wrt_a, axis=axis, keepdims=True)

        grad_wrt_b = grad
        while np.ndim(grad_wrt_b) > len(b.shape):
            grad_wrt_b = np.sum(grad_wrt_b, axis=0)
        for axis, size in enumerate(b.shape):
            if size == 1:
                grad_wrt_b = np.sum(grad_wrt_b, axis=axis, keepdims=True)

        return [grad_wrt_a, grad_wrt_b]


class matmul(Operation):
    def __init__(self, x, y):
        super().__init__([x,y])
    
    def compute(self, x, y):
        return x.dot(y)

    def bprop(self, grad):
        A = self.inputs[0]
        B = self.inputs[1]
        return [grad.dot(B.T), A.T.dot(grad)]


class sigmoid(Operation):
    def __init__(self, a):
        super().__init__([a])
    
    def compute(self, a):
        return 1/(1+np.exp(-a))

    def bprop(self, grad):
        sigmoid = self.output
        return grad * sigmoid * (1 - sigmoid)


class softmax(Operation):
    def __init__(self, a):
        super().__init__([a])
    
    def compute(self, a):
        exp_a = np.exp(a) # (N,d)
        sums = np.sum(exp_a, axis=1) # (N,)
        sums = sums[..., np.newaxis] # (N,1), you can also use: 
                                    # [..., np.newaxis]
                                    # [..., None]
                                    # np.reshape(array, (-1, 1))
                                    # np.atleast_2d(array)
        return exp_a / sums

    def bprop(self, grad):
        softmax = self.output
        return (grad - np.reshape(
            np.sum(grad * softmax, 1),
            [-1, 1]
        )) * softmax


class log(Operation):
    def __init__(self, a):
        super().__init__([a])

    def compute(self, a):
        return np.log(a)

    def bprop(self, grad):
        x = self.inputs[0]
        return grad/x


class multiply(Operation):
    def __init__(self, a, b):
        super().__init__([a,b])

    def compute(self, a, b):
        return a*b

    def bprop(self, grad):
        A = self.inputs[0]
        B = self.inputs[1]
        return [grad * B, grad * A]


class reduce_sum(Operation):
    def __init__(self, a, axis=None):
        super().__init__([a])
        self.axis = axis

    def compute(self, a):
        return np.sum(a, axis=self.axis)

    def bprop(self, grad):
        A = self.inputs[0]

        output_shape = np.array(A.shape)
        output_shape[self.axis] = 1
        tile_scaling = A.shape // output_shape
        grad = np.reshape(grad, output_shape)
        return np.tile(grad, tile_scaling)


class negative(Operation):
    def __init__(self, a):
        super().__init__([a])

    def compute(self, a):
        return -a

    def bprop(self, grad):
        return -grad


class Placeholder(Node):
    def __init__(self):
        super().__init__()
        _default_graph.placeholders.append(self)


class Variable(Node):
    def __init__(self, init_values):
        super().__init__()
        self.value = init_values
        _default_graph.variables.append(self)


def dfs_postorder(root):
    if isinstance(root, Operation):
        for node in root.input_nodes:
            yield from dfs_postorder(node)
    yield root


class Session(object):
    def run(self, target, feed_dict):
        for node in dfs_postorder(target):
            if type(node)==Placeholder:
                node.output = feed_dict[node]

            elif type(node)==Variable:
                node.output = node.value

            else:
                node.inputs = [
                        input_node.output for input_node in node.input_nodes
                    ]
                node.output = node.compute(*node.inputs)

        return target.output

if __name__=='__main__':
    g = Graph().as_default()
    n = 50

    blue_dots = np.concatenate((
        np.array([[0,0]]*n) + 0.2*np.random.randn(n,2),
        np.array([[2,2]]*n) + 0.2*np.random.randn(n,2),
        ))
    red_dots = np.concatenate((
        np.array([[0,2]]*n) + 0.2*np.random.randn(n,2),
        np.array([[2,0]]*n) + 0.2*np.random.randn(n,2),
        ))

    x = Placeholder()
    c = Placeholder()

    w1 = Variable(np.random.randn(2,2))
    b1 = Variable(np.random.randn(2))
    p1 = sigmoid(add(matmul(x, w1), b1))

    w2 = Variable(np.random.randn(2,2))
    b2 = Variable(np.random.randn(2))
    p2 = softmax(add(matmul(p1, w2), b2))

    w3 = Variable(np.random.randn(2,2))
    b3 = Variable(np.random.randn(2))
    p3 = softmax(add(matmul(p2, w3), b3))

    tmp = multiply(c, log(p3))
    tmp = reduce_sum(reduce_sum(tmp, axis=1))
    j = negative(tmp) # cross-entropy loss

    minimization = Minimization(j, 0.003)

    x_val = np.concatenate((red_dots, blue_dots))
    c_val = np.zeros((x_val.shape[0], 2))
    c_val[:blue_dots.shape[0], 0] = 1
    c_val[blue_dots.shape[0]:, 1] = 1
    pprint(c_val)

    feed_dict={
            x: x_val,
            c: c_val,
        }

    session = Session()
    for step in range(3000):
        loss = session.run(j, feed_dict=feed_dict)
        if step%100==0:
            print_line(f'step: {step} loss:{loss}')
        session.run(minimization, feed_dict)

    # Visualize classification boundary
    xs = np.linspace(-4, 4, 100)
    ys = np.linspace(-4, 4, 100)
    pred_classes = []
    for i in xs:
        for j in ys:
            feed_dict={x: np.array([[i, j]])}
            pred_class = session.run(p3,feed_dict=feed_dict)[0]
            pred_classes.append((i, j, pred_class.argmax()))
    xs_p, ys_p = [], []
    xs_n, ys_n = [], []
    for x, y, c in pred_classes:
        if c == 0:
            xs_n.append(x)
            ys_n.append(y)
        else:
            xs_p.append(x)
            ys_p.append(y)
    plt.plot(xs_p, ys_p, 'ro', xs_n, ys_n, 'bo')

    plt.scatter(red_dots[:,0], red_dots[:, 1], color='red')
    plt.scatter(blue_dots[:,0], blue_dots[:, 1], color='blue')
    plt.show()

