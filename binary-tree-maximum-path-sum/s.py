
# load local leetcode lib
import pathlib
lc_lib_path = pathlib.Path('../lc_lib.py')
if lc_lib_path.exists():
    import sys
    sys.path.insert(0, '..')
    from lc_lib import *

old_print = print
def print(*args, **kwargs):
    if lc_lib_path.exists():
        old_print(*args, **kwargs)

# Definition for a binary tree node.
# class TreeNode:
#     def __init__(self, val=0, left=None, right=None):
#         self.val = val
#         self.left = left
#         self.right = right
class Solution:
    def maxPathSum(self, root: TreeNode) -> int:
        result = -float('inf')
        def helper(root):
            nonlocal result
            if root==None:
                return 0
            else:
                left_max = helper(root.left)
                right_max = helper(root.right)
                val = root.val
                r = max(val, val+left_max+right_max, val+left_max, val+right_max)
                result = max(result, r)
                return max(val, val+left_max, val+right_max) 
        
        helper(root)
        return result


if __name__=='__main__':
    if lc_lib_path.exists():
        s = Solution()

        null = None
        root = [5,4,8,11,null,13,4,7,2,null,null,null,1]
        root = BinaryTree.from_list(root)
        r = s.maxPathSum(root)
        print(r)
        print('-'*30)

