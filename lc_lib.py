#!/usr/bin/env python

# these are only for local leetcode libs
import bisect
import os
import pathlib
import random
import sys
import time
import bisect
import itertools

# these are those libraries that might need be copied
# into solution source code
from sortedcontainers import SortedList, SortedDict, SortedSet
from collections import defaultdict, deque
from functools import partial
from pprint import pprint
from typing import *

# these are local leetcode libs, you might want to 
# copy source code to solution file
from libs.linked_list import ListNode
from libs.binary_tree import BinaryTree, TreeNode
from libs.union_find import UnionFindForest
from libs.matrix import Matrix
from libs.trie import Trie
from libs.lis import lis    # longest increasing subarray
from libs.edit_distance import edit_distance    # edit distance

class DefaultSortedDict(SortedDict):
    def __init__(self, default, *args, **kwargs):
        self.default = default
        super().__init__(*args, **kwargs)

    def __missing__(self, key):
        return default


def num_to_bin(num, fix_len=32):
    # or you can just use bin(num)[2:]
    l = []
    for i in range(fix_len):
        l.insert(0, num%2)
        num = num // 2
    return l 


def generate_list(vals=list(range(10)), length=10):
    return [random.choice(vals) for _ in range(length)]


def zeros(rows, cols):
    return [[0]*cols for _ in range(rows)]


def zeros_like(matrix):
    rows = len(matrix)
    if rows==0: return []
    cols = len(matrix[0])
    return [[0]*cols for _ in range(rows)]


def sorted_via_cmp(l, cmpf):
    '''
        # cmpf must return 1,0,or -1, as following example
        def cmpf(a, b):
            if a+b<b+a:
                return 1
            elif a+b==b+a:
                return 0
            else:
                return -1
    '''
    from functools import cmp_to_key
    return sorted(l, key=cmp_to_key(cmpf))


