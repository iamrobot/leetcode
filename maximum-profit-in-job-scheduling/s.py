
# load local leetcode lib
import pathlib

lc_lib_path = pathlib.Path('../lc_lib.py')
if lc_lib_path.exists():
    import sys
    sys.path.insert(0, '..')
    from lc_lib import *

from pprint import pprint


class Solution:
    def jobScheduling(self, startTime: List[int], endTime: List[int], profit: List[int]) -> int:
        jobs = list(zip(startTime, endTime, profit)) 
        jobs = sorted(jobs, key=lambda x:x[1])
        startTime = [x[0] for x in jobs]
        endTime = [x[1] for x in jobs]
        profit = [x[2] for x in jobs]

        max_profit = 0
        for index,job in enumerate(jobs):
            start, end, this_profit = job

            i = index-1
            while i>=0 and endTime[i]>start:
                i -= 1

            last_profit = 0 if i<0 else profit[i]
            p = this_profit + last_profit
            if p>max_profit:
                max_profit = p
            profit[index] = max_profit

        return max_profit

if __name__=='__main__':
    if lc_lib_path.exists():
        s = Solution()
        startTime = [1,2,3,4,6]
        endTime = [3,5,10,6,9]
        profit = [20,20,100,70,60]
        r = s.jobScheduling(startTime, endTime, profit)
        print(r) # -> 150
        print('='*30)

        startTime = [24,24,7,2,1,13,6,14,18,24]
        endTime = [27,27,20,7,14,22,20,24,19,27]
        profit = [6,1,4,2,3,6,5,6,9,8]
        r = s.jobScheduling(startTime, endTime, profit)
        print(r) # -> 20
