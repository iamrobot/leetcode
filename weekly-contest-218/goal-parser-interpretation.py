
# load local leetcode lib
import pathlib

lc_lib_path = pathlib.Path('../lc_lib.py')
if lc_lib_path.exists():
    import sys
    sys.path.insert(0, '..')
    from lc_lib import *

old_print = print
def print(*args, **kwargs):
    if lc_lib_path.exists():
        old_print(*args, **kwargs)

class Solution:
    def interpret(self, command: str) -> str:
        stack = []
        result = ''
        for i,char in enumerate(command):
            if char=='G':
                result += 'G'
            elif char==')':
                s = ''
                while True:
                    p = stack.pop()
                    if p=='(':
                        break
                    s = p+s
                if s=='':
                    result += 'o'
                else:
                    result += 'al'
            else:
                stack.append(char)

        return result

if __name__=='__main__':
    if lc_lib_path.exists():
        s = Solution()

        command = "(al)G(al)()()G"
        r = s.interpret(command)
        print(r)
        print('-'*30)

