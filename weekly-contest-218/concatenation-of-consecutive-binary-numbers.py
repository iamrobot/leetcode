
# load local leetcode lib
import pathlib

lc_lib_path = pathlib.Path('../lc_lib.py')
if lc_lib_path.exists():
    import sys
    sys.path.insert(0, '..')
    from lc_lib import *

old_print = print
def print(*args, **kwargs):
    if lc_lib_path.exists():
        old_print(*args, **kwargs)

class Solution:
    def concatenatedBinary(self, n: int) -> int:
        s = ''
        for i in range(1, n+1):
            si = bin(i)[2:]
            s += si
        result = int(s, base=2)
        return result % (10**9+7)

if __name__=='__main__':
    if lc_lib_path.exists():
        s = Solution()

        n = 30000
        r = s.concatenatedBinary(n)
        print(r)
        print('-'*30)

