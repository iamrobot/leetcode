
# load local leetcode lib
import pathlib

lc_lib_path = pathlib.Path('../lc_lib.py')
if lc_lib_path.exists():
    import sys
    sys.path.insert(0, '..')
    from lc_lib import *

old_print = print
def print(*args, **kwargs):
    if lc_lib_path.exists():
        old_print(*args, **kwargs)

from collections import Counter


class Solution:
    def maxOperations(self, nums: List[int], k: int) -> int:
        target = k
        c = Counter(nums)
        result = 0
        for k,v in c.items():
            other = target-k
            if other in c:
                if other==k:
                    result += (v//2)*2
                else:
                    result += min(v, c[other])

        return result // 2

if __name__=='__main__':
    if lc_lib_path.exists():
        s = Solution()

        nums = [2,2,2,3,1,1,4,1]
        k = 4
        r = s.maxOperations(nums,k)
        print(r)
        print('-'*30)

        nums = [3,1,3,4,3]
        k = 6
        r = s.maxOperations(nums,k)
        print(r)
        print('-'*30)
