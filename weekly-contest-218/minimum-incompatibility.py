
# load local leetcode lib
import pathlib

lc_lib_path = pathlib.Path('../lc_lib.py')
if lc_lib_path.exists():
    import sys
    sys.path.insert(0, '..')
    from lc_lib import *

old_print = print
def print(*args, **kwargs):
    if lc_lib_path.exists():
        old_print(*args, **kwargs)

from collections import Counter
from copy import deepcopy
from itertools import combinations


class Solution:
    def minimumIncompatibility(self, nums: List[int], k: int) -> int:
        counter = Counter(nums)
        groups = k
        items_in_one_group = len(nums) // groups
        if counter.most_common()[0][-1]>groups:
            return -1

        print(counter.most_common())

        result = 0
        return result

if __name__=='__main__':
    if lc_lib_path.exists():
        s = Solution()

        nums = [7,3,16,15,1,13,1,2,14,5,3,10,6,2,7,15]
        k = 8
        r = s.minimumIncompatibility(nums,k)
        print(r)
        print('-'*30)

        nums = [6,3,8,1,3,1,2,2]
        k = 4
        r = s.minimumIncompatibility(nums,k)
        print(r)
        print('-'*30)
