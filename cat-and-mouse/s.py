
# load local leetcode lib
import pathlib
lc_lib_path = pathlib.Path('../lc_lib.py')
if lc_lib_path.exists():
    import sys
    sys.path.insert(0, '..')
    from lc_lib import *

old_print = print
def print(*args, **kwargs):
    if lc_lib_path.exists():
        old_print(*args, **kwargs)

import collections
class Solution:
    def catMouseGame(self, graph: List[List[int]]) -> int:
        min_dis_map = collections.defaultdict(lambda:dict())
        for i,neighbors in enumerate(graph):
            q = collections.deque([(i,0)])
            seen = set([i])
            while q:
                j, k = q.popleft()
                min_dis_map[i][j] = k
                for p in graph[j]:
                    if p in seen:
                        continue
                    q.append((p,k+1))
                    seen.add(p)

        draw = 0
        mouse_win = 1
        cat_win = 2

        history = set()
        mouse = 1
        cat = 2
        hole = 0
        mouse_turn = True
        while True:
            cur = mouse, cat, mouse_turn
            if cur in history:
                return draw
            history.add(cur)
            print(mouse_turn, mouse, cat)

            if mouse_turn:
                neighbors = graph[mouse]
                if hole in neighbors:
                    return mouse_win
                neighbors = [(0 if min_dis_map[x][cat]>1 else 1, min_dis_map[x][hole], x) for x in neighbors]
                neighbors = sorted(neighbors)
                print(neighbors)
                mouse = neighbors[0][-1]
            else:
                neighbors = graph[cat]
                if mouse in [cat, *neighbors]:
                    return cat_win
                neighbors = [(min_dis_map[x][mouse], min_dis_map[x][hole], x) for x in neighbors]
                neighbors = sorted(neighbors)
                print(neighbors)
                cat = neighbors[0][-1]
            
            print('')
            mouse_turn = not mouse_turn

if __name__=='__main__':
    if lc_lib_path.exists():
        sol = Solution()

        graph = [[2,5],[3],[0,4,5],[1,4,5],[2,3],[0,2,3]]
        r = sol.catMouseGame(graph)
        print(r)
        print('-'*30)

        graph = [[3,4],[3,5],[3,6],[0,1,2],[0,5,6],[1,4],[2,4]]
        r = sol.catMouseGame(graph)
        print(r)
        print('-'*30)

