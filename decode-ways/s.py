
# load local leetcode lib
import pathlib
lc_lib_path = pathlib.Path('../lc_lib.py')
if lc_lib_path.exists():
    import sys
    sys.path.insert(0, '..')
    from lc_lib import *

old_print = print
def print(*args, **kwargs):
    if lc_lib_path.exists():
        old_print(*args, **kwargs)

d = set([str(i) for i in range(1, 27)])
import functools

class Solution:
    @functools.cache
    def numDecodings(self, s: str) -> int:
        if len(s)==0:
            r = 1
        
        elif len(s)==1:
            r = int(s[0] in d)
        
        else:
            r = 0
            if s[:1] in d:
                r += self.numDecodings(s[1:])
            if s[:2] in d:
                r += self.numDecodings(s[2:])

        return r

if __name__=='__main__':
    if lc_lib_path.exists():
        sol = Solution()

        s = "11106"
        r = sol.numDecodings(s)
        print(r)
        print('-'*30)

        s = "226"
        r = sol.numDecodings(s)
        print(r)
        print('-'*30)

