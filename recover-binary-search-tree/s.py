
# load local leetcode lib
import pathlib
lc_lib_path = pathlib.Path('../lc_lib.py')
if lc_lib_path.exists():
    import sys
    sys.path.insert(0, '..')
    from lc_lib import *

old_print = print
def print(*args, **kwargs):
    if lc_lib_path.exists():
        old_print(*args, **kwargs)

# Definition for a binary tree node.
# class TreeNode:
#     def __init__(self, val=0, left=None, right=None):
#         self.val = val
#         self.left = left
#         self.right = right
from collections import deque
class Solution:
    def recoverTree(self, root: TreeNode) -> None:
        """
        Do not return anything, modify root in-place instead.
        """
        inf = float('inf')

        last = TreeNode(-inf)
        def inorder_from_left(root):
            nonlocal last
            if not root:
                return
            inorder_from_left(root.left)
            this_val = root.val
            last_val = last.val
            if this_val<last_val:
                last.val = this_val
                root.val = last_val
            last = root
            inorder_from_left(root.right)
        inorder_from_left(root)

        last = TreeNode(inf)
        def inorder_from_right(root):
            nonlocal last
            if not root:
                return
            inorder_from_right(root.right)
            this_val = root.val
            last_val = last.val
            if this_val>last_val:
                last.val = this_val
                root.val = last_val
            last = root
            inorder_from_right(root.left)
        inorder_from_right(root)

        return root

if __name__=='__main__':
    if lc_lib_path.exists():
        s = Solution()

        root = [1,3,null,null,2]
        r = s.recoverTree(root)
        print(r)
        print('-'*30)

