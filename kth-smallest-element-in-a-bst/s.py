
# load local leetcode lib
import pathlib

lc_lib_path = pathlib.Path('../lc_lib.py')
if lc_lib_path.exists():
    import sys
    sys.path.insert(0, '..')
    from lc_lib import *

# Definition for a binary tree node.
# class TreeNode:
#     def __init__(self, val=0, left=None, right=None):
#         self.val = val
#         self.left = left
#         self.right = right
class Solution:
    def kthSmallest(self, root: TreeNode, k: int) -> int:
        def dfs(root):
            if root.left:
                yield from dfs(root.left)
            yield root
            if root.right:
                yield from dfs(root.right)
        i = 0
        for node in dfs(root):
            i += 1
            if i==k:
               return node.val

if __name__=='__main__':
    if lc_lib_path.exists():
        s = Solution()
        s.method()
