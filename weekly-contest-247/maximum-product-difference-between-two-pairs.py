
# load local leetcode lib
import pathlib
lc_lib_path = pathlib.Path('../lc_lib.py')
if lc_lib_path.exists():
    import sys
    sys.path.insert(0, '..')
    from lc_lib import *

old_print = print
def print(*args, **kwargs):
    if lc_lib_path.exists():
        old_print(*args, **kwargs)

class Solution:
    def maxProductDifference(self, nums: List[int]) -> int:
        nums = sorted(nums)
        return nums[-1]*nums[-2] - nums[0]*nums[1]

if __name__=='__main__':
    if lc_lib_path.exists():
        sol = Solution()

        nums = [5,6,2,7,4]
        r = sol.maxProductDifference(nums)
        print(r)
        print('-'*30)

