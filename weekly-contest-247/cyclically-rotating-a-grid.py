
# load local leetcode lib
import pathlib
lc_lib_path = pathlib.Path('../lc_lib.py')
if lc_lib_path.exists():
    import sys
    sys.path.insert(0, '..')
    from lc_lib import *

old_print = print
def print(*args, **kwargs):
    if lc_lib_path.exists():
        old_print(*args, **kwargs)

import collections

class Solution:
    def rotateGrid(self, grid: List[List[int]], k: int) -> List[List[int]]:
        m = len(grid)
        n = len(grid[0])
        max_i = min(m//2, n//2)
        for depth in range(max_i):
            top = depth
            left = depth
            right = depth + (n//2-depth)*2 -1
            bottom = depth + (m//2-depth)*2 -1
            
            point_ij = []
            values = []
            def add_ij(i,j):
                point_ij.append((i,j))
                values.append(grid[i][j])
            i = top
            for j in range(left, right):
                add_ij(i,j)
            j = right
            for i in range(top, bottom):
                add_ij(i,j)
            i = bottom
            for j in range(right, left, -1):
                add_ij(i,j)
            j = left
            for i in range(bottom, top, -1):
                add_ij(i,j)

            print(point_ij)
            print(values)
            
            dq = collections.deque(values)
            dq.rotate(-(k % len(values)))

            print(point_ij)
            print(dq)

            for index, ij in enumerate(point_ij):
                i,j = ij
                grid[i][j] = dq[index]

        return grid

if __name__=='__main__':
    if lc_lib_path.exists():
        sol = Solution()

        grid = [[40,10],[30,20]]
        k = 1
        r = sol.rotateGrid(grid,k)
        print(r)
        print('-'*30)


        grid = [[1,2,3,4],[5,6,7,8],[9,10,11,12],[13,14,15,16]]
        k = 2
        r = sol.rotateGrid(grid,k)
        print(r)
        print('-'*30)
