
# load local leetcode lib
import pathlib
lc_lib_path = pathlib.Path('../lc_lib.py')
if lc_lib_path.exists():
    import sys
    sys.path.insert(0, '..')
    from lc_lib import *

old_print = print
def print(*args, **kwargs):
    if lc_lib_path.exists():
        old_print(*args, **kwargs)


class Solution:
    def wonderfulSubstrings(self, word: str) -> int:
        d = {'':1}
        result = 0
        for char in word:
            result += d['']
            new_d = {'':1}
            for k,v in d.items():
                if char==k:
                    result += v
                    new_d[''] += v
                    continue

                if char in k:
                    if len(k)==2:
                        result += v
                    new_k = k.replace(char, '')
                    new_d[new_k] = v
                else:
                    new_k = ''.join(sorted(k+char))
                    new_d[new_k] = v
            d = new_d

        return result

if __name__=='__main__':
    if lc_lib_path.exists():
        sol = Solution()

        word = "aabb"
        r = sol.wonderfulSubstrings(word)
        print(r)
        print('-'*30)

