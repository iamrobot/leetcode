
# load local leetcode lib
import pathlib
lc_lib_path = pathlib.Path('../lc_lib.py')
if lc_lib_path.exists():
    import sys
    sys.path.insert(0, '..')
    from lc_lib import *

old_print = print
def print(*args, **kwargs):
    if lc_lib_path.exists():
        old_print(*args, **kwargs)

class Solution:
    def maxSubArray(self, nums: List[int]) -> int:
        if len(nums)==0:
            return 0

        import math
        import itertools
        result = -math.inf
        cumsums = itertools.accumulate(nums)
        min_s = 0
        for s in cumsums:
            result = max(result, s-min_s)
            min_s = min(min_s, s)

        return result

if __name__=='__main__':
    if lc_lib_path.exists():
        sol = Solution()

        nums = [-2,1,-3,4,-1,2,1,-5,4]
        r = sol.maxSubArray(nums)
        print(r)
        print('-'*30)

        nums = [-1]
        r = sol.maxSubArray(nums)
        print(r)
        print('-'*30)

