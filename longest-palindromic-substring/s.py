
# load local leetcode lib
import pathlib

lc_lib_path = pathlib.Path('../lc_lib.py')
if lc_lib_path.exists():
    import sys
    sys.path.insert(0, '..')
    from lc_lib import *

class Solution:
    def longestPalindrome(self, s: str) -> str:
        if len(s)<=1:
            return s

        longest_palindrome = ''
        for i in range(len(s)):
            # situation 1
            j = 1
            while i-j>=0 and i+j<len(s) and s[i-j]==s[i+j]:
                j += 1
            j -= 1
            if 2*j+1>len(longest_palindrome):
                longest_palindrome = s[i-j:i+j+1]

            # situation 2
            if i+1<len(s) and s[i]==s[i+1]:
                j = 1
                while i-j>=0 and i+1+j<len(s) and s[i-j]==s[i+1+j]:
                    j += 1
                j -= 1
                if 2*j+2>len(longest_palindrome):
                    longest_palindrome = s[i-j:i+1+j+1]
        
        return longest_palindrome

if __name__=='__main__':
    if lc_lib_path.exists():
        s = Solution()
        x = 'babad'
        r = s.longestPalindrome(x)
        print(r)

        x = 'cbbd'
        r = s.longestPalindrome(x)
        print(r)
