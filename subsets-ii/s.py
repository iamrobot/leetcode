
# load local leetcode lib
import pathlib

lc_lib_path = pathlib.Path('../lc_lib.py')
if lc_lib_path.exists():
    import sys
    sys.path.insert(0, '..')
    from lc_lib import *

old_print = print
def print(*args, **kwargs):
    if lc_lib_path.exists():
        old_print(*args, **kwargs)

class Solution:
    def subsetsWithDup(self, nums: List[int]) -> List[List[int]]:
        from collections import Counter
        c = Counter(nums)
        sorted_nums = sorted(nums)

        def backtrack(nums):
            print(nums)
            if not nums:
                yield []
            else:
                num = nums[0]
                count = c[num]
                for i in range(c[num]+1):
                    for r in backtrack(nums[count:]):
                        yield [num]*i+r
                
        return list(backtrack(sorted_nums))

if __name__=='__main__':
    if lc_lib_path.exists():
        s = Solution()
        x = [1,2,2]
        r = s.subsetsWithDup(x)
        print(r)
        print('-'*30)

        x = [4,4,4,1,4]
        r = s.subsetsWithDup(x)
        print(r)
        print('-'*30)
