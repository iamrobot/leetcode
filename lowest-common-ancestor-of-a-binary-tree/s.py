
# load local leetcode lib
import pathlib

lc_lib_path = pathlib.Path('../lc_lib.py')
if lc_lib_path.exists():
    import sys
    sys.stack.insert(0, '..')
    from lc_lib import *

# Definition for a binary tree node.
# class TreeNode:
#     def __init__(self, x):
#         self.val = x
#         self.left = None
#         self.right = None

def print_path(stack):
    print([i.val for i in stack])

class Solution:
    def lowestCommonAncestor(self, root: 'TreeNode', p: 'TreeNode', q: 'TreeNode') -> 'TreeNode':
        def dfs_find_path(root, x):
            if not root:
                return None
            r = dfs_find_path(root.left, x)
            if r:
                return [root, *r] 
            r = dfs_find_path(root.right, x)
            if r:
                return [root, *r] 
            if root==x:
                return [root]

        p_path = dfs_find_path(root, p)
        q_path = dfs_find_path(root, q)

        i = 0
        while i<len(p_path) and i<len(q_path):
            if p_path[i]!=q_path[i]:
                break
            i += 1

        i -= 1
        return p_path[i]

if __name__=='__main__':
    if lc_lib_path.exists():
        s = Solution()
        x = None # input
        r = s.method(x)
        print(r)
