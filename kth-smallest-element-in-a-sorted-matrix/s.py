
# load local leetcode lib
import pathlib
lc_lib_path = pathlib.Path('../lc_lib.py')
if lc_lib_path.exists():
    import sys
    sys.path.insert(0, '..')
    from lc_lib import *

old_print = print
def print(*args, **kwargs):
    if lc_lib_path.exists():
        old_print(*args, **kwargs)

from sortedcontainers import SortedList
class Solution:
    def kthSmallest(self, matrix: List[List[int]], k: int) -> int:
        n = len(matrix)
        if n==1 or k==1:
            return matrix[0][0]

        l = SortedList()
        l.add((matrix[0][0],0,0))
        seen = set()
        for _ in range(k):
            val, i, j = l.pop(0)
            if i<n-1 and (i+1,j) not in seen:
                l.add((matrix[i+1][j], i+1, j))
                seen.add((i+1,j))
            if j<n-1 and (i,j+1) not in seen:
                l.add((matrix[i][j+1], i, j+1))
                seen.add((i,j+1))

        return val

if __name__=='__main__':
    if lc_lib_path.exists():
        s = Solution()

        matrix = [[1,5,9],[10,11,13],[12,13,15]]
        k = 8
        r = s.kthSmallest(matrix,k)
        print(r)
        print('-'*30)

        matrix = [[1,4,7,11,15],[2,5,8,12,19],[3,6,9,16,22],[10,13,14,17,24],[18,21,23,26,30]]
        k = 5
        r = s.kthSmallest(matrix,k)
        print(r)
        print('-'*30)

        matrix = [[1,3,5],[6,7,12],[11,14,14]]
        pprint(matrix)
        k = 6
        r = s.kthSmallest(matrix,k)
        print(r)
        print('-'*30)

