
# Definition for singly-linked list.
class ListNode:
    def __init__(self, x):
        self.val = x
        self.next = None

# Definition for a binary tree node.
class TreeNode:
    def __init__(self, val=0, left=None, right=None):
        self.val = val
        self.left = left
        self.right = right

from typing import *


# Definition for a binary tree node.
# class TreeNode:
#     def __init__(self, val=0, left=None, right=None):
#         self.val = val
#         self.left = left
#         self.right = right
class Solution:
    def isSymmetric(self, root: TreeNode) -> bool:
        if not root:
            return True

        from queue import Queue

        q = Queue()
        q.put(root)
        next_level_q = Queue()
        next_level_nodes = []

        while True:
            while not q.empty():
                node = q.get()

                if node.left:
                    next_level_q.put(node.left)
                    next_level_nodes.append(node.left.val)
                else:
                    next_level_nodes.append(None)
                if node.right:
                    next_level_q.put(node.right)
                    next_level_nodes.append(node.right.val)
                else:
                    next_level_nodes.append(None)

            print(next_level_nodes)
            if not next_level_nodes==next_level_nodes[::-1]:
                return False

            q = next_level_q
            if q.empty():
                break
            next_level_q = Queue()
            next_level_nodes = []
        return True

if __name__=='__main__':
    s = Solution()
