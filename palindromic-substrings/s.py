
# load local leetcode lib
import pathlib

lc_lib_path = pathlib.Path('../lc_lib.py')
if lc_lib_path.exists():
    import sys
    sys.path.insert(0, '..')
    from lc_lib import *

class Solution:
    def countSubstrings(self, s: str) -> int:
        result = 0
        for i,char in enumerate(s):
            j = 0
            while i-j>=0 and i+j<len(s):
                if s[i-j]==s[i+j]:
                    result += 1
                else:
                    break
                j += 1

            if i+1<len(s) and s[i]==s[i+1]:
                result += 1
                j = 1
                while i-j>=0 and i+1+j<len(s):
                    if s[i-j]==s[i+1+j]:
                        result += 1
                    else:
                        break
                    j += 1

        return result

if __name__=='__main__':
    if lc_lib_path.exists():
        s = Solution()
        x = 'abc'
        r = s.countSubstrings(x)
        print(r)

        x = 'aaa'
        r = s.countSubstrings(x)
        print(r)

        x = 'aaaaa'
        r = s.countSubstrings(x)
        print(r)
