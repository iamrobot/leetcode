
# load local leetcode lib
import pathlib

lc_lib_path = pathlib.Path('../lc_lib.py')
if lc_lib_path.exists():
    import sys
    sys.path.insert(0, '..')
    from lc_lib import *

import heapq


class Solution:
    def dailyTemperatures(self, T: List[int]) -> List[int]:
        inf = float('inf')
        T.reverse()
        results = []
        min_temp = min(T)
        max_temp = max(T)
        latest = [-1]*(max_temp-min_temp+1)
        T = [x-min_temp for x in T]
        for i,temp in enumerate(T):
            latest_warmer_day = -inf
            for warmer_day in latest[temp+1:]:
                if warmer_day!=-1:
                    latest_warmer_day = max(latest_warmer_day, warmer_day)
            if latest_warmer_day==-inf:
                results.append(0)
            else:
                results.append(i-latest_warmer_day)
            latest[temp] = i

        results.reverse()
        return results
                

if __name__=='__main__':
    if lc_lib_path.exists():
        s = Solution()
        x = [73, 74, 75, 71, 69, 72, 76, 73]
        r = s.dailyTemperatures(x)
        print(r)
