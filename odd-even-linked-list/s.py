
# load local leetcode lib
import pathlib
lc_lib_path = pathlib.Path('../lc_lib.py')
if lc_lib_path.exists():
    import sys
    sys.path.insert(0, '..')
    from lc_lib import *

old_print = print
def print(*args, **kwargs):
    if lc_lib_path.exists():
        old_print(*args, **kwargs)

# Definition for singly-linked list.
# class ListNode:
#     def __init__(self, val=0, next=None):
#         self.val = val
#         self.next = next
class Solution:
    def oddEvenList(self, head: ListNode) -> ListNode:
        if head==None or head.next==None:
            return head

        l0_head = head
        l1_head = head.next
        p0 = l0_head
        p1 = l1_head
        p = l1_head.next
        i = 3
        while p:
            if i%2==1:
                p0.next = p
                p0 = p
            else:
                p1.next = p
                p1 = p
            p = p.next
            i += 1

        p0.next = l1_head
        p1.next = None
        return head

if __name__=='__main__':
    if lc_lib_path.exists():
        s = Solution()

        head = ListNode.generate(end=10)
        print(head)
        r = s.oddEvenList(head)
        print(r)
        print('-'*30)

