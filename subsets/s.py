
# load local leetcode lib
import pathlib

lc_lib_path = pathlib.Path('../lc_lib.py')
if lc_lib_path.exists():
    import sys
    sys.path.insert(0, '..')
    from lc_lib import *

class Solution:
    def subsets(self, nums: List[int]) -> List[List[int]]:
        def helper(nums):
            if not nums:
                yield []
            else:
                num = nums[0]
                for x in helper(nums[1:]):
                    yield x
                    yield [num] + x

        result_l = list(helper(nums))
        return result_l

if __name__=='__main__':
    if lc_lib_path.exists():
        s = Solution()
        nums = [1,2,3]
        r = s.subsets(nums)
        print(r)
