
# load local leetcode lib
import pathlib
lc_lib_path = pathlib.Path('../lc_lib.py')
if lc_lib_path.exists():
    import sys
    sys.path.insert(0, '..')
    from lc_lib import *

old_print = print
def print(*args, **kwargs):
    if lc_lib_path.exists():
        old_print(*args, **kwargs)

import string
import itertools
class Solution:
    def letterCombinations(self, digits: str) -> List[str]:
        if not digits:
            return []
        result = []
        s = str(string.ascii_lowercase)
        l = [3]*5 + [4,3,4]
        d = dict()
        for j,chunk in enumerate(l):
            i = j+2
            d[i] = s[:chunk]
            s = s[chunk:]
        l = [d[int(i)] for i in digits]
        for x in itertools.product(*l):
            result.append(''.join(x))
        return result


if __name__=='__main__':
    if lc_lib_path.exists():
        s = Solution()

        digits = "23"
        r = s.letterCombinations(digits)
        print(r)
        print('-'*30)

        digits = ""
        r = s.letterCombinations(digits)
        print(r)
        print('-'*30)

