
# load local leetcode lib
import pathlib
lc_lib_path = pathlib.Path('../lc_lib.py')
if lc_lib_path.exists():
    import sys
    sys.path.insert(0, '..')
    from lc_lib import *

old_print = print
def print(*args, **kwargs):
    if lc_lib_path.exists():
        old_print(*args, **kwargs)


class Matrix:
    def __init__(self, matrix):
        self.matrix = matrix
        self.m = len(matrix)
        self.n = len(matrix[0]) if self.m else None

    def __getattr__(self, item):
        return getattr(self.matrix, item)

    def __getitem__(self, item):
        return self.matrix[item]

    def __setitem__(self, key, value):
        self.matrix[key] = value

    def __repr__(self):
        return repr(self.matrix)

    def __str__(self):
        return str(self.matrix)

    def __call__(self, *args):
        if len(args)==1:
            i,j = args[0]
        elif len(args)==2:
            i,j = args
        else:
            msg = 'you can get matrix item by matrix[i][j], matrix(i,j) or matrix((i,j)), no other ways'
            raise Exception(msg)
        return self[i][j]

    def __iter__(self):
        m = self.m
        n = self.n
        for i in range(m):
            for j in range(n):
                pos = (i,j)
                yield pos, self(pos)

    def is_pos_in_matrix(self, pos):
        m = self.m
        n = self.n
        i,j = pos
        return 0<=i<m and 0<=j<n

    def get_neighbors(self, pos, including_diag=False):
        i,j = pos
        neighbors = [
                (i, j+1),
                (i, j-1),
                (i+1, j),
                (i-1, j),
                ]
        if including_diag:
            neighbors += [
                    (i-1, j-1),
                    (i-1, j+1),
                    (i+1, j-1),
                    (i+1, j+1)
                    ]
        return [pos for pos in neighbors if self.is_pos_in_matrix(pos)]

    def zeros_like(self):
        rows = self.m
        cols = self.n
        if rows==0: return []
        return [[0]*cols for _ in range(rows)]

from copy import deepcopy
class Solution:
    def gameOfLife(self, board: List[List[int]]) -> None:
        """
        Do not return anything, modify board in-place instead.
        """
        old_board = Matrix(deepcopy(board))
        board = Matrix(board)

        for pos,_ in old_board:
            i,j = pos
            neighbors_count = sum([old_board(x) for x in old_board.get_neighbors(pos, including_diag=True)])
            print(pos, old_board(pos), neighbors_count)
            if old_board(pos)==1:
                v = int(neighbors_count in [2,3])
            else:
                v = int(neighbors_count==3)
            board[i][j] = v


if __name__=='__main__':
    if lc_lib_path.exists():
        sol = Solution()

        board = [[0,1,0],[0,0,1],[1,1,1],[0,0,0]]
        pprint(board)
        r = sol.gameOfLife(board)
        print(r)
        pprint(board)
        print('-'*30)

