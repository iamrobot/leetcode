
# load local leetcode lib
import pathlib

lc_lib_path = pathlib.Path('../lc_lib.py')
if lc_lib_path.exists():
    import sys
    sys.path.insert(0, '..')
    from lc_lib import *

class Solution:
    def rob(self, nums: List[int]) -> int:
        if not nums:
            return 0

        nums_length = len(nums)
        memo = [-1]*nums_length
        for i,num in enumerate(nums):
            if i==0:
                memo[0] = num
            elif i==1:
                memo[1] = max(nums[:2])
            else:
                memo[i] = max(memo[i-1], memo[i-2]+num)

        return memo[-1]
            
            

if __name__=='__main__':
    if lc_lib_path.exists():
        s = Solution()
        x = [2,7,9,3,1]
        r = s.rob(x)
        print(r)
