
# load local leetcode lib
import pathlib
lc_lib_path = pathlib.Path('../lc_lib.py')
if lc_lib_path.exists():
    import sys
    sys.path.insert(0, '..')
    from lc_lib import *

old_print = print
def print(*args, **kwargs):
    if lc_lib_path.exists():
        old_print(*args, **kwargs)

import bisect
import itertools
class Solution:
    def eraseOverlapIntervals(self, intervals: List[List[int]]) -> int:
        def helper(intervals):
            remove_i_set = set()
            current_end = intervals[0][1]
            for i in range(len(intervals)-1):
                p = intervals[i]
                q = intervals[i+1]
                if q[0]<current_end:
                    remove_i_set.add(i+1)
                else:
                    current_end = q[1]
            intervals = [interval for i,interval in enumerate(intervals) if i not in remove_i_set]
            return intervals

        intervals = sorted(intervals, key=lambda x:(x[1], x[0]))
        #print(intervals)
        result = helper(intervals)
        #print(result)
        return len(intervals)-len(result)

        
if __name__=='__main__':
    if lc_lib_path.exists():
        sol = Solution()

        intervals = [[1,2],[2,3],[3,4],[1,3]]
        r = sol.eraseOverlapIntervals(intervals)
        print(r)
        print('-'*30)

        intervals = [[1,2],[1,2],[1,2]]
        r = sol.eraseOverlapIntervals(intervals)
        print(r)
        print('-'*30)

        intervals = [[1,2],[2,3]]
        r = sol.eraseOverlapIntervals(intervals)
        print(r)
        print('-'*30)

        intervals = [[1,100],[11,22],[1,11],[2,12]]
        r = sol.eraseOverlapIntervals(intervals)
        print(r)
        print('-'*30)

        intervals = [[-52,31],[-73,-26],[82,97],[-65,-11],[-62,-49],[95,99],[58,95],
                        [-31,49],[66,98],[-63,2],[30,47],[-40,-26]]
        r = sol.eraseOverlapIntervals(intervals)
        print(r)
        print('-'*30)

