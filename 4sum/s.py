
# load local leetcode lib
import pathlib
lc_lib_path = pathlib.Path('../lc_lib.py')
if lc_lib_path.exists():
    import sys
    sys.path.insert(0, '..')
    from lc_lib import *

old_print = print
def print(*args, **kwargs):
    if lc_lib_path.exists():
        old_print(*args, **kwargs)

import itertools
import collections
class Solution:
    def fourSum(self, nums: List[int], target: int) -> List[List[int]]:
        n = len(nums)
        sum_to_index_map = collections.defaultdict(lambda:set())
        for i,j in itertools.combinations(list(range(n)), 2):
            s = nums[i] + nums[j]
            sum_to_index_map[s].add((i,j))

        results = set()
        for s in sum_to_index_map:
            other = target-s
            if other not in sum_to_index_map:
                continue
            if other==s and len(sum_to_index_map[s])<2:
                continue
            s_set = sum_to_index_map[s]
            other_set = sum_to_index_map[other]
            for x,y in itertools.product(s_set, other_set):
                result = tuple(sorted([*x, *y]))
                if len(set(result))==4:
                    result = [nums[i] for i in result]
                    result = tuple(sorted(result))
                    results.add(result)

        return results

if __name__=='__main__':
    if lc_lib_path.exists():
        sol = Solution()

        nums = [1,0,-1,0,-2,2]
        target = 0
        r = sol.fourSum(nums,target)
        print(r)
        print('-'*30)

        nums = [-2,-1,-1,1,1,2,2]
        target = 0
        r = sol.fourSum(nums,target)
        print(r)
        print('-'*30)

