
# load local leetcode lib
import pathlib

lc_lib_path = pathlib.Path('../lc_lib.py')
if lc_lib_path.exists():
    import sys
    sys.path.insert(0, '..')
    from lc_lib import *

def num_to_bin(num):
    l = []
    for i in range(32):
        l.insert(0, num%2)
        num = num // 2
    return l

class Solution:
    def findMaximumXOR_wrong(self, nums: List[int]) -> int:
        '''
            it's even slower than bruteforce, what am I doing...
        '''
        def helper(left_nums, right_nums, i, prev_result):
            print(i, prev_result, len(left_nums), len(right_nums))
            if not left_nums or not right_nums:
                return 0
            if i==32:
                return int(prev_result, base=2)

            left_0 = [num for num in left_nums if num[i]=='0']
            left_1 = [num for num in left_nums if num[i]=='1']
            right_0 = [num for num in right_nums if num[i]=='0']
            right_1 = [num for num in right_nums if num[i]=='1']
            return max([
                helper(left_0, right_1, i+1, prev_result+'1'),
                helper(left_1, right_0, i+1, prev_result+'1'),
                helper(left_0, right_0, i+1, prev_result+'0'),
                helper(left_1, right_1, i+1, prev_result+'0'),
            ])

        bin_nums = [num_to_bin(num) for num in nums]

        return helper(bin_nums, bin_nums, i=0, prev_result='')

    def findMaximumXOR_trie(self, nums):
        trie = Trie()
        bin_nums = [num_to_bin(num) for num in nums]
        for bin_num in bin_nums:
            trie.insert(bin_num)

        max_xor = 0
        for bin_num in bin_nums:
            result = ''
            cur_node = trie.root
            for cur_bit in bin_num:
                wanted_bit = cur_bit ^ 1
                if wanted_bit in cur_node.children:
                    result += '1'
                    cur_node = cur_node.children[wanted_bit]
                else:
                    result += '0'
                    cur_node = cur_node.children[cur_bit]

            result = int(result, base=2)
            max_xor = max(result, max_xor)

        return max_xor

    def findMaximumXOR_bruteforce(self, nums):
        max_result = 0
        for i,num in enumerate(nums):
            j = i+1
            while j<len(nums):
                the_other = nums[j]
                this_result = num ^ the_other
                j += 1
                max_result = max(max_result, this_result)
        return max_result

    def findMaximumXOR(self, nums):
        return self.findMaximumXOR_trie(nums)

if __name__=='__main__':
    if lc_lib_path.exists():
        from testcase import testcase
        s = Solution()
        x = testcase
        r = s.findMaximumXOR_bruteforce(x)
        print('bruteforce:', r)
        r = s.findMaximumXOR_trie(x)
        print('trie:', r)
