
# load local leetcode lib
import pathlib

lc_lib_path = pathlib.Path('../lc_lib.py')
if lc_lib_path.exists():
    import sys
    sys.path.insert(0, '..')
    from lc_lib import *

from functools import lru_cache


class Solution:
    @lru_cache(1024)
    def wordBreak_set(self, s: str, wordDict) -> bool:
        same_candidates = [word for word in wordDict if s==word]
        if any(same_candidates):
            return True

        possible_candidates = [word for word in wordDict if s.startswith(word)]
        for possible_candidate in possible_candidates:
            r = self.wordBreak(s[len(possible_candidate):], wordDict)
            if r:
                return True

        return False

    def wordBreak(self, s: str, wordDict: List[str]) -> bool:
        return self.wordBreak_set(s, tuple(wordDict))

if __name__=='__main__':
    if lc_lib_path.exists():
        s = Solution()
        x = "applepenapple"
        wordDict = ["apple", "pen"]
        r = s.wordBreak(x, wordDict)
        print(r)

        x = "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaab"
        wordDict = ["a","aa","aaa","aaaa","aaaaa","aaaaaa","aaaaaaa","aaaaaaaa","aaaaaaaaa","aaaaaaaaaa"]
        r = s.wordBreak(x, wordDict)
        print(r)

        x = "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaabaabaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa"
        wordDict = ["aa","aaa","aaaa","aaaaa","aaaaaa","aaaaaaa","aaaaaaaa","aaaaaaaaa","aaaaaaaaaa","ba"]
        r = s.wordBreak(x, wordDict)
        print(r)
