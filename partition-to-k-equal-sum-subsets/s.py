
# load local leetcode lib
import pathlib
lc_lib_path = pathlib.Path('../lc_lib.py')
if lc_lib_path.exists():
    import sys
    sys.path.insert(0, '..')
    from lc_lib import *

old_print = print
def print(*args, **kwargs):
    if lc_lib_path.exists():
        old_print(*args, **kwargs)

class Solution:
    def canPartitionKSubsets(self, nums: List[int], k: int) -> bool:
        nums.sort(reverse=True)
        n = len(nums)
        s = sum(nums)
        if s%k!=0: return False
        group_sum = s//k
        if nums[0]>group_sum: return False
        groups = [0]*k
        result = False
        def dfs(i):
            nonlocal result
            if i==n:
                result = True
                return
            num = nums[i]
            for j in range(k):
                if (j>0 and groups[j]==groups[j-1]) or groups[j]+num>group_sum:
                    continue
                groups[j] += num
                dfs(i+1)
                groups[j] -= num
        dfs(0)
        return result

if __name__=='__main__':
    if lc_lib_path.exists():
        s = Solution()

        nums = [4,3,2,3,5,2,1]
        k = 4
        r = s.canPartitionKSubsets(nums,k)
        print(r)
        print('-'*30)

