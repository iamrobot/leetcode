
# load local leetcode lib
import pathlib

lc_lib_path = pathlib.Path('../lc_lib.py')
if lc_lib_path.exists():
    import sys
    sys.path.insert(0, '..')
    from lc_lib import *

from collections import Counter


class Solution:
    def threeSum(self, nums: List[int]) -> List[List[int]]:
        def twosum(nums, target):
            num_counter = Counter(nums)
            for num in num_counter:
                num_counter[num] -= 1
                the_other_one_num = target - num
                if num_counter[the_other_one_num]>0:
                    yield [num, the_other_one_num]

        if len(nums)<3:
            return []

        result = set()
        seen = set()
        for i,num in enumerate(nums):
            if num in seen:
                continue
            seen.add(num)
            for the_other_two in twosum(nums[:i]+nums[i+1:], -num):
                r = the_other_two + [num]
                result.add(tuple(sorted(r)))

        return [list(x) for x in result]

if __name__=='__main__':
    if lc_lib_path.exists():
        s = Solution()
        x = [-1,0,1,2,-1,-4]
        r = s.threeSum(x)
        print(r)
