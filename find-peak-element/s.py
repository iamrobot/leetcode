
# load local leetcode lib
import pathlib
lc_lib_path = pathlib.Path('../lc_lib.py')
if lc_lib_path.exists():
    import sys
    sys.path.insert(0, '..')
    from lc_lib import *

old_print = print
def print(*args, **kwargs):
    if lc_lib_path.exists():
        old_print(*args, **kwargs)

class Solution:
    def findPeakElement(self, nums: List[int]) -> int:
        inf = float('inf')
        nums = [-inf, *nums, -inf]
        for i,num in enumerate(nums):
            if num>nums[i-1] and num>nums[i+1]:
                return i-1

if __name__=='__main__':
    if lc_lib_path.exists():
        s = Solution()

        nums = [1,2,3,1]
        r = s.findPeakElement(nums)
        print(r)
        print('-'*30)

