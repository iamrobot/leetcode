
# load local leetcode lib
import pathlib

lc_lib_path = pathlib.Path('../lc_lib.py')
if lc_lib_path.exists():
    import sys
    sys.path.insert(0, '..')
    from lc_lib import *

class Solution:
    def merge(self, intervals: List[List[int]]) -> List[List[int]]:
        intervals = sorted(intervals, key=lambda x:x[0]) 
        to_remove = set()
        for i in range(len(intervals)-1):
            low_0, high_0 = intervals[i]
            low_1, high_1 = intervals[i+1]
            if low_1<=high_0:
                high = max(high_0, high_1) 
                merged = [low_0, high]
                to_remove.add(i)
                intervals[i+1] = merged

        result = []
        for i in range(len(intervals)):
            if i not in to_remove:
                result.append(intervals[i])
        return result

if __name__=='__main__':
    if lc_lib_path.exists():
        s = Solution()
        x = [[1,3],[2,6],[8,10],[15,18]]
        r = s.merge(x)
        print(r)
