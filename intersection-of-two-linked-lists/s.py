
# Definition for singly-linked list.
class ListNode:
    def __init__(self, x):
        self.val = x
        self.next = None

from typing import *

# Definition for singly-linked list.
# class ListNode:
#     def __init__(self, x):
#         self.val = x
#         self.next = None

class Solution:
    def getIntersectionNode(self, headA: ListNode, headB: ListNode) -> ListNode:
        a_node_set = set()
        while headA:
            a_node_set.add(headA)
            headA = headA.next

        while headB:
            if headB in a_node_set:
                break
            headB = headB.next

        return headB

if __name__=='__main__':
    s = Solution()
