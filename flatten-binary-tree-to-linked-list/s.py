
# load local leetcode lib
import pathlib

lc_lib_path = pathlib.Path('../lc_lib.py')
if lc_lib_path.exists():
    import sys
    sys.path.insert(0, '..')
    from lc_lib import *

# Definition for a binary tree node.
# class TreeNode:
#     def __init__(self, val=0, left=None, right=None):
#         self.val = val
#         self.left = left
#         self.right = right
class Solution:
    def flatten(self, root: TreeNode) -> None:
        """
        Do not return anything, modify root in-place instead.
        """
        def helper(root):
            if root==None:
                return None, None

            left_tree_flattened_head, left_tree_flattened_tail = helper(root.left)
            right_tree_flattened_head, right_tree_flattened_tail = helper(root.right)
            root.left = None
            if left_tree_flattened_head!=None:
                root.right = left_tree_flattened_head
                if right_tree_flattened_head!=None:
                    left_tree_flattened_tail.right = right_tree_flattened_head
                    return root, right_tree_flattened_tail
                else:
                    return root, left_tree_flattened_tail
            else:
                if right_tree_flattened_head!=None:
                    root.right = right_tree_flattened_head
                    return root, right_tree_flattened_tail
                else:
                    return root, root
                
        helper(root)

if __name__=='__main__':
    if lc_lib_path.exists():
        s = Solution()
        x = None # input
        r = s.method(x)
        print(r)
