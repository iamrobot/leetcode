
# load local leetcode lib
import pathlib

lc_lib_path = pathlib.Path('../lc_lib.py')
if lc_lib_path.exists():
    import sys
    sys.path.insert(0, '..')
    from lc_lib import *

old_print = print
def print(*args, **kwargs):
    if lc_lib_path.exists():
        old_print(*args, **kwargs)

from collections import defaultdict


class Solution:
    def combinationSum(self, candidates: List[int], target: int) -> List[List[int]]:
        sorted_candidates = sorted(candidates)
        results = []
        target_map = defaultdict(lambda:set())
        et = tuple()
        target_map[0].add(et)
        for i,k in enumerate(sorted_candidates):
            for j in range(k, target+1):
                for p in target_map[j-k]:
                    r = p+(k,)
                    target_map[j].add(r)

            print(dict(target_map))
            print('')
        
        return target_map[target]

if __name__=='__main__':
    if lc_lib_path.exists():
        s = Solution()
        x = [2,3,5]
        r = s.combinationSum(x, 8)
        print(r)
