
# load local leetcode lib
import pathlib

lc_lib_path = pathlib.Path('../lc_lib.py')
if lc_lib_path.exists():
    import sys
    sys.path.insert(0, '..')
    from lc_lib import *

old_print = print
def print(*args, **kwargs):
    if lc_lib_path.exists():
        old_print(*args, **kwargs)

# Definition for a binary tree node.
# class TreeNode(object):
#     def __init__(self, x):
#         self.val = x
#         self.left = None
#         self.right = None

def drawtree(root):
    def height(root):
        return 1 + max(height(root.left), height(root.right)) if root else -1
    def jumpto(x, y):
        t.penup()
        t.goto(x, y)
        t.pendown()
    def draw(node, x, y, dx):
        if node:
            t.goto(x, y)
            jumpto(x, y-20)
            t.write(node.val, align='center', font=('Arial', 12, 'normal'))
            draw(node.left, x-dx, y-60, dx/2)
            jumpto(x, y-20)
            draw(node.right, x+dx, y-60, dx/2)
    import turtle
    t = turtle.Turtle()
    t.speed(0); turtle.delay(0)
    h = height(root)
    jumpto(0, 30*h)
    draw(root, 0, 30*h, 40*h)
    t.hideturtle()
    canvas = turtle.getcanvas()
    canvas.postscript(file='/tmp/t.ps', colormode='color')
    turtle.done()
    #turtle.mainloop()

def dfs(root):
    if not root:
        return
    else:
        print(root.val)
        dfs(root.left)
        dfs(root.right)

from itertools import dropwhile
class Codec:

    def serialize(self, root):
        results = []
        level_nodes = [root]

        while level_nodes:
            if all([x==None for x in level_nodes]):
                break
            results.extend([node.val if node else None
                                for node in level_nodes])
            next_level_nodes = []
            for node in level_nodes:
                if node:
                    next_level_nodes += [node.left, node.right]
                else:
                    next_level_nodes += [None, None]
            level_nodes = next_level_nodes

        return results

    def deserialize(self, data):
        if not data:
            return None

        nodes = [None if val==None else TreeNode(val)
                  for val in data]
        kids = nodes[::-1]
        root = kids.pop()
        for node in nodes:
            if node:
                if kids: node.left = kids.pop()
                if kids: node.right = kids.pop()
        
        #drawtree(root)
        return root


if __name__=='__main__':
    if lc_lib_path.exists():
        ser = Codec()
        deser = Codec()

        null = None
        #x = [1,2,3,null,null,4,5]
        #ans = ser.serialize(deser.deserialize(x))
        #print(ans)

        x = []
        for i in range(1,1001):
            x.append(i)
            x.append(None)
        x = x[:-1]
        ans = ser.serialize(deser.deserialize(x))
        print(ans)
