
# load local leetcode lib
import pathlib
lc_lib_path = pathlib.Path('../lc_lib.py')
if lc_lib_path.exists():
    import sys
    sys.path.insert(0, '..')
    from lc_lib import *

old_print = print
def print(*args, **kwargs):
    if lc_lib_path.exists():
        old_print(*args, **kwargs)

class Solution:
    def checkPartitioning(self, s: str) -> bool:
        pals = set()
        n = len(s)
        for i,char in enumerate(s):
            p = i+1
            pals.add((i,i))

            j = 1
            while i-j>=0 and i+j<n:
                if s[i-j]==s[i+j]:
                    pals.add((i-j,i+j))
                else:
                    break
                j += 1

            if p<n and char==s[p]:
                pals.add((i,p))
                j = 1
                while i-j>=0 and p+j<n:
                    if s[i-j]==s[p+j]:
                        pals.add((i-j, p+j))
                    else:
                        break
                    j += 1
        for pal in pals:
            i,j = pal
            if i==0 or j==n-1:
                continue

            if (0,i-1) in pals and (j+1,n-1) in pals:
                return True

        return False

if __name__=='__main__':
    if lc_lib_path.exists():
        sol = Solution()

        s = "abcbdd"
        r = sol.checkPartitioning(s)
        print(r)
        print('-'*30)

        s = "juchzcedhfesefhdeczhcujzzvbmoeombv"
        r = sol.checkPartitioning(s)
        print(r)
        print('-'*30)

