
# load local leetcode lib
import pathlib
lc_lib_path = pathlib.Path('../lc_lib.py')
if lc_lib_path.exists():
    import sys
    sys.path.insert(0, '..')
    from lc_lib import *

old_print = print
def print(*args, **kwargs):
    if lc_lib_path.exists():
        old_print(*args, **kwargs)

import itertools
class Solution:
    def canEat(self, candiesCount: List[int], queries: List[List[int]]) -> List[bool]:
        candiesCount = list(itertools.accumulate(candiesCount))
        print(candiesCount)
        print('')

        results = []
        for q in queries:
            type, day, cap = q
            min_eat = day+1
            max_eat = cap*(day+1)
            i = 0 if type==0 else candiesCount[type-1]
            j = candiesCount[type]
            r = not (max_eat<=i or min_eat>j)
            results.append(r)
        return results

if __name__=='__main__':
    if lc_lib_path.exists():
        sol = Solution()

        candiesCount = [7,4,5,3,8]
        queries = [[0,2,2],[4,2,4],[2,13,1000000000]]
        r = sol.canEat(candiesCount,queries)
        print(r)
        print('-'*30)

