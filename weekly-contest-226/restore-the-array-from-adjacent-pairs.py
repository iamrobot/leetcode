
# load local leetcode lib
import pathlib
lc_lib_path = pathlib.Path('../lc_lib.py')
if lc_lib_path.exists():
    import sys
    sys.path.insert(0, '..')
    from lc_lib import *

old_print = print
def print(*args, **kwargs):
    if lc_lib_path.exists():
        old_print(*args, **kwargs)

import collections
class Solution:
    def restoreArray(self, adjacentPairs: List[List[int]]) -> List[int]:
        n = len(adjacentPairs)
        d = collections.defaultdict(lambda:[])
        for i,j in adjacentPairs:
            d[i].append(j)
            d[j].append(i)

        for k in d.keys():
            if len(d[k])==1:
                break

        l = [k]
        last = k
        seen = set([k])
        for i in range(n):
            neighbors = d[last]
            v = [x for x in neighbors if x not in seen][0]
            l.append(v)
            last = v
            seen.add(v)

        return l


if __name__=='__main__':
    if lc_lib_path.exists():
        sol = Solution()

        adjacentPairs = [[2,1],[3,4],[3,2]]
        r = sol.restoreArray(adjacentPairs)
        print(r)
        print('-'*30)

