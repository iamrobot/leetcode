
# load local leetcode lib
import pathlib
lc_lib_path = pathlib.Path('../lc_lib.py')
if lc_lib_path.exists():
    import sys
    sys.path.insert(0, '..')
    from lc_lib import *

old_print = print
def print(*args, **kwargs):
    if lc_lib_path.exists():
        old_print(*args, **kwargs)

import collections
class Solution:
    def countBalls(self, lowLimit: int, highLimit: int) -> int:
        c = collections.Counter()
        for i in range(lowLimit, highLimit+1):
            n = sum([int(j) for j in str(i)])
            c[n] += 1

        return c.most_common()[0][1]

if __name__=='__main__':
    if lc_lib_path.exists():
        sol = Solution()

        lowLimit = 5
        highLimit = 15
        r = sol.countBalls(lowLimit,highLimit)
        print(r)
        print('-'*30)

