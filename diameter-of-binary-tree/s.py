
# Definition for singly-linked list.
class ListNode:
    def __init__(self, x):
        self.val = x
        self.next = None

from typing import *


# Definition for a binary tree node.
class TreeNode:
    def __init__(self, val=0, left=None, right=None):
        self.val = val
        self.left = left
        self.right = right
class Solution:
    def diameterOfBinaryTree(self, root: TreeNode) -> int:
        diameter = 0
        def dfs(root):
            nonlocal diameter
            if not root:
                return 0
            left_depth = dfs(root.left)
            right_depth = dfs(root.right)
            depth = max(left_depth, right_depth) + 1
            print(f'depth of {root.val}: {depth}')
            this_tree_diameter = left_depth + right_depth
            diameter = max(diameter, this_tree_diameter)
            return depth

        dfs(root)
        return diameter

if __name__=='__main__':
    s = Solution()
