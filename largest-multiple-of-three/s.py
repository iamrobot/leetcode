
# load local leetcode lib
import pathlib
lc_lib_path = pathlib.Path('../lc_lib.py')
if lc_lib_path.exists():
    import sys
    sys.path.insert(0, '..')
    from lc_lib import *

old_print = print
def print(*args, **kwargs):
    if lc_lib_path.exists():
        old_print(*args, **kwargs)

class Solution:
    def largestMultipleOfThree(self, digits: List[int]) -> str:
        import itertools 
        digits = sorted(digits, reverse=True)
        seen = set()
        for i in range(len(digits), 0, -1):
            for selected_digits in itertools.combinations(digits, i):
                selected_digits_str = [str(d) for d in selected_digits]
                key = ''.join(selected_digits_str)
                if key in seen:
                    continue
                seen.add(key)

                if sum(selected_digits)%3==0:
                    if sum(selected_digits)==0:
                        return '0'
                    else:
                        return ''.join(selected_digits_str)
        else:
            return ''

if __name__=='__main__':
    if lc_lib_path.exists():
        sol = Solution()

        digits = [8,1,9]
        r = sol.largestMultipleOfThree(digits)
        print(r)
        print('-'*30)

        digits = [8,6,7,1,0]
        r = sol.largestMultipleOfThree(digits)
        print(r)
        print('-'*30)

        digits = [0]*6
        r = sol.largestMultipleOfThree(digits)
        print(r)
        print('-'*30)

