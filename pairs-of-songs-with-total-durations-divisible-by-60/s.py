
# load local leetcode lib
import pathlib
lc_lib_path = pathlib.Path('../lc_lib.py')
if lc_lib_path.exists():
    import sys
    sys.path.insert(0, '..')
    from lc_lib import *

old_print = print
def print(*args, **kwargs):
    if lc_lib_path.exists():
        old_print(*args, **kwargs)

class Solution:
    def numPairsDivisibleBy60(self, time: List[int]) -> int:
        import collections
        result = 0
        remains = collections.Counter([x%60 for x in time])

        if remains.get(0, 0)>0:
            r = remains[0]*(remains[0]-1) // 2
            if r:
                result += r

        if remains.get(30, 0)>0:
            r = remains[30]*(remains[30]-1) // 2
            if r:
                result += r

        for i in range(1, 30):
            r = remains.get(i, 0) * remains.get(60-i, 0)
            if r:
                result += r

        return result



if __name__=='__main__':
    if lc_lib_path.exists():
        sol = Solution()

        time = [30,20,150,100,40]
        r = sol.numPairsDivisibleBy60(time)
        print(r)
        print('-'*30)

        time = [60,60,60]
        r = sol.numPairsDivisibleBy60(time)
        print(r)
        print('-'*30)

