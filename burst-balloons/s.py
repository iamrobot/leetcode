
# load local leetcode lib
import pathlib
lc_lib_path = pathlib.Path('../lc_lib.py')
if lc_lib_path.exists():
    import sys
    sys.path.insert(0, '..')
    from lc_lib import *

old_print = print_line
def print(*args, **kwargs):
    if lc_lib_path.exists():
        old_print(*args, **kwargs)

class Solution:
    def maxCoins(self, nums: List[int]) -> int:
        def burst(nums, i):
            if i==0:
                left = 1
            else:
                left = nums[i-1]

            if i==len(nums)-1:
                right = 1
            else:
                right = nums[i+1]

            return nums[i]*left*right

        mem = {}
        def helper(nums):
            if tuple(nums) in mem.keys():
                # print(tuple(nums), 'found in mem')
                return mem[tuple(nums)]

            if len(nums)==1:
                return nums[0]
            
            max_coins = 0
            for i,num in enumerate(nums):
                left_if_burst_i = nums[:i] + nums[i+1:]
                coins_if_burst_i = burst(nums, i) + helper(left_if_burst_i)
                max_coins = max(max_coins, coins_if_burst_i)

            mem[tuple(nums)] = max_coins

            return max_coins

        return helper(nums)

if __name__=='__main__':
    if lc_lib_path.exists():
        sol = Solution()

        nums = [3,1,5,8]
        r = sol.maxCoins(nums)
        print(r)
        print('-'*30)

        nums = [9,76,64,21,97,60] # 1086136
        r = sol.maxCoins(nums)
        print(r)
        print('-'*30)

        nums = [8,3,4,3,5,0,5,6,6,2,8,5,6,2,3,8,3,5,1,0,2] # 1086136
        r = sol.maxCoins(nums)
        print(r)
        print('-'*30)

