
# load local leetcode lib
import pathlib
lc_lib_path = pathlib.Path('../lc_lib.py')
if lc_lib_path.exists():
    import sys
    sys.path.insert(0, '..')
    from lc_lib import *

old_print = print
def print(*args, **kwargs):
    if lc_lib_path.exists():
        old_print(*args, **kwargs)

class Solution:
    def subarraySum(self, nums: List[int], k: int) -> int:
        import collections
        result = 0
        target = k

        cumsums = list(itertools.accumulate(nums))
        d = collections.defaultdict(lambda: 0)
        d[0] = 1
        s = 0
        for num in nums:
            s += num
            result += d[s-target] # 2, 5
            d[s] += 1
        return result
        


if __name__=='__main__':
    if lc_lib_path.exists():
        sol = Solution()

        nums = [1,1,1]
        k = 2
        r = sol.subarraySum(nums,k)
        print(r)
        print('-'*30)

