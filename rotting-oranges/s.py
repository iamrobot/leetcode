

class Matrix:
    def __init__(self, matrix):
        self.matrix = matrix
        self.m = len(matrix)
        self.n = len(matrix[0]) if self.m else None

    def __getattr__(self, item):
        return getattr(self.matrix, item)

    def __getitem__(self, item):
        return self.matrix[item]

    def __setitem__(self, key, value):
        self.matrix[key] = value

    def __repr__(self):
        return repr(self.matrix)

    def __str__(self):
        return str(self.matrix)

    def __call__(self, *args):
        if len(args)==1:
            i,j = args[0]
        elif len(args)==2:
            i,j = args
        else:
            msg = 'you can get matrix item by matrix[i][j], matrix(i,j) or matrix((i,j))'
            raise Exception(msg)
        return self[i][j]

    def __iter__(self):
        m = self.m
        n = self.n
        for i in range(m):
            for j in range(n):
                pos = (i,j)
                yield pos, self(pos)

    def is_pos_in_matrix(self, pos):
        m = self.m
        n = self.n
        i,j = pos
        return 0<=i<m and 0<=j<n

    def get_neighbors(self, pos, including_diag=False):
        i,j = pos
        neighbors = [
                (i, j+1),
                (i, j-1),
                (i+1, j),
                (i-1, j),
                ]
        if including_diag:
            neighbors += [
                    (i-1, j-1),
                    (i-1, j+1),
                    (i+1, j-1),
                    (i+1, j+1)
                    ]
        return [pos for pos in neighbors if self.is_pos_in_matrix(pos)]

    def zeros_like(self):
        rows = self.m
        cols = self.n
        if rows==0: return []
        return [[0]*cols for _ in range(rows)]

# load local leetcode lib
import pathlib
lc_lib_path = pathlib.Path('../lc_lib.py')
if lc_lib_path.exists():
    import sys
    sys.path.insert(0, '..')
    from lc_lib import *

old_print = print
def print(*args, **kwargs):
    if lc_lib_path.exists():
        old_print(*args, **kwargs)

# 0 representing an empty cell,
# 1 representing a fresh orange, or
# 2 representing a rotten orange.

class Solution:
    def orangesRotting(self, grid: List[List[int]]) -> int:
        import collections
        matrix = Matrix(grid)
        vals = collections.Counter([val for pos,val in matrix])
        if vals[1]==0:
            return 0

        for step in range(0, 10000):
            matrix = Matrix(grid)
            val2pos = collections.defaultdict(lambda:[])
            for pos,val in matrix:
                val2pos[val].append(pos)

            if not val2pos[1]:
                return step

            rotten_this_minute = []
            for pos in val2pos[1]:
                neibor_pos_l = matrix.get_neighbors(pos)
                neibors = [matrix(p) for p in neibor_pos_l]
                if 2 in neibors:
                    rotten_this_minute.append(pos)

            if not rotten_this_minute:
                return -1

            for pos in rotten_this_minute:
                i,j = pos
                grid[i][j] = 2
                        

if __name__=='__main__':
    if lc_lib_path.exists():
        sol = Solution()

        grid = [[2,1,1],[1,1,0],[0,1,1]]
        r = sol.orangesRotting(grid)
        print(r)
        print('-'*30)

        grid = [[2,1,1],[0,1,1],[1,0,1]]
        r = sol.orangesRotting(grid)
        print(r)
        print('-'*30)

        grid = [[0,2]]
        r = sol.orangesRotting(grid)
        print(r)
        print('-'*30)

