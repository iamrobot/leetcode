
# load local leetcode lib
import pathlib
lc_lib_path = pathlib.Path('../lc_lib.py')
if lc_lib_path.exists():
    import sys
    sys.path.insert(0, '..')
    from lc_lib import *

old_print = print
def print(*args, **kwargs):
    if lc_lib_path.exists():
        old_print(*args, **kwargs)

class Solution:
    def minDistance(self, word1: str, word2: str) -> int:
        m = len(word1)
        n = len(word2)
        dp = [[0]*(n+1) for _ in range(m+1)]
        for j in range(n+1):
            dp[0][j] = j
        for i in range(m+1):
            dp[i][0] = i

        for i in range(1, m+1):
            char1 = word1[i-1]
            for j in range(1, n+1):
                char2 = word2[j-1]
                if char1==char2:
                    dp[i][j] = dp[i-1][j-1]
                else:
                    dp[i][j] = min(dp[i-1][j-1], dp[i-1][j], dp[i][j-1])+1

        return dp[-1][-1]

        
if __name__=='__main__':
    if lc_lib_path.exists():
        s = Solution()

        word1 = "horse this is impossible /home/jlz/.pip/"
        word2 = "rose impossible /home/"
        r = s.minDistance(word1,word2)
        print(r)
        print('-'*30)

        word1 = "rose"
        word2 = ""
        r = s.minDistance(word1,word2)
        print(r)
        print('-'*30)

        word1 = "intention"
        word2 = "execution"
        r = s.minDistance(word1,word2)
        print(r)
        print('-'*30)

