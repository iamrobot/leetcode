
# load local leetcode lib
import pathlib
lc_lib_path = pathlib.Path('../lc_lib.py')
if lc_lib_path.exists():
    import sys
    sys.path.insert(0, '..')
    from lc_lib import *

old_print = print
def print(*args, **kwargs):
    if lc_lib_path.exists():
        old_print(*args, **kwargs)

def is_pal(s):
    n = len(s)
    i = 0
    j = n-1
    while i<j:
        if s[i]==s[j]:
            i += 1
            j -= 1
        else:
            return False
    return True

from sortedcontainers import SortedList, SortedDict
from collections import defaultdict
class Solution:
    def partition(self, s: str) -> List[List[str]]:
        n = len(s)
        result = []
        def dfs(start, prev_list):
            nonlocal result
            if start==n:
                result.append(prev_list)
            for i in range(start+1, n+1):
                t = s[start:i]
                if is_pal(t):
                    dfs(i, [*prev_list, t])
        dfs(0, [])
        return result


    def partition_2(self, s: str) -> List[List[str]]:
        n = len(s)
        pals = defaultdict(lambda:[])
        for i,char in enumerate(s):
            j = 0
            while i-j>=0 and i+j<n:
                if s[i-j]==s[i+j]:
                    pals[i-j].append(i+j)
                else:
                    break
                j += 1
            if i!=n-1 and char==s[i+1]:
                j=0
                while i-j>=0 and i+1+j<n:
                    if s[i-j]==s[i+1+j]:
                        pals[i-j].append(i+1+j)
                    else:
                        break
                    j += 1


        def helper(cur, prev):
            if cur>=n:
                yield prev

            for end in pals[cur]:
                t = s[cur:end+1]
                yield from helper(end+1, [*prev, t])

        return list(helper(0, []))


if __name__=='__main__':
    if lc_lib_path.exists():
        s = Solution()

        x = "aabb"
        r = s.partition(x)
        print(r)
        print('-'*30)

        x = "efe"
        r = s.partition(x)
        print(r)
        print('-'*30)

