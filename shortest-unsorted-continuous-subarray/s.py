
# load local leetcode lib
import pathlib

lc_lib_path = pathlib.Path('../lc_lib.py')
if lc_lib_path.exists():
    import sys
    sys.path.insert(0, '..')
    from lc_lib import *

class Solution:
    def findUnsortedSubarray(self, nums: List[int]) -> int:
        if len(nums)<2:
            return 0

        sorted_nums = sorted(nums)
        i = 0
        while sorted_nums[i]==nums[i]:
            i += 1
            if i==len(nums):
                break

        j = len(nums)-1
        while sorted_nums[j]==nums[j]:
            if j<=i:
                break
            j -= 1

        return j-i+1

if __name__=='__main__':
    if lc_lib_path.exists():
        s = Solution()
        x = [2,6,4,8,10,9,15]
        r = s.findUnsortedSubarray(x)
        print(r)

        x = [1,2,3,4]
        r = s.findUnsortedSubarray(x)
        print(r)
