
# load local leetcode lib
import pathlib

lc_lib_path = pathlib.Path('../lc_lib.py')
if lc_lib_path.exists():
    import sys
    sys.path.insert(0, '..')
    from lc_lib import *

class Solution:
    def canJump(self, nums: List[int]) -> bool:
        far = 0
        for i,num in enumerate(nums):
            if i<=far:
                far = max(far, i+num)
                if far>=len(nums)-1:
                    return True
            else:
                return False

if __name__=='__main__':
    if lc_lib_path.exists():
        s = Solution()
        x = generate_int_list(shuffle=True)
        r = s.canJump(x)
        print(r)

        x = [2,3,1,1,4]
        r = s.canJump(x)
        print(r)
