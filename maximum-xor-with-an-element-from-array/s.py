
# load local leetcode lib
import pathlib

lc_lib_path = pathlib.Path('../lc_lib.py')
if lc_lib_path.exists():
    import sys
    sys.path.insert(0, '..')
    from lc_lib import *

def num_to_bin(num, fix_len=32):
    l = []
    for i in range(fix_len):
        l.insert(0, num%2)
        num = num // 2
    return l


from collections import defaultdict


class TrieNode(object):
    def __init__(self):
        self.children = defaultdict(lambda:TrieNode())

class Trie(object):
    def __init__(self):
        self.root = TrieNode()

    def insert(self, word):
        cur_node = self.root
        for char in word:
            x = cur_node.children[char] 
            cur_node = x
class Solution:
    def maximizeXor(self, nums: List[int], queries: List[List[int]]) -> List[int]:
        nums = sorted(nums)
        min_num = nums[0]
        queries_index, queries = zip(*sorted(enumerate(queries), key=lambda x:x[1][-1]))
        num_i = 0
        results = []
        trie = Trie()
        for query in queries:
            x, m = query
            if m<min_num:
                results.append(-1)
                continue
            this_result = ''
            while num_i<len(nums) and nums[num_i]<=m:
                num = nums[num_i]
                bin_num = num_to_bin(num)
                trie.insert(bin_num)
                num_i += 1
            bin_x = num_to_bin(x)
            cur = trie.root
            for x_bit in bin_x:
                wanted_bit = x_bit ^ 1
                if wanted_bit in cur.children:
                    this_result += '1'
                    cur = cur.children[wanted_bit]
                else:
                    this_result += '0'
                    cur = cur.children[x_bit]
            this_result = int(this_result, base=2)
            results.append(this_result)
        
        results = sorted(zip(queries_index, results), key=lambda x:x[0])
        results = [x[1] for x in results]
        return results

if __name__=='__main__':
    if lc_lib_path.exists():
        s = Solution()
        nums = [0,1,2,3,4]
        queries = [[3,1],[1,3],[5,6]]
        r = s.maximizeXor(nums, queries)
        print(r)
        nums = [5,2,4,6,6,3]
        queries = [[12,4],[8,1],[6,3]]
        r = s.maximizeXor(nums, queries)
        print(r)
