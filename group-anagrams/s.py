
# load local leetcode lib
import pathlib

lc_lib_path = pathlib.Path('../lc_lib.py')
if lc_lib_path.exists():
    import sys
    sys.path.insert(0, '..')
    from lc_lib import *

import string
from collections import defaultdict


class Solution:
    def groupAnagrams(self, strs: List[str]) -> List[List[str]]:
        d = {char:i for i,char in enumerate(string.ascii_lowercase)}
        result = defaultdict(lambda: [])
        for s in strs:
            key = [0]*26
            for char in s:
                i = d[char]
                key[i] += 1
            key = tuple(key)
            this_anagram_l = result[key]
            this_anagram_l.append(s)
            result[key] = this_anagram_l
        
        return [v for k,v in result.items()]

if __name__=='__main__':
    if lc_lib_path.exists():
        s = Solution()
        x = ["eat","tea","tan","ate","nat","bat"]
        r = s.groupAnagrams(x)
        print(r)
