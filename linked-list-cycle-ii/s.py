
# load local leetcode lib
import pathlib

lc_lib_path = pathlib.Path('../lc_lib.py')
if lc_lib_path.exists():
    import sys
    sys.path.insert(0, '..')
    from lc_lib import *

# Definition for singly-linked list.
# class ListNode:
#     def __init__(self, x):
#         self.val = x
#         self.next = None

class Solution:
    def detectCycle(self, head: ListNode) -> ListNode:
        seen = set()
        p = head
        i = 0
        while p:
            if p in seen:
                return p
            seen.add(p)
            i += 1 
            p = p.next
        return None

if __name__=='__main__':
    if lc_lib_path.exists():
        s = Solution()
        x = None # input
        r = s.method(x)
        print(r)
