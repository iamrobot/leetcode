
# load local leetcode lib
import pathlib

lc_lib_path = pathlib.Path('../lc_lib.py')
if lc_lib_path.exists():
    import sys
    sys.path.insert(0, '..')
    from lc_lib import *

old_print = print
def print(*args, **kwargs):
    if lc_lib_path.exists():
        old_print(*args, **kwargs)

class Solution:
    def maxProfit(self, prices: List[int]) -> int:
        n = len(prices)
        if n<2:
            return 0

        hold = [0]*(n+3)
        not_hold = [0]*(n+3)

        for i,price in enumerate(prices):
            if i==0:
                continue
            prev_price = prices[i-1]
            hold[i] = max(not_hold[i-1], hold[i-1]+price-prev_price)
            not_hold[i] = max(not_hold[i-1], hold[i-1])

        return max(max(hold), max(not_hold))
                    
if __name__=='__main__':
    if lc_lib_path.exists():
        s = Solution()
        x = [1,2,3,0,2]
        r = s.maxProfit(x)
        print(r)
        print('-'*30)
        x = [6,1,3,2,4,7]
        r = s.maxProfit(x)
        print(r)
        print('-'*30)
        x = [2,6,8,7,8,7,9,4,1,2,4,5,8]
        r = s.maxProfit(x)
        print(r)
