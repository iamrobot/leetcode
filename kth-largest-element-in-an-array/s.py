
# load local leetcode lib
import pathlib

lc_lib_path = pathlib.Path('../lc_lib.py')
if lc_lib_path.exists():
    import sys
    sys.path.insert(0, '..')
    from lc_lib import *

import heapq


class Solution:
    def findKthLargest(self, nums: List[int], k: int) -> int:
        heap = []
        for i,num in enumerate(nums[:k]):
            num = num
            heap.append(num)

        heapq.heapify(heap)
    
        for num in nums[k:]:
            if num>heap[0]:
                heapq.heapreplace(heap, num)
        return heap[0]


if __name__=='__main__':
    if lc_lib_path.exists():
        s = Solution()
        x = [3,2,1,5,6,4]
        r = s.findKthLargest(x, 2)
        print(r)

        x = [3,2,3,1,2,4,5,5,6]
        r = s.findKthLargest(x, 4)
        print(r)
