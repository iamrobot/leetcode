
# load local leetcode lib
import pathlib

lc_lib_path = pathlib.Path('../lc_lib.py')
if lc_lib_path.exists():
    import sys
    sys.path.insert(0, '..')
    from lc_lib import *

class Solution:
    def searchMatrix(self, matrix: List[List[int]], target: int) -> bool:
        m = len(matrix)
        n = len(matrix[0])
        candidates = []
        def get_col(col):
            l = []
            for i in range(m):
                l.append(matrix[i][col])
            return l

        for i in range(min(m,n)):
            if matrix[i][i]<target:
                continue
            elif matrix[i][i]==target:
                return True
            else:
                break
        
        sep = i

        for i in range(sep, m):
            candidates += matrix[i]
        for i in range(sep, n):
            candidates += get_col(i)

        print(candidates)

        return target in candidates

if __name__=='__main__':
    if lc_lib_path.exists():
        s = Solution()
        s.method()
