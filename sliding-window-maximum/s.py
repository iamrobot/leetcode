
# load local leetcode lib
import pathlib

lc_lib_path = pathlib.Path('../lc_lib.py')
if lc_lib_path.exists():
    import sys
    sys.path.insert(0, '..')
    from lc_lib import *

old_print = print
def print(*args, **kwargs):
    if lc_lib_path.exists():
        old_print(*args, **kwargs)

import bisect


class Solution:
    def maxSlidingWindow(self, nums: List[int], k: int) -> List[int]:
        if k==1:
            return nums
        l = []
        results = []
        for i in range(k-1, len(nums)):
            if i==k-1:
                l = sorted(nums[:k])
            else:
                l.pop(bisect.bisect_left(l, nums[i-k]))
                bisect.insort_left(l, nums[i])
            results.append(l[-1])
    
        return results

if __name__=='__main__':
    if lc_lib_path.exists():
        s = Solution()

        nums = [1,3,-1,-3,5,3,6,7]
        k = 3
        r = s.maxSlidingWindow(nums,k)
        print(r)
        print('-'*30)

