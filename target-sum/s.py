
# load local leetcode lib
import pathlib

lc_lib_path = pathlib.Path('../lc_lib.py')
if lc_lib_path.exists():
    import sys
    sys.path.insert(0, '..')
    from lc_lib import *

import math
from functools import lru_cache


class Solution:
    def findTargetSumWays(self, nums: List[int], S: int) -> int:

        @lru_cache(1024)
        def helper(nums, target):
            num_of_ways = 0
            n0 = nums[0]

            if len(nums)==1:
                if abs(n0)==abs(target):
                    return 2 if n0==0 else 1
                else:
                    return 0

            num_of_ways += helper(nums[1:], target+n0)
            num_of_ways += helper(nums[1:], target-n0)

            return num_of_ways

        return helper(tuple(nums), S)

if __name__=='__main__':
    if lc_lib_path.exists():
        s = Solution()
        nums = [47,30,12,40,10,31,5,12,14,25,45,34,34,31,20,1,33,28,30,30]
        S = 36
        print(s.findTargetSumWays(nums, S))


