
# load local leetcode lib
import pathlib

lc_lib_path = pathlib.Path('../lc_lib.py')
if lc_lib_path.exists():
    import sys
    sys.path.insert(0, '..')
    from lc_lib import *

# Definition for singly-linked list.
# class ListNode:
#     def __init__(self, val=0, next=None):
#         self.val = val
#         self.next = next
class Solution:
    def removeNthFromEnd(self, head: ListNode, n: int) -> ListNode:
        l = []
        p = head
        while p:
            l.append(p)
            p = p.next

        if len(l)==n:
            return head.next
        elif len(l)>n:
            l[-n-1].next = l[-n].next
            return head
        else:
            return head

if __name__=='__main__':
    if lc_lib_path.exists():
        s = Solution()
        x = linked_list_from_list([1,2,3,4,5])
        r = s.removeNthFromEnd(x, 2)
        print_linked_list(r)

        x = linked_list_from_list([1])
        r = s.removeNthFromEnd(x, 2)
        print_linked_list(r)

        x = linked_list_from_list([1])
        r = s.removeNthFromEnd(x, 1)
        print_linked_list(r)

        x = linked_list_from_list([1, 2])
        r = s.removeNthFromEnd(x, 1)
        print_linked_list(r)
