
# load local leetcode lib
import pathlib

lc_lib_path = pathlib.Path('../lc_lib.py')
if lc_lib_path.exists():
    import sys
    sys.path.insert(0, '..')
    from lc_lib import *

import string


def find_until(s, start_index, f):
    index = start_index
    while index<len(s) and f(s[index]):
        index += 1
    return start_index, index
    
class Solution:
    def decodeString(self, s: str) -> str:
        def helper(s):
            index = 0
            result = ''
            count = 0
            while index<len(s):
                char = s[index]
                if char in string.digits:
                    start_index, index = find_until(s, index, lambda char: char in string.digits)
                    index = index
                    count = int(s[start_index:index])
                elif char in string.ascii_lowercase:
                    start_index, index = find_until(s, index, lambda char: char in string.ascii_lowercase)
                    index = index
                    result += s[start_index:index]
                elif char=='[':
                    left = 1
                    start_index = index+1
                    while left!=0:
                        index += 1
                        if s[index]=='[':
                            left += 1
                        elif s[index]==']':
                            left -= 1
                    end_index = index
                    result += count*helper(s[start_index:end_index])
                    count = 0
                    index += 1

            return result
                
        return helper(s)

if __name__=='__main__':
    if lc_lib_path.exists():
        s = Solution()
        x = '3[a]2[bc]'
        r = s.decodeString(x)
        print(r)

        x = '3[a2[c]]'
        r = s.decodeString(x)
        print(r)
