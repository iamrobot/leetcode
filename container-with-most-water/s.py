
# load local leetcode lib
import pathlib

lc_lib_path = pathlib.Path('../lc_lib.py')
if lc_lib_path.exists():
    import sys
    sys.path.insert(0, '..')
    from lc_lib import *

from collections import defaultdict


class Solution:
    def maxArea(self, height: List[int]) -> int:
        return self.maxArea_two_pointers(height)

    def maxArea_two_pointers(self, height: List[int]) -> int:
        def get_area(i, j):
            return abs(i-j)*min(height[i], height[j])

        left = 0
        right = len(height)-1
        max_area = 0
        while left<right:
            left_h = height[left]
            right_h = height[right]
            max_area = max(max_area, get_area(left,right))

            tmpleft = left+1
            while tmpleft<right and height[tmpleft]<=left_h:
                max_area = max(max_area, get_area(tmpleft, left))
                tmpleft += 1
            max_area = max(max_area, get_area(tmpleft, right))
            max_area = max(max_area, get_area(tmpleft, left))

            tmpright = right-1
            while tmpright>left and height[tmpright]<=right_h:
                max_area = max(max_area, get_area(tmpright, right))
                tmpright -= 1
            max_area = max(max_area, get_area(tmpright, left))
            max_area = max(max_area, get_area(tmpright, right))

            left = tmpleft
            right = tmpright

        return max_area 

    def maxArea_sort(self, height: List[int]) -> int:
        inf = float('inf')
        hmap = defaultdict(lambda:[inf, -inf])
        for index,h in enumerate(height):
            hmap[h][0] = min(index, hmap[h][0])
            hmap[h][1] = max(index, hmap[h][1])

        sorted_height_set = sorted(set(height), reverse=True)
        i = 0
        max_area = 0
        min_j = inf
        max_j = -inf

        for h in sorted_height_set:
            min_j = min(min_j, hmap[h][0])
            max_j = max(max_j, hmap[h][1])
            max_area = max(max_area, (max_j-min_j)*h)

        return max_area

if __name__=='__main__':
    if lc_lib_path.exists():
        s = Solution()
        x = [1,8,6,2,5,4,8,3,7]
        r = s.maxArea(x)
        print(r)
        print('---')
        x = [1,1]
        r = s.maxArea(x)
        print(r)
        print('---')
        x = [1,2,3,4,5,6,7,8,9,10]
        r = s.maxArea(x)
        print(r)
        print('---')
        x = [1,3,2,5,25,24,5]
        r = s.maxArea(x)
        print(x)
        print(r)
        print('---')
