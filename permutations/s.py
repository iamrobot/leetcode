
# load local leetcode lib
import pathlib

lc_lib_path = pathlib.Path('../lc_lib.py')
if lc_lib_path.exists():
    import sys
    sys.path.insert(0, '..')
    from lc_lib import *

from copy import deepcopy


class Solution:
    def permute(self, nums: List[int]) -> List[List[int]]:
        def helper(nums):
            nums = deepcopy(nums)
            if len(nums)==1:
                yield nums
            else:
                num_0 = nums[0]
                for x in self.permute(nums[1:]):
                    for i in range(len(x)+1):
                        copied_x = deepcopy(x)
                        copied_x.insert(i, num_0)
                        yield copied_x
            
        return list(helper(nums))

if __name__=='__main__':
    if lc_lib_path.exists():
        s = Solution()
        x = [1,2,3]
        r = s.permute(x)
        print(r)
