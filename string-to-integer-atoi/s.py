
# load local leetcode lib
import pathlib
lc_lib_path = pathlib.Path('../lc_lib.py')
if lc_lib_path.exists():
    import sys
    sys.path.insert(0, '..')
    from lc_lib import *

old_print = print
def print(*args, **kwargs):
    if lc_lib_path.exists():
        old_print(*args, **kwargs)

import string
class Solution:
    def myAtoi(self, s: str) -> int:
        result = 0
        sign = lambda x:x
        result_max = 2**31-1
        result_min = -(2**31)
        s = s.lstrip() 
        if len(s)==0:
            return 0
        if s[0] not in string.digits+'+-':
            return 0

        if s[0] == '-':
            sign = lambda x:-x
            i = 1
        elif s[0] == '+':
            sign = lambda x:x
            i = 1
        else: 
            i = 0

        result_string = ''
        while i<len(s) and s[i] in string.digits:
            result_string += s[i]
            i += 1
        if result_string:
            result = int(result_string)

        result = sign(result)
        if result>result_max:
            result = result_max
        if result<result_min:
            result = result_min
            
        return result


if __name__=='__main__':
    if lc_lib_path.exists():
        sol = Solution()

        s = "   -"
        r = sol.myAtoi(s)
        print(r)
        print('-'*30)

        s = "91283472332"
        r = sol.myAtoi(s)
        print(r)
        print('-'*30)

        s = "words and 987"
        r = sol.myAtoi(s)
        print(r)
        print('-'*30)

