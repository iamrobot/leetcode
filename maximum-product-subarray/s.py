
# load local leetcode lib
import pathlib

lc_lib_path = pathlib.Path('../lc_lib.py')
if lc_lib_path.exists():
    import sys
    sys.path.insert(0, '..')
    from lc_lib import *

from itertools import takewhile, accumulate
import operator
class Solution:
    def maxProduct(self, nums: List[int]) -> int:
        result = nums[0]

        prev_max, prev_min = nums[0], nums[0]
        for num in nums[1:]:
            tmp = num, num*prev_max, num*prev_min
            prev_max, prev_min = max(tmp), min(tmp)
            result = max(result, prev_max)

        return result


if __name__=='__main__':
    if lc_lib_path.exists():
        s = Solution()
        x = [2,-1,1,1]
        r = s.maxProduct(x)
        print(r)

        x = [2,3,-2,4]
        r = s.maxProduct(x)
        print(r)

        x = [-2,0,-1]
        r = s.maxProduct(x)
        print(r)

        x = [0,2]
        r = s.maxProduct(x)
        print(r)

        x = [3,-1,4]
        r = s.maxProduct(x)
        print(r)

        x = [-2]
        r = s.maxProduct(x)
        print(r)
