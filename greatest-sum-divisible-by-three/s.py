
# load local leetcode lib
import pathlib
lc_lib_path = pathlib.Path('../lc_lib.py')
if lc_lib_path.exists():
    import sys
    sys.path.insert(0, '..')
    from lc_lib import *

old_print = print
def print(*args, **kwargs):
    if lc_lib_path.exists():
        old_print(*args, **kwargs)

class Solution:
    def maxSumDivThree(self, nums: List[int]) -> int:
        '''
        https://leetcode.com/problems/greatest-sum-divisible-by-three/discuss/431077/JavaC%2B%2BPython-One-Pass-O(1)-space
        '''
        max_sum = [0] * 3
        for num in nums:
            for i in max_sum[:]: # 必须写成max_sum[:]，相当于复制了一份
                j = (i+num) % 3
                max_sum[j] = max(max_sum[j], i+num)
                print(num, i, max_sum)
        return max_sum[0]

    def maxSumDivThree_2(self, nums: List[int]) -> int:
        max_sum = [0] * 3
        for num in nums:
            next_max_sum = list(max_sum)

            for i in range(0,3):
                if max_sum[i]:
                    j = (i+num) % 3
                    next_max_sum[j] = max(max_sum[j], max_sum[i]+num)

                print(num, i, next_max_sum)

            k = num % 3
            next_max_sum[k] = max(num, next_max_sum[k])
            print(num, k, next_max_sum)
            print('')

            max_sum = next_max_sum

        return max_sum[0]


if __name__=='__main__':
    if lc_lib_path.exists():
        sol = Solution()

        nums = [3,6,5,1,8]
        r = sol.maxSumDivThree(nums)
        print(r)
        print('-'*30)

