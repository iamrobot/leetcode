
# load local leetcode lib
import pathlib
lc_lib_path = pathlib.Path('../lc_lib.py')
if lc_lib_path.exists():
    import sys
    sys.path.insert(0, '..')
    from lc_lib import *

old_print = print
def print(*args, **kwargs):
    if lc_lib_path.exists():
        old_print(*args, **kwargs)

from collections import deque
class Solution:
    def longestValidParentheses(self, s: str) -> int:
        lc = '('
        rc = ')'
        def is_valid(s):
            left = 0
            for char in s:
                if char==rc:
                    if left==0:
                        return False
                    else:
                        left-=1
                else:
                    left += 1
            return left==0

        def bruteforce(s):
            n = len(s)
            max_valid = 0
            for i in range(n):
                for j in range(i+1,n+1):
                    if is_valid(s[i:j]):
                        max_valid = max(max_valid, j-i) 
            return max_valid

        def helper(s):
            n = len(s)
            return 0

        for i in range(1000):
            print('---')
            s = list(generate_list('()', length=12))
            s = ''.join(s)
            bruteforce_result = bruteforce(s)
            result = helper(s)
            print(s, bruteforce(s), helper(s))
            if bruteforce_result!=result:
                break
                    
if __name__=='__main__':
    if lc_lib_path.exists():
        sol = Solution()

        s = "(()"
        r = sol.longestValidParentheses(s)
        print(r)
        print('-'*30)

