
# load local leetcode lib
import pathlib

lc_lib_path = pathlib.Path('../lc_lib.py')
if lc_lib_path.exists():
    import sys
    sys.path.insert(0, '..')
    from lc_lib import *

old_print = print
def print(*args, **kwargs):
    if lc_lib_path.exists():
        old_print(*args, **kwargs)

from collections import Counter


class Solution:
    def countPairs(self, deliciousness: List[int]) -> int:
        results = 0
        c = Counter(deliciousness)
        powers = [2**i for i in range(22)]
        for power in powers:
            for key in c:
                if key==power-key:
                    r = c[key]*(c[key]-1)
                else:
                    r = c[key]*c[power-key]
                if r:
                    print(key, power-key)
                    print(c[key], c[power-key])
                    print('')
                results += r

        results = results // 2
        m = 10**9 + 7
        return results % m

if __name__=='__main__':
    if lc_lib_path.exists():
        s = Solution()

        deliciousness = [1,3,5,7,9]
        r = s.countPairs(deliciousness)
        print(r)
        print('-'*30)

        deliciousness = [1,1,1,3,3,3,7]
        r = s.countPairs(deliciousness)
        print(r)
        print('-'*30)

