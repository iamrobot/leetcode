
# load local leetcode lib
import pathlib

lc_lib_path = pathlib.Path('../lc_lib.py')
if lc_lib_path.exists():
    import sys
    sys.path.insert(0, '..')
    from lc_lib import *

old_print = print
def print(*args, **kwargs):
    if lc_lib_path.exists():
        old_print(*args, **kwargs)

import bisect
from collections import defaultdict


class Solution:
    def minOperations_tails(self, target: List[int], arr: List[int]) -> int:
        inf = float('inf')
        vi_map = {v:i for i,v in enumerate(target)}
        arr = [vi_map[x] for x in arr if x in vi_map]

        tails = [inf]
        for val in arr:
            if val>tails[-1]:
                tails.append(val)
            else:
                i = bisect.bisect_left(tails, val)
                tails[i] = val

        return len(target)-len(tails)

    def minOperations(self, target: List[int], arr: List[int]) -> int:
        inf = float('inf')
        vi_map = {v:i for i,v in enumerate(target)}
        arr = [vi_map[x] for x in arr if x in vi_map]

        val_to_max_is_len = defaultdict(lambda:0)
        for val in arr:
            max_is_len = 1
            to_remove_keys = []
            for k,v in val_to_max_is_len.items():
                if k<val:
                    max_is_len = max(max_is_len, v+1)
                elif v<=max_is_len:
                    to_remove_keys.append(k)
            for k in to_remove_keys:
                val_to_max_is_len.pop(k)
            val_to_max_is_len[val] = max_is_len

        return len(target)-max(val_to_max_is_len.values())
        
if __name__=='__main__':
    if lc_lib_path.exists():
        s = Solution()

        target = [5,1,3]
        arr = [9,4,2,3,4]
        r = s.minOperations(target,arr)
        print(r)
        print('-'*30)

        target = [6,4,8,1,3,2]
        arr = [4,7,6,2,3,8,6,1]
        r = s.minOperations(target,arr)
        print(r)
        print('-'*30)

        p = pathlib.Path('testcase.txt')
        target, arr = p.read_text().splitlines()
        r = s.minOperations(target,arr)
        print(r)
        print('-'*30)

