
# load local leetcode lib
import pathlib

lc_lib_path = pathlib.Path('../lc_lib.py')
if lc_lib_path.exists():
    import sys
    sys.path.insert(0, '..')
    from lc_lib import *

old_print = print
def print(*args, **kwargs):
    if lc_lib_path.exists():
        old_print(*args, **kwargs)

class Solution:
    def maximumUnits(self, boxTypes: List[List[int]], truckSize: int) -> int:
        boxTypes = sorted(boxTypes, key=lambda x:x[1], reverse=True)        
        units = 0
        print(boxTypes)
        i = 0
        while truckSize and i<len(boxTypes):
            box_count, units_per_box = boxTypes[i]
            box_taken = min(box_count, truckSize)
            units += box_taken*units_per_box
            truckSize -= box_taken
            i += 1
        return units

if __name__=='__main__':
    if lc_lib_path.exists():
        s = Solution()

        boxTypes = [[1,3],[2,2],[3,1]]
        truckSize = 4
        r = s.maximumUnits(boxTypes,truckSize)
        print(r)
        print('-'*30)


        boxTypes = [[5,10],[2,5],[4,7],[3,9]]
        truckSize = 10
        r = s.maximumUnits(boxTypes,truckSize)
        print(r)
        print('-'*30)
