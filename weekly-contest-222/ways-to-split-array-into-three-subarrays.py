
# load local leetcode lib
import pathlib

lc_lib_path = pathlib.Path('../lc_lib.py')
if lc_lib_path.exists():
    import sys
    sys.path.insert(0, '..')
    from lc_lib import *

old_print = print
def print(*args, **kwargs):
    if lc_lib_path.exists():
        old_print(*args, **kwargs)

import bisect


class Solution:
    def waysToSplit(self, nums: List[int]) -> int:
        sums = []
        s = 0
        for num in nums:
            s += num
            sums.append(s)

        print(sums)
        
        results = 0
        for i in range(len(sums)-1):
            s1 = sums[i]
            if sums[-1]<3*s1:
                break
            print(i, s1)
            mid_min = 2*s1
            mid_max = (s+s1)/2
            if mid_min>mid_max:
                break
            mid_left = bisect.bisect_left(sums, mid_min)
            mid_left = max(i+1, mid_left)
            mid_right = bisect.bisect_right(sums, mid_max)
            mid_right = min(mid_right, len(sums)-1)
            print(mid_min, mid_max, mid_left, mid_right)
            print('')
            results += mid_right-mid_left

        m = 10**9+7
        return results % m


if __name__=='__main__':
    if lc_lib_path.exists():
        s = Solution()

        nums = [1,1,1]
        r = s.waysToSplit(nums)
        print(r)
        print('-'*30)

        nums = [1,2,2,2,5,0]
        r = s.waysToSplit(nums)
        print(r)
        print('-'*30)

        nums = [3,2,1]
        r = s.waysToSplit(nums)
        print(r)
        print('-'*30)

        nums = [0,3,3]
        r = s.waysToSplit(nums)
        print(r)
        print('-'*30)

