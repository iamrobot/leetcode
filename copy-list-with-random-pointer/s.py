
# load local leetcode lib
import pathlib

lc_lib_path = pathlib.Path('../lc_lib.py')
if lc_lib_path.exists():
    import sys
    sys.path.insert(0, '..')
    from lc_lib import *

old_print = print
def print(*args, **kwargs):
    if lc_lib_path.exists():
        old_print(*args, **kwargs)

# Definition for a Node.
class Node:
    def __init__(self, x: int, next: 'Node' = None, random: 'Node' = None):
        self.val = int(x)
        self.next = next
        self.random = random

from collections import OrderedDict


class Solution:
    def copyRandomList(self, head: 'Node') -> 'Node':
        if head==None:
            return head
        new_nodes = []
        old_to_new = dict()
        new_to_old = dict()
        p = head
        while p:
            new_node = Node(p.val)
            new_nodes.append(new_node)
            old_to_new[p] = new_node
            new_to_old[new_node] = p
            p = p.next

        i = 0
        while i<len(new_nodes):
            if i<len(new_nodes)-1:
                new_nodes[i].next = new_nodes[i+1]
            new_node = new_nodes[i]
            old_node = new_to_old[new_node]
            if old_node.random==None:
                new_nodes[i].random = None
            else:
                new_nodes[i].random = old_to_new[old_node.random]
            
            i += 1
        
        return new_nodes[0]

if __name__=='__main__':
    if lc_lib_path.exists():
        s = Solution()
        x = None # input
        r = s.copyRandomList(x)
        print(r)
