
# load local leetcode lib
import pathlib
lc_lib_path = pathlib.Path('../lc_lib.py')
if lc_lib_path.exists():
    import sys
    sys.path.insert(0, '..')
    from lc_lib import *

old_print = print
def print(*args, **kwargs):
    if lc_lib_path.exists():
        old_print(*args, **kwargs)

from sortedcontainers import SortedList, SortedSet
class Solution:
    def minimumIncompatibility(self, nums: List[int], k: int) -> int:
        nums.sort()
        print(nums)
        n = len(nums)
        elements_per_group = n//k
        groups = [SortedList() for _ in range(k)]
        inf = float('inf')
        result = inf

        def dfs(i, cur_res):
            nonlocal result
            if i==n:
                result = min(result, cur_res)

            if cur_res>=result:
                return

            num = nums[i]
            for j in range(k):
                group = groups[j]
                if any([
                    len(group)>=elements_per_group,
                    j>0 and group==groups[j-1],
                    num in group,
                   ]):
                    continue
                res_add = 0
                if group and (num<group[0] or num>group[-1]):
                    res_add = min(abs(num-group[0]), abs(num-group[-1]))
                group.add(num)
                dfs(i+1, cur_res+res_add)
                group.remove(num)
                    
        dfs(0, 0)
        return result if result!=inf else -1

if __name__=='__main__':
    if lc_lib_path.exists():
        s = Solution()

        nums = [1,2,1,4]
        k = 2
        r = s.minimumIncompatibility(nums,k)
        print(r)
        print('-'*30)

        nums = [7,3,16,15,1,13,1,2,14,5,3,10,6,2,7,15]
        k = 8
        r = s.minimumIncompatibility(nums,k)
        print(r)
        print('-'*30)

