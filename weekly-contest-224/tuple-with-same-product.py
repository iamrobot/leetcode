
# load local leetcode lib
import pathlib
lc_lib_path = pathlib.Path('../lc_lib.py')
if lc_lib_path.exists():
    import sys
    sys.path.insert(0, '..')
    from lc_lib import *

old_print = print
def print(*args, **kwargs):
    if lc_lib_path.exists():
        old_print(*args, **kwargs)

import itertools
import collections
import math

class Solution:
    def tupleSameProduct(self, nums: List[int]) -> int:
        product_to_ij_map = collections.defaultdict(lambda:[])
        for i,j in itertools.combinations(nums, 2):
            product = i*j
            product_to_ij_map[product].append((i,j))

        pprint(product_to_ij_map)
        result = 0
        for k,v in product_to_ij_map.items():
            n = len(v)
            if n==1:
                continue
            result += 2*2*n*(n-1)

        return result

if __name__=='__main__':
    if lc_lib_path.exists():
        sol = Solution()

        nums = [1,2,4,8,16,32,64,128,256,512,1024,2048,4096,8192]
        r = sol.tupleSameProduct(nums)
        print(r)
        print('-'*30)

        nums = [1,2,4,5,10]
        r = sol.tupleSameProduct(nums)
        print(r)
        print('-'*30)

        nums = [2,3,4,6,8,12]
        r = sol.tupleSameProduct(nums)
        print(r)
        print('-'*30)

