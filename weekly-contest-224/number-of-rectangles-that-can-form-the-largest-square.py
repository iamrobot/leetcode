
# load local leetcode lib
import pathlib
lc_lib_path = pathlib.Path('../lc_lib.py')
if lc_lib_path.exists():
    import sys
    sys.path.insert(0, '..')
    from lc_lib import *

old_print = print
def print(*args, **kwargs):
    if lc_lib_path.exists():
        old_print(*args, **kwargs)

from collections import Counter
class Solution:
    def countGoodRectangles(self, rectangles: List[List[int]]) -> int:
        k = [min(i,j) for i,j in rectangles]
        max_k = max(k)
        c = Counter(k)
        return c[max_k]

if __name__=='__main__':
    if lc_lib_path.exists():
        sol = Solution()

        rectangles = [[5,8],[3,9],[5,12],[16,5]]
        r = sol.countGoodRectangles(rectangles)
        print(r)
        print('-'*30)

