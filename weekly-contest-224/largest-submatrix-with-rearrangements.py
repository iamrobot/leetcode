
# load local leetcode lib
import pathlib
lc_lib_path = pathlib.Path('../lc_lib.py')
if lc_lib_path.exists():
    import sys
    sys.path.insert(0, '..')
    from lc_lib import *

old_print = print
def print(*args, **kwargs):
    if lc_lib_path.exists():
        old_print(*args, **kwargs)


class Matrix:
    def __init__(self, matrix):
        self.matrix = matrix
        self.m = len(matrix)
        self.n = len(matrix[0]) if self.m else None

    def __getattr__(self, item):
        return getattr(self.matrix, item)

    def __getitem__(self, item):
        return self.matrix[item]

    def __setitem__(self, key, value):
        self.matrix[key] = value

    def __repr__(self):
        return repr(self.matrix)

    def __str__(self):
        return str(self.matrix)

    def __call__(self, *args):
        if len(args)==1:
            i,j = args[0]
        elif len(args)==2:
            i,j = args
        else:
            msg = 'you can get matrix item by matrix[i][j], matrix(i,j) or matrix((i,j)), no other ways'
            raise Exception(msg)
        return self[i][j]

    def __iter__(self):
        m = self.m
        n = self.n
        for i in range(m):
            for j in range(n):
                pos = (i,j)
                yield pos, self(pos)

    def is_pos_in_matrix(self, pos):
        m = self.m
        n = self.n
        i,j = pos
        return 0<=i<m and 0<=j<n

    def get_neighbors(self, pos):
        i,j = pos
        neighbors = [
                (i, j+1),
                (i, j-1),
                (i+1, j),
                (i-1, j),
                ]
        return [pos for pos in neighbors if self.is_pos_in_matrix(pos)]

    def zeros_like(self):
        rows = self.m
        cols = self.n
        if rows==0: return []
        return [[0]*cols for _ in range(rows)]

import itertools
class Solution:
    def largestSubmatrix(self, matrix: List[List[int]]) -> int:
        matrix = Matrix(matrix)
        m = matrix.m
        n = matrix.n
        dp = matrix.zeros_like()

        for j in range(n):
            ones = 0
            for i in range(m):
                if matrix[i][j]==1:
                    ones += 1
                else:
                    ones = 0
                dp[i][j] = ones

        result = 0
        for i in range(m):
            l = dp[i]
            l.sort(reverse=True)
            for j,v in enumerate(l):
                if v==0:
                    break
                result = max(result, v*(j+1))
        return result


if __name__=='__main__':
    if lc_lib_path.exists():
        sol = Solution()

        l = [
            [[0,0,1],[1,1,1],[1,0,1]],
            [[1,1,0],[1,0,1]],
            [[0,0],[0,0]],
            [[1,0,1,0,1]],
            [[1,1,0,0,1,0,1,1,0,1,1,1,1,0,1,1,1,0,1,1,1,0,1,0,0,1,1,1,1,1,0,1,0,1,1],[1,1,1,1,1,1,0,1,0,1,1,1,0,1,1,1,1,0,1,1,0,1,1,1,1,1,1,0,1,0,0,1,1,1,1],[1,1,1,0,1,1,1,1,1,0,0,0,1,1,1,0,1,1,1,1,1,1,1,1,1,0,1,1,1,0,1,1,1,1,0]],
        ]
        for matrix in l:
            r = sol.largestSubmatrix(matrix)
            print(r)
            print('-'*30)

