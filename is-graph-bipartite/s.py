
# load local leetcode lib
import pathlib
lc_lib_path = pathlib.Path('../lc_lib.py')
if lc_lib_path.exists():
    import sys
    sys.path.insert(0, '..')
    from lc_lib import *

old_print = print
def print(*args, **kwargs):
    if lc_lib_path.exists():
        old_print(*args, **kwargs)

class Solution:
    def isBipartite(self, graph: List[List[int]]) -> bool:
        def dfs(i, group, another_group):
            if i in visited:
                return
            group.add(i)
            visited.add(i)
            neighbors = graph[i]
            for neighbor in neighbors:
                another_group.add(neighbor)
                dfs(neighbor, another_group, group)

        g0 = set()
        g1 = set()
        visited = set()
        n = len(graph)
        for i in range(n):
            dfs(i, g0, g1)

        intersection = g0.intersection(g1)
        return len(intersection)==0

if __name__=='__main__':
    if lc_lib_path.exists():
        sol = Solution()

        graph = [[1,2,3],[0,2],[0,1,3],[0,2]]
        r = sol.isBipartite(graph)
        print(r)
        print('-'*30)

        graph = [[1,3],[0,2],[1,3],[0,2]]
        r = sol.isBipartite(graph)
        print(r)
        print('-'*30)
