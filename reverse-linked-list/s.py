
# load local leetcode lib
import pathlib

lc_lib_path = pathlib.Path('../lc_lib.py')
if lc_lib_path.exists():
    import sys
    sys.path.insert(0, '..')
    from lc_lib import *

# Definition for singly-linked list.
# class ListNode:
#     def __init__(self, val=0, next=None):
#         self.val = val
#         self.next = next
class Solution:
    def reverseList(self, head: ListNode) -> ListNode:
        if head==None:
            return head

        def reverse_f(head):
            p = head
            if p.next==None:
                return p, p
            new_head, new_tail = reverse_f(head.next)
            new_tail.next = p
            p.next = None
            return new_head, p

        new_head, new_tail = reverse_f(head)
        return new_head

    def reverseList_iteratively(self, head: ListNode) -> ListNode:
        if head==None or head.next==None:
            return head

        p1 = head
        p2 = head.next
        while True:
            tmp_p1 = p1
            tmp_p2 = p2
            tmp_p3 = p2.next

            p1 = tmp_p2
            p2 = tmp_p3
            tmp_p2.next = tmp_p1

            if tmp_p3==None:
                head.next = None
                return tmp_p2

if __name__=='__main__':
    if lc_lib_path.exists():
        s = Solution()
        l = generate_linked_list(end=5)
        newl = s.reverseList(l)
        print_linked_list(newl)
        print('---')
