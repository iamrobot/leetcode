
# load local leetcode lib
import pathlib

lc_lib_path = pathlib.Path('../lc_lib.py')
if lc_lib_path.exists():
    import sys
    sys.path.insert(0, '..')
    from lc_lib import *

class UnionFindTree(object):
    def __init__(self, val):
        self.parent = self
        self.val = val
        self.size = 1

class UnionFindForest(object):
    def __init__(self, vals=[]):
        self.vals = set()
        self.val_to_node_map = dict()
        for val in vals:
            self.add(val)

    def add(self, val):
        if val not in self.vals:
            self.vals.add(val)
            new_tree = UnionFindTree(val)
            self.val_to_node_map[val] = new_tree
        return self.val_to_node_map[val]

    def find(self, val):
        node = self.val_to_node_map[val]
        nodes_on_path = []
        while node.parent!=node:
            nodes_on_path.append(node)
            node = node.parent
        root = node
        for node in nodes_on_path:
            node.parent = root
        return root

    def union(self, val1, val2):
        tree1 = self.find(val1)
        tree2 = self.find(val2)
        if tree1==tree2:
            return

        if tree1.size>tree2.size:
            child, parent = tree2, tree1
        else:
            child, parent = tree1, tree2
        child.parent = parent
        parent.size = parent.size + child.size

    def print(self):
        trees = defaultdict(lambda:[])
        for val in self.vals:
            root = self.find(val)
            trees[root].append(val)

        for root in trees:
            tree = trees[root]
            print(f'{root.val}  -> {[x for x in tree]}')


class Solution:
    def distanceLimitedPathsExist_union_find(self, n: int, edgeList: List[List[int]], queries: List[List[int]]) -> List[bool]:
        edgeList = sorted(edgeList, key=lambda x:x[-1])
        uff = UnionFindForest(range(n))

        queries_sorted_index, queries = zip(*sorted(enumerate(queries), key=lambda x:x[1][-1]))
        edge_i = 0
        result = []
        for query in queries:
            q_u, q_v, q_dis = query
            while edge_i<len(edgeList) and edgeList[edge_i][-1]<q_dis:
                e_u, e_v, e_dis = edgeList[edge_i]
                uff.union(e_u, e_v)
                edge_i += 1

            result.append(uff.find(q_u)==uff.find(q_v))

        result = [i[1] for i in sorted(zip(queries_sorted_index, result), key=lambda x:x[0])]
        return result

    def distanceLimitedPathsExist(self, *args, **kwargs):
        return self.distanceLimitedPathsExist_union_find(*args, **kwargs)
        

if __name__=='__main__':
    if lc_lib_path.exists():
        s = Solution()
        n = 13
        edgeList = [[9,1,53],[3,2,66],[12,5,99],[9,7,26],[1,4,78],[11,1,62],[3,10,50],[12,1,71],[12,6,63],[1,10,63],[9,10,88],[9,11,59],[1,4,37],[4,2,63],[0,2,26],[6,12,98],[9,11,99],[4,5,40],[2,8,25],[4,2,35],[8,10,9],[11,9,25],[10,11,11],[7,6,89],[2,4,99],[10,4,63]]
        queries = [[9,7,65],[9,6,1],[4,5,34],[10,8,43],[3,7,76],[4,2,15],[7,6,52],[2,0,50],[7,6,62],[1,0,81],[4,5,35],[0,11,86],[12,5,50],[11,2,2],[9,5,6],[12,0,95],[10,6,9],[9,4,73],[6,10,48],[12,0,91],[9,10,58],[9,8,73],[2,3,44],[7,11,83],[5,3,14],[6,2,33]]
        r = s.distanceLimitedPathsExist(n, edgeList, queries)
        print(r)
