
# load local leetcode lib
import pathlib
lc_lib_path = pathlib.Path('../lc_lib.py')
if lc_lib_path.exists():
    import sys
    sys.path.insert(0, '..')
    from lc_lib import *

old_print = print
def print(*args, **kwargs):
    if lc_lib_path.exists():
        old_print(*args, **kwargs)


def zeros(rows, cols):
    return [[0]*cols for _ in range(rows)]

def zeros_like(matrix):
    m = len(matrix)
    if m==0: return []
    n = len(matrix[0])
    return zeros(m, n)

class Solution:
    def maximalRectangle(self, matrix: List[List[str]]) -> int:
        m = len(matrix)
        if m==0: return 0
        n = len(matrix[0])
        if n==0: return 0

        for i in range(m):
            for j in range(n):
                matrix[i][j]=int(matrix[i][j])

        row_sum = zeros(m+1, n+1)
        col_sum = zeros(m+1, n+1)

        for i in range(m):
            ones_before = 0
            for j in range(n):
                if matrix[i][j]==1:
                    ones_before += 1
                else:
                    ones_before = 0
                row_sum[i+1][j+1]=ones_before
        for j in range(n):
            ones_before = 0
            for i in range(m):
                if matrix[i][j]==1:
                    ones_before += 1
                else:
                    ones_before = 0
                col_sum[i+1][j+1]=ones_before

        inf = float('inf')
        cur_max = 0
        for i in range(m):
            ti = i+1
            for j in range(n):
                tj = j+1
                if matrix[i][j]==0: continue
                p = row_sum[ti][tj]
                q = col_sum[ti][tj]
                if p*q<=cur_max:
                    continue
                col = inf
                for k in range(p):
                    col = min(col, col_sum[ti][tj-k])
                    cur_max = max(cur_max, col*(k+1))

                row = inf
                for k in range(q):
                    row = min(row, row_sum[ti-k][tj])
                    cur_max = max(cur_max, row*(k+1))

        return cur_max


if __name__=='__main__':
    if lc_lib_path.exists():
        s = Solution()

        matrix = [[1,0],[1,0]]
        r = s.maximalRectangle(matrix)
        print(r)
        print('-'*30)

        matrix = [["1","0","1","0","0"],["1","0","1","1","1"],["1","1","1","1","1"],["1","0","0","1","0"]]
        r = s.maximalRectangle(matrix)
        print(r)
        print('-'*30)

        from testcase import x
        matrix = x
        r = s.maximalRectangle(matrix)
        print(r)
        print('-'*30)

