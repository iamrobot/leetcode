class Solution:
    def containsNearbyAlmostDuplicate(self, nums: List[int], k: int, t: int) -> bool:
        from collections import OrderedDict

        if len(nums)<2:
            return False
        if k<=0:
            return False
        if t<0:
            return False

        l = len(nums)
        k = min(l-1, k)  # i j

        d = OrderedDict()

        for num in nums:
            bucket = num // t if t else num

            for m in (d.get(bucket-1), d.get(bucket), d.get(bucket+1)):
                if m and (m-num)<=t:
                    return True
                
                d[bucket] = num

            if len(d)>k:
                d.pop()

        return False


