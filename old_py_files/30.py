from collections import Counter


class Solution:
    def findSubstring(self, s: str, words):
        word_len = len(words[0])
        s_l = [s[i:i+word_len] for i in range(0, len(s), word_len)]
        target_counter = Counter(words)
        s_counter = Counter(s_l[:len(words)-1])
        ans = []

        for end in range(len(words)-1, len(s_l)):
            end_word = s_l[end]
            start = end+1-len(words)
            start_word = s_l[start]

            s_counter += {end_word:1}
            if s_counter==target_counter:
                ans.append(start*word_len)
            s_counter -= {start_word:1}
            start += 1

            print(target_counter)
            print(s_counter)
            print('')

        return ans

if __name__=='__main__':
    s = 'barfoothefoobarman'
    words = ['foo', 'bar']
    s = "lingmindraboofooowingdingbarrwingmonkeypoundcake"
    words = ["fooo","barr","wing","ding","wing"]
    sol = Solution()
    ans = sol.findSubstring(s, words)
    print(ans)
