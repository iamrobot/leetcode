class Solution:
    def canPartition(self, nums) -> bool:
        if not nums:
            return False

        s = sum(nums)
        if s%2!=0:
            return False

        target_sum = s // 2

        # dynamic program
        l = [False] * (s+1)
        l[0] = True
        for num in nums:
            for i in range(s, 0, -1):
                if i>=num:
                    l[i] = l[i] or l[i-num]
                    print(s, i, num, l)
            print('-'*30)
        return l[target_sum]


        # using set
        #possible_sums = set([0])
        #for num in nums:
        #    next_possible_sums = {v+num for v in possible_sums}
        #    possible_sums.update(next_possible_sums)
        #    if target_sum in possible_sums:
        #        return True
        #return False


if __name__=='__main__':
    s = [1, 2, 5]
    sol = Solution()
    ans = sol.canPartition(s)
    print(ans)
