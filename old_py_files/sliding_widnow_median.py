class Solution:
    def medianSlidingWindow(self, nums: List[int], k: int) -> List[int]:
        import bisect

        if not nums:
            return []

        if k<=0:
            return []

        res = []
        window = sorted(nums[:k])

        for i,n in enumerate([*nums[k:], 0]):
            m = k // 2
            if k%2==0:
                a = (window[m]+window[m-1])/2
            else:
                a = window[m]
            
            res.append(a)

            bisect.insort(window, n)

            window.remove(nums[i])

        return res

