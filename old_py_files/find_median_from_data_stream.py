import bisect


class MedianFinder:

    def __init__(self):
        """
        initialize your data structure here.
        """
        self.data = []
        

    def addNum(self, num: int) -> None:
        bisect.insort(self.data, num)        
        

    def findMedian(self) -> float:
        window = self.data
        k = len(window)

        m = k // 2
        if k%2==0:
            a = (window[m]+window[m-1])/2
        else:
            a = window[m]
        
        return a


# Your MedianFinder object will be instantiated and called as such:
# obj = MedianFinder()
# obj.addNum(num)
# param_2 = obj.findMedian()
