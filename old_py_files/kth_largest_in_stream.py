import copy
import heapq


class KthLargest:

    def __init__(self, k: int, nums: List[int]):
        self.k = k
        self.pool = nums
        heapq.heapify(self.pool)
        while len(self.pool)>k:
            _ = heapq.heappop(self.pool) 
        

    def add(self, val: int) -> int:
        heapq.heappush(self.pool, val)
        while len(self.pool)>self.k:
            _ = heapq.heappop(self.pool) 
        return self.pool[0]
