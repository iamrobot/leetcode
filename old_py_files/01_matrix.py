import math
from queue import Queue


class Solution:
    def updateMatrix(self, matrix: List[List[int]]) -> List[List[int]]:
        if not matrix:
            return []

        rows = len(matrix)
        cols = len(matrix[0])

        dist_to_0_matrix = [
            [math.inf]*cols for i in range(rows)
        ]

        def ij_legal(i, j):
            return all([
                i>=0,
                j>=0,
                i<rows,
                j<cols,
            ])

        def ij_neighbors(i, j):
            ei, ej = i, j
            neighbors = [
                (ei-1, ej),
                (ei+1, ej),
                (ei, ej-1),
                (ei, ej+1),
            ]
            neighbors = [(i,j) for i,j in neighbors if ij_legal(i,j)]
            return neighbors

        def get_d(i, j):
            return dist_to_0_matrix[i][j]

        q = Queue()
        for i in range(rows):
            for j in range(cols):
                if matrix[i][j]==0:
                    dist_to_0_matrix[i][j]=0
                    q.put((i,j))

        while not q.empty():
            ei, ej = q.get()
            d = get_d(ei, ej)

            for ni, nj in ij_neighbors(ei, ej):
                if d+1<dist_to_0_matrix[ni][nj]:
                    dist_to_0_matrix[ni][nj] = d+1
                    q.put((ni, nj))

        return dist_to_0_matrix
