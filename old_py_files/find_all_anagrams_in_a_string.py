from collections import Counter


class Solution:
    def findAnagrams(self, s: str, p: str):
        if len(s)==0 or len(s)<len(p):
            return []

        scounter = Counter(s[:len(p)-1])
        pcounter = Counter(p)
        ans = []

        for end, end_char in enumerate(s[len(p)-1:]):
            end = end + len(p) -1
            start = end+1-len(p)
            scounter[end_char] += 1
            if scounter==pcounter:
                ans.append(start)
            scounter -= {s[start]:1}
        return ans

if __name__=='__main__':
    s = 'cbaebabacd'
    p = 'abc'
    sol = Solution()
    ans = sol.findAnagrams(s, p)
    print(ans)
    s = 'abab'
    p = 'ab'
    sol = Solution()
    ans = sol.findAnagrams(s, p)
    print(ans)
