from queue import Queue


class Solution:
    def findCircleNum(self, M: List[List[int]]) -> int:
        if not M:
            return 0

        circle_count = 0
        visited = set()
        students = len(M)

        for i in range(students):
            if i in visited:
                continue

            circle_count += 1

            q = Queue()
            q.put(i)
            while not q.empty():
                k = q.get()
                if k in visited:
                    continue
                visited.add(k)
                for j in range(students):
                    if j in visited:
                        continue
                    if M[k][j]==1:
                        q.put(j)

        return circle_count
