class Solution:
    def longestConsecutive(self, nums: List[int]) -> int:
        maxlen = 0
        nums = set(nums)
        while nums:
            n = nums.pop()
            current_len = 1

            m = n
            while m-1 in nums:
                nums.remove(m-1)
                current_len += 1
                m = m-1

            m = n
            while m+1 in nums:
                nums.remove(m+1)
                current_len += 1
                m = m+1
            maxlen = max(maxlen, current_len)

        return maxlen


