# Definition for singly-linked list.
# class ListNode:
#     def __init__(self, x):
#         self.val = x
#         self.next = None

class Solution:
    def getIntersectionNode(self, headA: ListNode, headB: ListNode) -> ListNode:
        if headA==None or headB==None:  
            return None

        parent = headA
        while parent.next:
            parent.next.parentA = parent
            parent = parent.next
        endA = parent

        parent = headB
        while parent.next:
            parent.next.parentB = parent
            parent = parent.next
        endB = parent

        if endA is not endB:
            return None

        while endA is endB:
            if not hasattr(endA, 'parentA'):
                return endA

            if not hasattr(endB, 'parentB'):
                return endB

            endA = endA.parentA
            endB = endB.parentB
            
        return endA.next
