#!/usr/bin/env python


#class Solution:
#    def searchInsert(self, nums: List[int], target: int) -> int:
#        for index,value in enumerate(nums):
#            if value<target:
#                continue
#            elif value==target:
#                return index
#            else:
#                return index
#        else:
#            return index+1

class Solution:
    def searchInsert(self, nums: List[int], target: int) -> int:
        if target<=nums[0]:
            return 0
        if target>=nums[-1]:
            return len(nums)

        left = 0
        right = len(nums)-1

        while left+1<right:
            mid = round((left+right)/2)
            mid_value = nums[mid]
            if mid_value==target:
                return mid
            elif mid_value<target:
                left = mid
            else:
                right = mid
        
        return right
