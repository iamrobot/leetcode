class Solution:
    def isValid(self, s: str) -> bool:
        from queue import LifoQueue
        d = {
            '(':')',
            '{':'}',
            '[':']',
        }
        reversed_d = {v:k for k,v in d.items()}
        q = LifoQueue()
        for c in s:
            if c in d.keys():
                q.put(c)
            else:
                if q.empty():
                    return False
                other = q.get()
                if other==reversed_d[c]:
                    continue
                else:
                    return False
        return q.empty()

s = Solution()
print(s.isValid('()[]{}'))
print(s.isValid('()[]{'))
print(s.isValid('([)]'))
