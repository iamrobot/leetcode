class Solution:
    def ladderLength(self, beginWord: str, endWord: str, wordList: List[str]) -> int:
        if endWord not in wordList:
            return 0

        from collections import defaultdict
        from queue import Queue

        length = len(beginWord)
        wordList = [beginWord, *wordList]

        pattern_word_map = defaultdict(list)
        for word in wordList:
            for i in range(length):
                pattern_word_map[f'{word[:i]}*{word[i+1:]}'].append(word)

        q = Queue()
        q.put((beginWord, 1))
        seen_words = set([beginWord])

        while not q.empty():
            current_word, current_level = q.get()
            for i in range(length):
                pattern = current_word[:i] + '*' + current_word[i+1:] 
                adjs = pattern_word_map[pattern]

                if endWord in adjs:
                    return current_level + 1

                for word in adjs:
                    if word in seen_words:
                        continue
                    q.put((word, current_level+1))
                    seen_words.add(word)

        return 0
