class Solution:
    def canThreePartsEqualSum(self, A) -> bool:
        s = sum(A)
        if s%3!=0:
            return False

        target_sum = s // 3
        current_sum = 0
        count = 0
        for i,v in enumerate(A):
            current_sum += v
            if current_sum==target_sum:
                current_sum = 0
                count += 1
                if count==3:
                    return True

if __name__=='__main__':
    a = [3,3,6,5,-2,2,5,1,-9,4]
    sol = Solution()
    ans = sol.canThreePartsEqualSum(a)
    print(ans)
