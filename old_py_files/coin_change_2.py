import math
from collections import defaultdict
from copy import deepcopy
from queue import Queue


class Solution:
    def change(self, amount: int, coins) -> int:
        if amount==0 or not coins:
            return 0

        dp = [0]*(amount+1)
        dp[0] = 1

        for coin in coins:
            for i in range(1, amount+1):
                if i >= coin:
                    dp[i] += dp[i-coin]

        return dp[amount]


if __name__=='__main__':
    amount = 500
    coins = [1,2,5]
    sol = Solution()
    ans = sol.change(amount, coins)
    print(ans)
