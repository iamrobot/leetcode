class Solution:
    def isUgly(self, num: int) -> bool:
        if num<1:
            return False

        while num!=1:
            if num%2==0:
                num = int(num/2)

            elif num%3==0:
                num = int(num/3)

            elif num%5==0:
                num = int(num/5)

            else:
                break


        return num==1
