from collections import Counter, defaultdict

# sliding window solution

#class Solution:
#    def lengthOfLongestSubstring(self, s: str) -> int:
#        start = end = 0
#        ans = 0
#        chars_counter = defaultdict(lambda:0)
#
#        while end<len(s):
#            end_char = s[end]
#            chars_counter[end_char] += 1
#            if chars_counter[end_char]==1:
#                ans = max(ans, end+1-start)
#            else:
#                while chars_counter[end_char]>1:
#                    start_char = s[start]
#                    chars_counter[start_char] -= 1
#                    start += 1
#            end += 1
#        return ans


# dp solution
# fuck it, dp does not work here, use sliding window instead

def split_with_delimiter(s, delimiter):
    return [e+delimiter for e in s.split(delimiter)]

class Solution:
    def lengthOfLongestSubstring(self, s: str) -> int:
        chars_counter = Counter(s)
        
        for char,count in chars_counter.items():
            if count>1:
                return max([0, *[self.lengthOfLongestSubstring(t) for t in split_with_delimiter(s, char)]])

        return len(s)

if __name__=='__main__':
    s = 'abcabcbb'
    sol = Solution()
    ans = sol.lengthOfLongestSubstring(s)
    print(ans)
