from queue import Queue


class Solution:
    def numSquares(self, n: int) -> int:
        squares = []
        for i in range(1,n+1):
            i2 = i**2
            if i2>n:
                break
            squares.append(i2)

        q = {n}
        square_count = 1

        while q:
            temp = set()
            for left_n in q:
                for square in squares:
                    if left_n<square:
                        break

                    elif left_n==square:
                        return square_count

                    else:
                        temp.add(left_n-square)

            q = temp
            square_count += 1

if __name__=='__main__':
    sol = Solution()
    for n in [7168, 1, 12]:
        ans = sol.numSquares(n)
        print(n, ans)
        print('')
