#!/usr/bin/env python

#  https://leetcode.com/problems/rotate-array/


class Solution:
    def rotate(self, nums: List[int], k: int) -> None:
        """
        Do not return anything, modify nums in-place instead.
        """
        
        n = len(nums)
        k = k % n
        for _ in range(k):
            item = nums.pop()
            nums.insert(0, item)
