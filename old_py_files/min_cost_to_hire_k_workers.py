import heapq
import math


class Solution:
    def mincostToHireWorkers(self, quality: List[int], wage: List[int], K: int) -> float:

        ratio_l = [wage[i]/quality[i] for i in range(len(quality))]
        workers = sorted(zip(ratio_l, quality, wage), key=lambda x:x[0])

        ans = math.inf
        pool = []
        sumq = 0

        for ratio, q, w in workers:
            heapq.heappush(pool, -q)
            sumq += q

            if len(pool)>K:
                sumq += heapq.heappop(pool)

            if len(pool)==K:
                _ans = ratio * sumq
                ans = min(ans, _ans)

        return ans
