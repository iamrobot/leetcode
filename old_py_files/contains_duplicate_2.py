#class Solution:
#    def containsNearbyDuplicate(self, nums: List[int], k: int) -> bool:
#        import heapq
#        from collections import defaultdict
#
#        d = defaultdict(lambda:[])
#
#        for index,num in enumerate(nums):
#            d[num].append(index)
#
#
#        for v in d.values():
#            if len(v)<2:
#                continue
#
#            diff = [v[i+1]-v[i] for i in range(len(v)-1)]
#
#            if min(diff)<k:
#                return True
#
#        return False

class Solution:
    def containsNearbyDuplicate(self, nums: List[int], k: int) -> bool:
        d = {}

        for i, n in enumerate(nums):
            if n in d:
                if i<=d[n]+k:
                    return True
                d[n] = i
        return False
