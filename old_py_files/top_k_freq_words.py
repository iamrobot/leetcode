class Solution:
    def topKFrequent(self, words: List[str], k: int) -> List[str]:
        from collections import Counter

        c = Counter(words)

        freq2words = [[] for i in range(len(words))]
        for item,freq in c.items():
            freq2words[freq].append(item)

        l = []
        for sublist in reversed(freq2words):
            for item in sorted(sublist):
                l.append(item)

        return l[:k]
