#!/usr/bin/env python
from pprint import pprint


class Solution:
    def findLength(self, A, B) -> int:
        

        lena = len(A)
        lenb = len(B)

        dp = [[0]*(lenb+1) for i in range(lena+1)]

        for i,char_a in enumerate(A):
            i += 1
            for j,char_b in enumerate(B):
                j += 1
                if char_a==char_b:
                    dp[i][j] = dp[i-1][j-1] + 1

        return max([max(l) for l in dp])

if __name__=='__main__':
    a = [1,2,3,2,1]
    b = [3,2,1,4,7]

    sol = Solution()
    ans = sol.findLength(a, b)
    print(ans)
