# Definition for a binary tree node.
# class TreeNode:
#     def __init__(self, val=0, left=None, right=None):
#         self.val = val
#         self.left = left
#         self.right = right
class Solution:
    def levelOrderBottom(self, root: TreeNode) -> List[List[int]]:
        if not root:
            return []

        from queue import LifoQueue, Queue
        q = LifoQueue()
        q.put((root, 1))
        r = []

        last_level = 1
        this_level_l = []
        while not q.empty():
            node, this_level = q.get()

            if last_level!=this_level:
                r.append(this_level_l) 
                this_level_l = []
                last_level = this_level
            this_level_l.append(node.val)

            if node.left:
                q.put((node.left, this_level+1))
            if node.right:
                q.put((node.right, this_level+1))
        
        r.append(this_level_l)

        return r
