from typing import List


class Solution:
    def findDisappearedNumbers(self, nums: List[int]) -> List[int]:
        for i,num in enumerate(nums):
            while num!=0 and num!=i+1:
                who_occupied_num_place = nums[num-1]
                if num==who_occupied_num_place:
                    nums[i] = 0
                    break
                else:
                    nums[num-1] = num
                    nums[i] = who_occupied_num_place
                    num = who_occupied_num_place

        l = []
        for i in range(len(nums)):
            if nums[i] == 0:
                l.append(i+1)
        return l
 
s = Solution()
print(s.findDisappearedNumbers([4,3,2,7,8,2,3,1]))
