import math


class Solution:
    def coinChange(self, coins: List[int], amount: int) -> int:
        r = [0, *([math.inf]*amount)]

        for coin in coins:
            for i in range(coin, amount):
                r[i] = min(r[i], r[i-coin]+1)
        

        r = r[-1]
        if r==math.inf:
            r = -1
        return r
