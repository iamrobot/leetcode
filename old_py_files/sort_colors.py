class Solution:
    def sortColors(self, nums: List[int]) -> None:
        """
        Do not return anything, modify nums in-place instead.
        """
        pzero = pone = 0
        ptwo = len(nums)-1

        while pone<ptwo:
            if nums[pone]==1:
                pone += 1
            elif nums[pone]==0:
                # todo
                nums[pzero], nums[pone] = 0, nums[pzero]
                pzero += 1
                pone += 1
            else:
                nums[ptwo], nums[pone] = 2, nums[ptwo]
                ptwo -= 1
