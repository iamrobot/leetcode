import math
from collections import Counter


class Solution:
    def longestSubstring(self, s: str, k: int) -> int:
        chars_counter = Counter(s)
        for char,count in chars_counter.items():
            if count<k:
                return max([0, *[self.longestSubstring(t,k) for t in s.split(char)]])

        return len(s)
