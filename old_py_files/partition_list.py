# Definition for singly-linked list.
# class ListNode:
#     def __init__(self, val=0, next=None):
#         self.val = val
#         self.next = next

def travel_list(head):
    node = head
    while node:
        yield node.val
        node =node.next

class Solution:
    def partition(self, head: ListNode, x: int) -> ListNode:
        if not head:
            return None

        p0_head = ListNode()
        p0_tail = p0_head
        p1_head = ListNode()
        p1_tail = p1_head
            
        node = head
        while node:
            if node.val < x:
                p0_tail.next = node
                p0_tail = node
            else:
                p1_tail.next = node
                p1_tail = node
            node = node.next

        p0_tail.next = None
        p1_tail.next = None
        p0 = p0_head.next
        p1 = p1_head.next
        if p0==None:
            return p1
        else:
            p0_tail.next = p1
            return p0
