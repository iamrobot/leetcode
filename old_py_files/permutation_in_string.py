from collections import Counter


class Solution:
    def checkInclusion(self, s1: str, s2: str) -> bool:
        need_chars = Counter(s1)
        window_chars = Counter('')

        for end,end_char in enumerate(s2):
            start = end - len(s1)
            if start>=0:
                start_char = s2[start] 
                window_chars -= {start_char:1}
            window_chars += {end_char:1}
            if window_chars==need_chars:
                return True

        return False

if __name__=='__main__':
    s = 'ab'
    t = 'bcabc'
    sol = Solution()
    ans = sol.checkInclusion(s, t)
    print(s, t, ans)
