class Solution:
    def numIslands(self, grid: List[List[str]]) -> int:
        if not grid:
            return 0

        def dfs(i, j):
            if any([
                i<0,
                j<0,
                i>=rows,
                j>=cols,
                (i,j) in visited,
            ]):
                return

            if grid[i][j]=='0':
                return

            visited.add((i,j))

            dfs(i-1, j)
            dfs(i+1, j)
            dfs(i, j-1)
            dfs(i, j+1)

        num_of_islands = 0
        visited = set()
        rows = len(grid)
        cols = len(grid[0])

        for i in range(rows):
            for j in range(cols):
                if (i,j) in visited:
                    continue
                if grid[i][j]=='1':
                    num_of_islands += 1
                    dfs(i, j)
                    
        return num_of_islands
