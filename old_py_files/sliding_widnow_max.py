class Solution:
    def maxSlidingWindow(self, nums: List[int], k: int) -> List[int]:
        from collections import deque

        if not nums:
            return []

        if k<=0:
            return []

        res = []
        bigger = deque()

        for i,n in enumerate(nums):
            while bigger and nums[bigger[-1]]<=n:
                bigger.pop()

            bigger.append(i)

            while bigger and i-bigger[0]>=k:
                bigger.popleft()

            if i>=k-1:
                res.append(nums[bigger[0]])
        return res

