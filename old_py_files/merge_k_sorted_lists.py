# Definition for singly-linked list.
# class ListNode:
#     def __init__(self, val=0, next=None):
#         self.val = val
#         self.next = next
class Solution:
    def mergeKLists(self, lists: List[ListNode]) -> ListNode:
       	def merge(l, r):
            head = ListNode(0)
            point = head
            while (l and r):
                if l.val < r.val:
                    point.next = l
                    l = l.next
                else:
                    point.next = r
                    r = r.next
                point = point.next
            if l:
                point.next = l
            else:
                point.next = r

            return head.next
                
            
        if not lists:
            return

        if len(lists)==1:
            return lists[0]

        mid = len(lists)//2
        l = self.mergeKLists(lists[:mid])
        r = self.mergeKLists(lists[mid:])
        return merge(l,r)
