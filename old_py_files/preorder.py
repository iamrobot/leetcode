# Definition for a binary tree node.
# class TreeNode:
#     def __init__(self, val=0, left=None, right=None):
#         self.val = val
#         self.left = left
#         self.right = right

from queue import LifoQueue


class Solution:
    def preorderTraversal(self, root: TreeNode) -> List[int]:
        if not root:
            return []

        result = []
        q = LifoQueue()
        q.put(root)

        while not q.empty():
            item = q.get()
            if item:
                q.put(item.right)
                q.put(item.left)
                result.append(item.val)

        return result
