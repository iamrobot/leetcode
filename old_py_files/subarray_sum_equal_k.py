class Solution:
    def subarraySum(self, nums: List[int], k: int) -> int:
        from collections import defaultdict

        count = 0
        m = defaultdict(lambda key:0)
        m[0] = 1

        s = 0
        for n in nums:
            s += n
            count += m[s-k]
            m[s] = m[s] + 1

        return count
