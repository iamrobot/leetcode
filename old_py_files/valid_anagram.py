class Solution:
    def isAnagram(self, s: str, t: str) -> bool:
        ms = dict()
        mt = dict()

        for i in s:
            c = ms.get(i, 0)
            c += 1
            ms[i] = c

        for i in t:
            c = mt.get(i, 0)
            c += 1
            mt[i] = c


        keys = set(list(ms.keys()) + list(mt.keys()))
        for key in keys:
            c1 = ms.get(key, -1)
            c2 = mt.get(key, -1)
            if c1!=c2:
                return False
        else:
            return True
