from pprint import pprint


class Solution:
    def longestCommonSubsequence(self, text1: str, text2: str) -> int:
        len1 = len(text1)
        len2 = len(text2)

        dp = [[0]*(len2+1) for _ in range(len1+1)]

        for i,char1 in enumerate(text1):
            i += 1
            for j,char2 in enumerate(text2):
                j += 1
                if char1==char2:
                    dp[i][j] = dp[i-1][j-1] + 1
                else:
                    dp[i][j] = max(dp[i][j-1], dp[i-1][j])
        

        return dp[len1][len2]

if __name__=='__main__':
    s1 = "bsbininm"
    s2 = "jmjkbkjkv"
    #s1 = 'abcde'
    #s2 = 'ace'
    sol = Solution()
    ans = sol.longestCommonSubsequence(s1, s2)
    print(ans)
