class Solution:
    def majorityElement(self, nums: List[int]):
        import math
        c1, n1, c2, n2 = 1, 0, 2, 0

        for num in nums:
            if num==c1:
                n1 += 1
            elif num==c2:
                n2 += 1
            else:
                if n1==0:
                    c1 = num
                    n1 = 1
                elif n2==0:
                    c2 = num
                    n2 = 1
                else:
                    n1 -= 1
                    n2 -= 1
        return [c for c in [c1,c2] if nums.count(c)>len(nums)//3]
