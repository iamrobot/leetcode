import math
from collections import Counter


class Solution:
    def minWindow(self, s: str, t: str) -> str:
        if s=='' or t=='':
            return ''

        need_char_set = set(t)
        need = Counter(t)
        missing = len(t)

        start = end = 0
        ans = ''
        ans_l = math.inf
        while end<len(s):
            char = s[end]
            if char in need_char_set:
                if need[char]>0:
                    missing -= 1
                need[char] -= 1

            while missing<=0:
                start_char = s[start]
                if start_char in need_char_set:
                    need[start_char] += 1
                    if need[start_char]>0:
                        missing += 1

                if (end-start)<ans_l:
                    ans = s[start:end+1]
                    ans_l = len(ans)

                start += 1

            end += 1

        return ans

if __name__=='__main__':
    s = 'ADOBECODEBANC'
    t = 'ABC'
    sol = Solution()
    ans = sol.minWindow(s, t)
    print(s, t, ans)
