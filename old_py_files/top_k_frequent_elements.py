class Solution:
    def topKFrequent(self, nums: List[int], k: int) -> List[int]:
        from collections import Counter

        c = Counter(nums)

        freq2elements = [[] for i in range(len(nums))]
        for item,freq in c.items():
            freq2elements[freq].append(item)

        l = []
        for sublist in reversed(freq2elements):
            for item in sublist:
                l.append(item)

        return l[:k]
