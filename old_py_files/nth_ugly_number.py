import heapq


class Solution:
    def nthSuperUglyNumber(self, n: int, primes: List[int]) -> int:
        uglys = [1]

        def gen(prime):
            for ugly in uglys:
                yield prime*ugly

        for ugly in heapq.merge(*map(gen, primes)):
            if ugly!=uglys[-1]:
                uglys.append(ugly)

                if len(uglys)>=n:
                    return uglys[n-1]

if __name__=='__main__':
    s = Solution()
