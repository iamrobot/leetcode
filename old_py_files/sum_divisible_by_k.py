class Solution:
    def subarraysDivByK(self, A: List[int], K: int) -> int:
        from collections import defaultdict

        count = 0
        d = defaultdict(lambda:0)
        d[0] = 1
        s = 0
        for num in A:
            s += num
            r = s % K
            count += d[r]
            d[r] += 1

        return count
