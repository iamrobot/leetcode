
# load local leetcode lib
import pathlib

lc_lib_path = pathlib.Path('../lc_lib.py')
if lc_lib_path.exists():
    import sys
    sys.path.insert(0, '..')
    from lc_lib import *

old_print = print
def print(*args, **kwargs):
    if lc_lib_path.exists():
        old_print(*args, **kwargs)

import heapq


class Solution:
    def maxResult(self, nums: List[int], k: int) -> int:
        prev_max = 0
        l = []
        for i,num in enumerate(nums):
            if i==0:
                cur_max = num
            else:
                while True:
                    prev_max,prev_index = heapq.heappop(l)
                    if prev_index>=i-k:
                        heapq.heappush(l, (prev_max,prev_index))
                        break

            cur_max = -prev_max + num
            heapq.heappush(l, (-cur_max, i))
        
        return cur_max

if __name__=='__main__':
    if lc_lib_path.exists():
        s = Solution()

        nums = [1,-1,-2,4,-7,3]
        k = 2
        r = s.maxResult(nums,k)
        print(r)
        print('-'*30)

        from testcase import k, l
        r = s.maxResult(l,k)
        print(r)
        print('-'*30)
