
# load local leetcode lib
import pathlib

lc_lib_path = pathlib.Path('../lc_lib.py')
if lc_lib_path.exists():
    import sys
    sys.path.insert(0, '..')
    from lc_lib import *

old_print = print
def print(*args, **kwargs):
    if lc_lib_path.exists():
        old_print(*args, **kwargs)


class UnionFindTree(object):
    def __init__(self, val):
        self.parent = self
        self.val = val
        self.size = 1


class UnionFindForest(object):
    def __init__(self, vals=[]):
        self.vals = set()
        self.val_to_node_map = dict()
        for val in vals:
            self.add(val)

    def add(self, val):
        if val not in self.vals:
            self.vals.add(val)
            new_tree = UnionFindTree(val)
            self.val_to_node_map[val] = new_tree
        return self.val_to_node_map[val]

    def find(self, val):
        node = self.val_to_node_map[val]
        nodes_on_path = []
        while node.parent!=node:
            nodes_on_path.append(node)
            node = node.parent
        root = node
        for node in nodes_on_path:
            node.parent = root
        return root

    def union(self, val1, val2):
        tree1 = self.find(val1)
        tree2 = self.find(val2)
        if tree1==tree2:
            return

        if tree1.size>tree2.size:
            child, parent = tree2, tree1
        else:
            child, parent = tree1, tree2
        child.parent = parent
        parent.size = parent.size + child.size

    def print(self):
        trees = defaultdict(lambda:[])
        for val in self.vals:
            root = self.find(val)
            trees[root].append(val)

        for root in trees:
            tree = trees[root]
            print(f'{root.val}  -> {[x for x in tree]}')


from collections import defaultdict


class Solution:
    def distanceLimitedPathsExist(self, n: int, edgeList: List[List[int]], queries: List[List[int]]) -> List[bool]:
        edgeList = sorted(edgeList, key=lambda x:x[-1]) 
        vertices = set()
        for edge in edgeList:
            u,v,dis = edge
            vertices.add(u)
            vertices.add(v)
        for q in queries:
            u,v,dis = q
            vertices.add(u)
            vertices.add(v)

        queries_results = [None]*len(queries)
        queries_index_map = defaultdict(lambda:[])
        for i,q in enumerate(queries):
            queries_index_map[tuple(q)].append(i)

        uf = UnionFindForest(vertices)
        i = 0
        for q in sorted(queries, key=lambda x:x[-1]):
            u,v,dis = q
            while i<len(edgeList) and edgeList[i][-1]<dis: 
                eu, ev, _ = edgeList[i]
                uf.union(eu, ev)
                i += 1
            for qi in queries_index_map[tuple(q)]:
                queries_results[qi] = uf.find(u)==uf.find(v)
        
        return queries_results


if __name__=='__main__':
    if lc_lib_path.exists():
        s = Solution()

        true = True
        false = False

        n = 3
        edgeList = [[0,1,2],[1,2,4],[2,0,8],[1,0,16]]
        queries = [[0,1,2],[0,2,5]]
        r = s.distanceLimitedPathsExist(n,edgeList,queries)
        print(r)
        print('-'*30)


        from testcase import es, n, qs
        r = s.distanceLimitedPathsExist(n,es,qs)
        print(r)
        print('-'*30)
