
# load local leetcode lib
import pathlib
lc_lib_path = pathlib.Path('../lc_lib.py')
if lc_lib_path.exists():
    import sys
    sys.path.insert(0, '..')
    from lc_lib import *

old_print = print
def print(*args, **kwargs):
    if lc_lib_path.exists():
        old_print(*args, **kwargs)

class Solution:
    def countAndSay(self, n: int) -> str:
        if n==1:
            return '1'
        x = self.countAndSay(n-1)
        return Solution.say(x)

        '''
        There exists iterative solution:
        https://leetcode.com/problems/count-and-say/discuss/16044/Simple-Python-Solution
        '''

    @staticmethod
    def say(s):
        result = ''
        count = 1
        i = 0
        c = s[0]
        while i+1<len(s):
            if s[i+1]==c:
                count += 1
            else:
                result += f'{count}{c}'
                c = s[i+1]
                count = 1
            i += 1
        result += f'{count}{c}'
        return result

if __name__=='__main__':
    if lc_lib_path.exists():
        sol = Solution()

        n = 19
        r = sol.countAndSay(n)
        print(r)
        print('-'*30)

