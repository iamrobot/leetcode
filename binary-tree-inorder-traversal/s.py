
# load local leetcode lib
import pathlib

lc_lib_path = pathlib.Path('../lc_lib.py')
if lc_lib_path.exists():
    import sys
    sys.path.insert(0, '..')
    from lc_lib import *

# Definition for a binary tree node.
# class TreeNode:
#     def __init__(self, val=0, left=None, right=None):
#         self.val = val
#         self.left = left
#         self.right = right

from collections import deque


class Solution:
    def inorderTraversal(self, root: TreeNode) -> List[int]:
        def helper(root):
            results = []
            stack = deque([])
            while True:
                while root:
                    stack.append(root)
                    root = root.left

                if not stack:
                    break
                node = stack.pop()
                yield node.val
                root = node.right

        return list(helper(root))

if __name__=='__main__':
    if lc_lib_path.exists():
        s = Solution()
        x = None # input
        r = s.inorderTraversal(x)
        print(r)
