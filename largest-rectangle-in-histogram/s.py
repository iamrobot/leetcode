
# load local leetcode lib
import pathlib
lc_lib_path = pathlib.Path('../lc_lib.py')
if lc_lib_path.exists():
    import sys
    sys.path.insert(0, '..')
    from lc_lib import *

old_print = print
def print(*args, **kwargs):
    if lc_lib_path.exists():
        old_print(*args, **kwargs)

class UnionFindNode(object):
    def __init__(self, val):
        self.parent = self
        self.val = val
        self.size = 1

class UnionFindForest(object):
    def __init__(self, vals=[]):
        self.vals = set()
        self.roots = set()
        self.val_to_node_map = dict()
        for val in vals:
            self.add(val)
        self.max_tree_size = 1

    def add(self, val):
        if val not in self.vals:
            self.vals.add(val)
            new_tree = UnionFindNode(val)
            self.val_to_node_map[val] = new_tree
            self.roots.add(new_tree)
        return self.val_to_node_map[val]

    def find(self, val):
        node = self.val_to_node_map[val]
        nodes_on_path = []
        while node.parent!=node:
            nodes_on_path.append(node)
            node = node.parent
        root = node
        for node in nodes_on_path:
            node.parent = root
        return root

    def union(self, val1, val2):
        tree1 = self.find(val1)
        tree2 = self.find(val2)
        if tree1==tree2:
            return

        if tree1.size>tree2.size:
            child, parent = tree2, tree1
        else:
            child, parent = tree1, tree2
        child.parent = parent
        parent.size = parent.size + child.size
        self.roots.discard(child)
        self.max_tree_size = max(self.max_tree_size, parent.size)

    def print(self):
        trees = defaultdict(lambda:[])
        for val in self.vals:
            root = self.find(val)
            trees[root].append(val)

        for root in trees:
            tree = trees[root]
            print(f'{root.val}  -> {[x for x in tree]}')

from collections import defaultdict
class Solution:
    def largestRectangleArea(self, heights: List[int]) -> int:
        uf = UnionFindForest()
        hi_map = defaultdict(lambda:[])
        for i,h in enumerate(heights):
            hi_map[h].append(i)
        heights = sorted(set(heights), reverse=True)
        max_area = 0
        for h in heights:
            h_index_l = hi_map[h]
            for i in h_index_l:
                uf.add(i)
                if i-1 in uf.val_to_node_map:
                    uf.union(i, i-1)
                if i+1 in uf.val_to_node_map:
                    uf.union(i, i+1)
            max_area = max(max_area, uf.max_tree_size*h)

        return max_area


if __name__=='__main__':
    if lc_lib_path.exists():
        s = Solution()

        heights = [2,1,5,6,2,3]
        r = s.largestRectangleArea(heights)
        print(r)
        print('-'*30)

        from testcase import x
        heights = x
        r = s.largestRectangleArea(heights)
        print(r)
        print('-'*30)

