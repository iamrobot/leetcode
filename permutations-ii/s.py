
# load local leetcode lib
import pathlib

lc_lib_path = pathlib.Path('../lc_lib.py')
if lc_lib_path.exists():
    import sys
    sys.path.insert(0, '..')
    from lc_lib import *

old_print = print
def print(*args, **kwargs):
    if lc_lib_path.exists():
        old_print(*args, **kwargs)

from collections import Counter
from copy import copy


class Solution:
    def permuteUnique(self, nums: List[int]) -> List[List[int]]:
        def helper(c):
            if not c:
                yield []
            for key in c:
                c2 = copy(c)
                c2[key] -= 1
                if c2[key]==0:
                    c2.pop(key)

                for r in helper(c2):
                    yield [key] + r

        return list(helper(Counter(nums)))

if __name__=='__main__':
    if lc_lib_path.exists():
        s = Solution()
        x = [1,2,2]
        r = s.permuteUnique(x)
        print(r)
        x = [1,2,3]
        r = s.permuteUnique(x)
        print(r)
