
# load local leetcode lib
import pathlib
lc_lib_path = pathlib.Path('../lc_lib.py')
if lc_lib_path.exists():
    import sys
    sys.path.insert(0, '..')
    from lc_lib import *

old_print = print
def print(*args, **kwargs):
    if lc_lib_path.exists():
        old_print(*args, **kwargs)

from sortedcontainers import SortedList, SortedDict, SortedSet
class Solution:
    def minimumTimeRequired(self, jobs: List[int], k: int) -> int:
        jobs.sort(reverse=True)
        n = len(jobs)
        work_times = [0]*k
        one_worker_max_time = sum(jobs[:(n//k)+1])

        result = float('inf')
        def dfs(i, cur_res):
            nonlocal result
            if i==n:
                result = min(result, cur_res)
                return

            if cur_res>=result:
                return

            job = jobs[i]
            for j in range(k):
                if (j>0 and work_times[j]==work_times[j-1]) or work_times[j]+job>one_worker_max_time:
                    continue
                work_times[j] += job
                dfs(i+1, max(cur_res, work_times[j]))
                work_times[j] -= job

        dfs(0, 0)
        return result

if __name__=='__main__':
    if lc_lib_path.exists():
        s = Solution()

        jobs = [38,49,91,59,14,76,84]
        k = 3
        r = s.minimumTimeRequired(jobs,k)
        print(r)  # 140
        print('-'*30)

        jobs = [254,256,256,254,251,256,254,253,255,251,251,255]
        k = 10
        r = s.minimumTimeRequired(jobs,k)
        print(r)  # 140
        print('-'*30)
