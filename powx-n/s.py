
# load local leetcode lib
import pathlib
lc_lib_path = pathlib.Path('../lc_lib.py')
if lc_lib_path.exists():
    import sys
    sys.path.insert(0, '..')
    from lc_lib import *

old_print = print
def print(*args, **kwargs):
    if lc_lib_path.exists():
        old_print(*args, **kwargs)

import math
class Solution:
    def myPow(self, x: float, n: int) -> float:
        if n==0: return 1

        sign = 1 if n>0 else -1
        n = abs(n)
        result = 1
        cur_pow = x
        while n>0:
            try:
                if n%2!=0:
                    result *= cur_pow
                cur_pow = cur_pow**2
                n = n // 2
            except OverflowError:
                result = math.inf
                break

        result = result if sign==1 else 1/result

        return result

if __name__=='__main__':
    if lc_lib_path.exists():
        sol = Solution()

        x = 2.00000
        n = 10
        r = sol.myPow(x,n)
        print(r)
        print('-'*30)

        x = 2.00000
        n = -2
        r = sol.myPow(x,n)
        print(r)
        print('-'*30)

        x = 0.00001
        n = 2147483647
        r = sol.myPow(x,n)
        print(r)
        print('-'*30)

        x = 2.00000
        n = -2147483648
        r = sol.myPow(x,n)
        print(r)
        print('-'*30)
