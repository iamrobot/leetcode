
# load local leetcode lib
import pathlib

lc_lib_path = pathlib.Path('../lc_lib.py')
if lc_lib_path.exists():
    import sys
    sys.path.insert(0, '..')
    from lc_lib import *

class Solution:
    def lis(self, nums):
        import bisect
        tails = []
        for num in nums:
            i = bisect.bisect_left(tails, num)
            if i==len(tails):
                tails.append(num)
            else:
                tails[i] = num
        return len(tails)

    def lengthOfLIS(self, nums: List[int]) -> int:
        inf = float('inf')
        cur_max_to_length = {
            -inf:0, 
        }
        for num in nums:
            prev_length = 0
            for k,v in cur_max_to_length.items():
                if k<num:
                    prev_length = max(prev_length, v)
            cur_max_to_length[num] = prev_length+1

        return max(cur_max_to_length.values())

if __name__=='__main__':
    if lc_lib_path.exists():
        s = Solution()
        for _ in range(20):
            x = generate_list(vals=list(range(1000000)), length=2**10)
            print(s.lis(x))
