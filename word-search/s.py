
# load local leetcode lib
import pathlib

lc_lib_path = pathlib.Path('../lc_lib.py')
if lc_lib_path.exists():
    import sys
    sys.path.insert(0, '..')
    from lc_lib import *

from collections import deque
from copy import deepcopy


class Solution:
    def exist(self, board: List[List[str]], word: str) -> bool:
        m = len(board)
        n = len(board[0])

        def is_pos_legal(pos):
            i,j = pos
            return 0<=i<m and 0<=j<n

        def get_neighbors(pos):
            i,j = pos
            candidate_pos = [
                (i-1,j),
                (i+1,j),
                (i,j-1),
                (i,j+1),
            ]
            return [pos for pos in candidate_pos if is_pos_legal(pos)]

        first_char_pos_l = []
        for i in range(m):
            for j in range(n):
                if board[i][j]==word[0]:
                    first_char_pos_l.append((i,j))

        cur_l = deque([(set([pos]),pos,word[1:]) for pos in first_char_pos_l])
        while cur_l:
            _visited,last_pos,remain_word = cur_l.pop()
            if remain_word=='':
                return True
            next_char = remain_word[0]

            for pos in get_neighbors(last_pos):
                i,j = pos
                if pos in _visited or board[i][j]!=next_char:
                    continue
                visited = deepcopy(_visited)
                visited.add(pos)
                cur_l.append((visited, pos, remain_word[1:]))

        return False

if __name__=='__main__':
    if lc_lib_path.exists():
        s = Solution()
        board = [["A","B","C","E"],["S","F","E","S"],["A","D","E","E"]]
        word = "ABCESEEEFS"
        r = s.exist(board, word)
        print(r)
