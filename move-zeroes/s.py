
# Definition for singly-linked list.
class ListNode:
    def __init__(self, x):
        self.val = x
        self.next = None

# Definition for a binary tree node.
class TreeNode:
    def __init__(self, val=0, left=None, right=None):
        self.val = val
        self.left = left
        self.right = right

from typing import *


class Solution:
    def moveZeroes(self, nums: List[int]) -> None:
        """
        Do not return anything, modify nums in-place instead.
        """
        from queue import Queue
        zero_count = 0
        zero_start_index = None
        for index,num in enumerate(nums):
            if num==0:
                zero_count += 1
                if zero_start_index==None:
                    zero_start_index = index
                continue

            if zero_count==0:
                continue

            nums[index] = 0
            nums[zero_start_index] = num
            while nums[zero_start_index]!=0:
                zero_start_index += 1
        

if __name__=='__main__':
    s = Solution()
    nums = [0,1,0,3,12]
    s.moveZeroes(nums)
    print(nums)
