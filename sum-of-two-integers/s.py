
# load local leetcode lib
import pathlib
lc_lib_path = pathlib.Path('../lc_lib.py')
if lc_lib_path.exists():
    import sys
    sys.path.insert(0, '..')
    from lc_lib import *

old_print = print
def print(*args, **kwargs):
    if lc_lib_path.exists():
        old_print(*args, **kwargs)

def num_to_bin(num, fix_len=32):
    # or you can just use bin(num)[2:]
    l = []
    while num:
        l.insert(0, num%2)
        num = num // 2
    l.reverse()
    l = l+[0]
    return l


from collections import Counter
from itertools import zip_longest
class Solution:
    def getSum(self, a: int, b: int) -> int:
        s = ''
        bin_a = num_to_bin(a)
        bin_b = num_to_bin(b)
        print(bin_a)
        print(bin_b)

        last = 0
        result = []
        for bit0, bit1 in zip_longest(bin_a, bin_b, fillvalue=0):
            c = Counter([last, bit0, bit1])
            if c[1]==0:
                result.insert(0, 0)
                last = 0
            elif c[1]==1:
                result.insert(0, 1)
                last = 0
            elif c[1]==2:
                result.insert(0, 0)
                last = 1
            elif c[1]==3:
                result.insert(0, 1)
                last = 1
            else:
                print('what happened?')

        return int(''.join([str(x) for x in result]), base=2)

if __name__=='__main__':
    if lc_lib_path.exists():
        s = Solution()

        a = 2
        b = 3
        r = s.getSum(a,b)
        print(r)
        print('-'*30)

