
# load local leetcode lib
import pathlib
lc_lib_path = pathlib.Path('../lc_lib.py')
if lc_lib_path.exists():
    import sys
    sys.path.insert(0, '..')
    from lc_lib import *

old_print = print
def print(*args, **kwargs):
    if lc_lib_path.exists():
        old_print(*args, **kwargs)

class Solution:
    def setZeroes(self, matrix: List[List[int]]) -> None:
        """
        Do not return anything, modify matrix in-place instead.
        """
        m = len(matrix)
        n = len(matrix[0])
        rows = set()
        cols = set()
        for i in range(m):
            for j in range(n):
                if matrix[i][j]==0:
                    rows.add(i)
                    cols.add(j)
        for i in rows:
            for j in range(n):
                matrix[i][j]=0
        for j in cols:
            for i in range(m):
                matrix[i][j]=0

        return matrix

if __name__=='__main__':
    if lc_lib_path.exists():
        s = Solution()

        matrix = [[1,1,1],[1,0,1],[1,1,1]]
        r = s.setZeroes(matrix)
        pprint(r)
        print('-'*30)

