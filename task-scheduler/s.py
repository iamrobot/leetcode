
# load local leetcode lib
import pathlib

lc_lib_path = pathlib.Path('../lc_lib.py')
if lc_lib_path.exists():
    import sys
    sys.path.insert(0, '..')
    from lc_lib import *

old_print = print
def print(*args, **kwargs):
    if lc_lib_path.exists():
        old_print(*args, **kwargs)

from collections import Counter
from sortedcontainers import SortedList
class Solution:
    def leastInterval(self, tasks: List[str], n: int) -> int:
        n_tasks = len(tasks)
        if n==0:
            return n_tasks
        
        result = 0
        tasks_counter = Counter(tasks)
        
        while tasks_counter:
            i = 0
            for task in tasks_counter:
                tasks_counter[task] -= 1
                i += 1
            tasks_counter = {k:v for k,v in tasks_counter.items() if v!=0}
            if tasks_counter:
                i = max(n+1, i)
            result += i
        
        return result


if __name__=='__main__':
    if lc_lib_path.exists():
        s = Solution()

        tasks = ["A","A","A","B","B","B"]
        n = 2
        r = s.leastInterval(tasks,n)
        print(r)
        print('-'*30)

        tasks = ["A","A","A","A","A","A","B","C","D","E","F","G"]
        n = 2
        r = s.leastInterval(tasks,n)
        print(r)
        print('-'*30)

        tasks = ["A","A","A","B","B","B", "C","C","C", "D", "D", "E"]
        n = 2
        r = s.leastInterval(tasks,n)
        print(r)
        print('-'*30)

