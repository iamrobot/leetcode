
# load local leetcode lib
import pathlib

lc_lib_path = pathlib.Path('../lc_lib.py')
if lc_lib_path.exists():
    import sys
    sys.path.insert(0, '..')
    from lc_lib import *

class Solution:
    def sortColors(self, nums: List[int]) -> None:
        """
        Do not return anything, modify nums in-place instead.
        """
        if len(nums)<2:
            return nums

        def swap(i,j):
            tmp_i = nums[i]
            tmp_j = nums[j]
            nums[i] = tmp_j
            nums[j] = tmp_i

        i = 0
        j = 0
        k = len(nums)-1
        while j<=k:
            print(i, j, k, nums)
            if nums[j]==0:
                swap(i, j)
                i += 1
                j += 1
            elif nums[j]==1:
                j += 1
            elif nums[j]==2:
                swap(j, k)
                k -= 1
        
        return nums

if __name__=='__main__':
    if lc_lib_path.exists():
        s = Solution()
        x = [2,0,2,1,1,0]
        r = s.sortColors(x)
        print(r)
        print('')
        x = [2,0,1]
        r = s.sortColors(x)
        print(r)
