
# load local leetcode lib
import pathlib

lc_lib_path = pathlib.Path('../lc_lib.py')
if lc_lib_path.exists():
    import sys
    sys.path.insert(0, '..')
    from lc_lib import *

def generate_grid(rows, cols):
    import random
    grid = []
    for row in range(rows):
        grid.append(random.choices(range(1, 10), k=cols))
    return grid

def print_grid(grid):
    for row in grid:
        l = [str(x) for x in row]
        print(' '.join(l))

class Solution:
    def minPathSum_recursive(self, grid: List[List[int]]) -> int:
        m = len(grid)
        n = len(grid[0])

        def helper(grid, i, j):
            print('we are at', i,j)
            if i==m-1:
                return sum(grid[i][j:])
            if j==n-1:
                return sum([grid[k][j] for k in range(i,m)])

            s1 = helper(grid, i+1, j)
            s2 = helper(grid, i, j+1)
            return min(s1, s2)+grid[i][j]

        return helper(grid, 0, 0)
    
    def minPathSum_dynamic(self, grid):
        m = len(grid)
        n = len(grid[0])
        inf = float('inf')

        for i in range(m):
            for j in range(n):
                if i==0 and j==0:
                    grid[i][j] = grid[0][0]
                else:
                    if i-1<0:
                        s1 = inf
                    else:
                        s1 = grid[i-1][j]
                    if j-1<0:
                        s2 = inf
                    else:
                        s2 = grid[i][j-1]
                    grid[i][j] = grid[i][j] + min(s1, s2)

        return grid[m-1][n-1]

    def minPathSum(self, grid):
        return self.minPathSum_dynamic(grid)


if __name__=='__main__':
    if lc_lib_path.exists():
        s = Solution()
        x = generate_grid(3,3)
        print_grid(x)
        r = s.minPathSum(x)
        print(r)
