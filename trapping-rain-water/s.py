
# load local leetcode lib
import pathlib

lc_lib_path = pathlib.Path('../lc_lib.py')
if lc_lib_path.exists():
    import sys
    sys.path.insert(0, '..')
    from lc_lib import *

old_print = print
def print(*args, **kwargs):
    if lc_lib_path.exists():
        old_print(*args, **kwargs)

from copy import copy


class Solution:
    def trap(self, height: List[int]) -> int:
        inf = float('inf')
        max_from_left_l = []
        max_from_left = -inf
        for i,h in enumerate(height):
            max_from_left = max(h, max_from_left)
            max_from_left_l.append(max_from_left)
        reversed_height = copy(height)
        reversed_height.reverse()
        max_from_right_l = []
        max_from_right = -inf
        for i,h in enumerate(reversed_height):
            max_from_right = max(h, max_from_right)
            max_from_right_l.append(max_from_right)
        max_from_right_l.reverse()
        result = 0
        for i,h in enumerate(height):
            result += min(max_from_left_l[i]-h, max_from_right_l[i]-h)
        return result

if __name__=='__main__':
    if lc_lib_path.exists():
        s = Solution()

        height = [0,1,0,2,1,0,1,3,2,1,2,1]
        r = s.trap(height)
        print(r)
        print('-'*30)

