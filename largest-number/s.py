
# load local leetcode lib
import pathlib
lc_lib_path = pathlib.Path('../lc_lib.py')
if lc_lib_path.exists():
    import sys
    sys.path.insert(0, '..')
    from lc_lib import *

old_print = print
def print(*args, **kwargs):
    if lc_lib_path.exists():
        old_print(*args, **kwargs)

from functools import cmp_to_key
from itertools import dropwhile
class Solution:
    def largestNumber(self, nums: List[int]) -> str:
        def compare(a, b):
            if a+b<b+a:
                return 1
            elif a+b==b+a:
                return 0
            else:
                return -1

        nums = sorted([str(x) for x in nums], key=cmp_to_key(compare))
        print(nums)
        nums = dropwhile(lambda x:x=='0', nums)
        result = ''.join(nums)
        return result if result else '0'

if __name__=='__main__':
    if lc_lib_path.exists():
        s = Solution()

        nums = [3,30,34,5,9]
        r = s.largestNumber(nums)
        print(r)
        print('-'*30)

