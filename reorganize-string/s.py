
# load local leetcode lib
import pathlib
lc_lib_path = pathlib.Path('../lc_lib.py')
if lc_lib_path.exists():
    import sys
    sys.path.insert(0, '..')
    from lc_lib import *

old_print = print
def print(*args, **kwargs):
    if lc_lib_path.exists():
        old_print(*args, **kwargs)

import collections
class Solution:
    def reorganizeString(self, S: str) -> str:
        s = S
        c = collections.Counter(s) 
        s = ''
        last_char = None
        while len(c.keys())>1:
            for k,v in c.most_common(2):
                if k==last_char:
                    continue
                s += k
                c[k] -= 1
                if c[k]==0:
                    c.pop(k)
        if any([v>1 for v in c.values()]):
            return ''
        else:
            return s

if __name__=='__main__':
    if lc_lib_path.exists():
        sol = Solution()

        S = "aab"
        r = sol.reorganizeString(S)
        print(r)
        print('-'*30)

