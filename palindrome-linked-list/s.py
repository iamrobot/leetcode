
# load local leetcode lib
import pathlib

lc_lib_path = pathlib.Path('../lc_lib.py')
if lc_lib_path.exists():
    import sys
    sys.path.insert(0, '..')
    from lc_lib import *

# Definition for singly-linked list.
# class ListNode:
#     def __init__(self, val=0, next=None):
#         self.val = val
#         self.next = next
class Solution:
    def isPalindrome(self, head: ListNode) -> bool:
        if head==None or head.next==None:
            return True

        def yield_from_head(head):
            p = head
            while p.next:
                yield p
                p = p.next
        def yield_fromtail(head):
            p = head
            if p.next:
                yield from yield_fromtail(p.next)
            yield p

        for x1, x2 in zip(yield_from_head(head), yield_fromtail(head)):
            if not x1.val==x2.val:
                return False

        return True

if __name__=='__main__':
    if lc_lib_path.exists():
        s = Solution()
        l = generate_linked_list()
        r = s.isPalindrome(l)
        print(r)
        l = linked_list_from_list([1,2,2,1])
        r = s.isPalindrome(l)
        print(r)
