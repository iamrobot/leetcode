
# load local leetcode lib
import pathlib
lc_lib_path = pathlib.Path('../lc_lib.py')
if lc_lib_path.exists():
    import sys
    sys.path.insert(0, '..')
    from lc_lib import *

old_print = print
def print(*args, **kwargs):
    if lc_lib_path.exists():
        old_print(*args, **kwargs)


class UnionFindNode(object):
    def __init__(self, val):
        self.parent = self
        self.val = val
        self.size = 1

class UnionFindForest(object):
    def __init__(self, vals=[]):
        self.vals = set()
        self.roots = set()
        self.val_to_node_map = dict()
        for val in vals:
            self.add(val)

    def add(self, val):
        if val not in self.vals:
            self.vals.add(val)
            new_tree = UnionFindNode(val)
            self.val_to_node_map[val] = new_tree
            self.roots.add(new_tree)
        return self.val_to_node_map[val]

    def find(self, val):
        node = self.val_to_node_map[val]
        nodes_on_path = []
        while node.parent!=node:
            nodes_on_path.append(node)
            node = node.parent
        root = node
        for node in nodes_on_path:
            node.parent = root
        return root

    def union(self, val1, val2):
        tree1 = self.find(val1)
        tree2 = self.find(val2)
        if tree1==tree2:
            return

        if tree1.size>tree2.size:
            child, parent = tree2, tree1
        else:
            child, parent = tree1, tree2
        child.parent = parent
        parent.size = parent.size + child.size
        self.roots.discard(child)

    def print(self):
        trees = defaultdict(lambda:[])
        for val in self.vals:
            root = self.find(val)
            trees[root].append(val)

        for root in trees:
            tree = trees[root]
            print(f'{root.val}  -> {[x for x in tree]}')

from collections import defaultdict, Counter
class Solution:
    def minimumHammingDistance(self, source: List[int], target: List[int], allowedSwaps: List[List[int]]) -> int:
        n = len(source)
        uf = UnionFindForest()
        for i in range(n):
            uf.add(i)

        for i,j in allowedSwaps:
            uf.union(i,j)

        groups = defaultdict(lambda:set())
        for i in range(n):
            root = uf.find(i)
            groups[root].add(i)

        dis = 0
        for group in groups.values():
            source_vals = [source[i] for i in group]
            target_vals = [target[i] for i in group]
            c0 = Counter(source_vals)
            c1 = Counter(target_vals)
            for k,v in c1.items():
                c1[k] -= min(c0[k], v)

            dis += sum(c1.values())
        
        return dis

if __name__=='__main__':
    if lc_lib_path.exists():
        s = Solution()

        source = [1,2,3,4]
        target = [2,1,4,5]
        allowedSwaps = [[0,1],[2,3]]
        r = s.minimumHammingDistance(source,target,allowedSwaps)
        print(r)
        print('-'*30)

        source = [5,1,2,4,3]
        target = [1,5,4,2,3]
        allowedSwaps = [[0,4],[4,2],[1,3],[1,4]]
        r = s.minimumHammingDistance(source,target,allowedSwaps)
        print(r)
        print('-'*30)

