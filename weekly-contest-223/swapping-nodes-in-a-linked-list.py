
# load local leetcode lib
import pathlib
lc_lib_path = pathlib.Path('../lc_lib.py')
if lc_lib_path.exists():
    import sys
    sys.path.insert(0, '..')
    from lc_lib import *

old_print = print
def print(*args, **kwargs):
    if lc_lib_path.exists():
        old_print(*args, **kwargs)

# Definition for singly-linked list.
# class ListNode:
#     def __init__(self, val=0, next=None):
#         self.val = val
#         self.next = next
class Solution:
    def swapNodes(self, head: ListNode, k: int) -> ListNode:
        nodes = []
        p = head
        while p:
            nodes.append(p)
            p = p.next

        p0 = nodes[k-1]
        p1 = nodes[-k]
        nodes[k-1] = p1
        nodes[-k] = p0
        for i,node in enumerate(nodes):
            if i==len(nodes)-1:
                node.next=None
                return nodes[0]
            node.next = nodes[i+1]

if __name__=='__main__':
    if lc_lib_path.exists():
        s = Solution()

        head = [1,2,3,4,5]
        k = 2
        r = s.swapNodes(head,k)
        print(r)
        print('-'*30)

