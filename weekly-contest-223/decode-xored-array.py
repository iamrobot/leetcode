
# load local leetcode lib
import pathlib
lc_lib_path = pathlib.Path('../lc_lib.py')
if lc_lib_path.exists():
    import sys
    sys.path.insert(0, '..')
    from lc_lib import *

old_print = print
def print(*args, **kwargs):
    if lc_lib_path.exists():
        old_print(*args, **kwargs)

class Solution:
    def decode(self, encoded: List[int], first: int) -> List[int]:
        arr = [first]
        for i,num in enumerate(encoded):
            r = num^arr[i]
            arr.append(r)
        return arr

if __name__=='__main__':
    if lc_lib_path.exists():
        s = Solution()

        encoded = [1,2,3]
        first = 1
        r = s.decode(encoded,first)
        print(r)
        print('-'*30)

