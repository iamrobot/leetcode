
# load local leetcode lib
import pathlib

lc_lib_path = pathlib.Path('../lc_lib.py')
if lc_lib_path.exists():
    import sys
    sys.path.insert(0, '..')
    from lc_lib import *

old_print = print
def print(*args, **kwargs):
    if lc_lib_path.exists():
        old_print(*args, **kwargs)

class CircleDetected(Exception):
    pass

from collections import defaultdict


class Solution:
    def canFinish(self, numCourses: int, prerequisites: List[List[int]]) -> bool:
        map = defaultdict(lambda:set())
        reverse_map = defaultdict(lambda:set())
        seen = set()
        for u,v in prerequisites:
            map[u].add(v)
            reverse_map[v].add(u)
            seen.add(u)
            seen.add(v)

        visited = set()
        in_stack = set()
        stack = []
        def dfs(i):
            print(f'dfs {i}')
            if i not in seen:
                return

            if i in visited:
                if not i in in_stack:
                    raise CircleDetected('')
            else:
                visited.add(i)
                for u in map[i]:
                    dfs(u)
                stack.append(i)
                in_stack.add(i)

        for i in range(numCourses):
            try:
                dfs(i)
            except CircleDetected:
                return False

        print(stack)
        return True

if __name__=='__main__':
    if lc_lib_path.exists():
        s = Solution()
        x = [[1,0],[2,6],[1,7],[5,1],[6,4],[7,0],[0,5]]
        r = s.canFinish(8, x)
        print(r)
