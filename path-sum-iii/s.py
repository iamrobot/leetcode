
# load local leetcode lib
import pathlib
lc_lib_path = pathlib.Path('../lc_lib.py')
if lc_lib_path.exists():
    import sys
    sys.path.insert(0, '..')
    from lc_lib import *

old_print = print
def print(*args, **kwargs):
    if lc_lib_path.exists():
        old_print(*args, **kwargs)

# Definition for a binary tree node.
# class TreeNode:
#     def __init__(self, val=0, left=None, right=None):
#         self.val = val
#         self.left = left
#         self.right = right
class Solution:
    def pathSum(self, root: Optional[TreeNode], targetSum: int) -> int:
        if root==None:
            return 0
        
        target = targetSum
        result = 0
        import collections
        d = collections.defaultdict(lambda: 0)
        d[0] = 1
        def helper(root, cumsum):
            nonlocal result
            if root==None:
                return
            cumsum = cumsum+root.val
            result += d[cumsum-target]
            d[cumsum] += 1
            helper(root.left, cumsum)
            helper(root.right, cumsum)
            d[cumsum] -= 1

        helper(root, 0) 
        return result


if __name__=='__main__':
    if lc_lib_path.exists():
        sol = Solution()

        root = [10,5,-3,3,2,None,11,3,-2,None,1]
        root = BinaryTree.from_list(root)
        targetSum = 8
        r = sol.pathSum(root,targetSum)
        print(r)
        print('-'*30)

