
# load local leetcode lib
import pathlib
lc_lib_path = pathlib.Path('../lc_lib.py')
if lc_lib_path.exists():
    import sys
    sys.path.insert(0, '..')
    from lc_lib import *

old_print = print
def print(*args, **kwargs):
    if lc_lib_path.exists():
        old_print(*args, **kwargs)

import bisect
class MedianFinder:

    def __init__(self):
        """
        initialize your data structure here.
        """
        self.data = []
        

    def addNum(self, num: int) -> None:
        data = self.data
        bisect.insort(data, num)
        

    def findMedian(self) -> float:
        data = self.data
        n = len(data)
        half = n//2
        if n%2==0:
            return (data[half]+data[half-1])/2
        else:
            return data[half]
        


# Your MedianFinder object will be instantiated and called as such:
# obj = MedianFinder()
# obj.addNum(num)
# param_2 = obj.findMedian()
        pass

if __name__=='__main__':
    if lc_lib_path.exists():
        pass
        #s = Solution()

