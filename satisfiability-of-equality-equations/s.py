
# load local leetcode lib
import pathlib
lc_lib_path = pathlib.Path('../lc_lib.py')
if lc_lib_path.exists():
    import sys
    sys.path.insert(0, '..')
    from lc_lib import *

old_print = print
def print(*args, **kwargs):
    if lc_lib_path.exists():
        old_print(*args, **kwargs)

from collections import defaultdict

class UnionFindNode(object):
    def __init__(self, val):
        self.parent = self
        self.val = val
        self.size = 1

class UnionFindForest(object):
    def __init__(self, vals=[]):
        self.vals = set()
        self.roots = set()
        self.val_to_node_map = dict()
        for val in vals:
            self.add(val)

    def add(self, val):
        if val not in self.vals:
            self.vals.add(val)
            new_tree = UnionFindNode(val)
            self.val_to_node_map[val] = new_tree
            self.roots.add(new_tree)
        return self.val_to_node_map[val]

    def find(self, val):
        node = self.val_to_node_map[val]
        nodes_on_path = []
        while node.parent!=node:
            nodes_on_path.append(node)
            node = node.parent
        root = node
        for node in nodes_on_path:
            node.parent = root
        return root

    def union(self, val1, val2):
        tree1 = self.find(val1)
        tree2 = self.find(val2)
        if tree1==tree2:
            return

        if tree1.size>tree2.size:
            child, parent = tree2, tree1
        else:
            child, parent = tree1, tree2
        child.parent = parent
        parent.size = parent.size + child.size
        self.roots.discard(child)

    def print(self):
        trees = defaultdict(lambda:[])
        for val in self.vals:
            root = self.find(val)
            trees[root].append(val)

        for root in trees:
            tree = trees[root]
            print(f'{root.val}  -> {[x for x in tree]}')

class Solution:
    def equationsPossible(self, equations: List[str]) -> bool:
        uff = UnionFindForest()
        def iter_equations():
            for equation in equations:
                l_val, equal_or_not, r_val = equation[0], equation[1:3], equation[-1]
                l_node = uff.add(l_val)
                r_node = uff.add(r_val)
                yield l_val, equal_or_not, r_val

        for l_val, equal_or_not, r_val in iter_equations():
            if equal_or_not == '==':
                uff.union(l_val, r_val)

        for l_val, equal_or_not, r_val in iter_equations():
            if equal_or_not == '!=':
                l_tree = uff.find(l_val)
                r_tree = uff.find(r_val)
                if l_tree == r_tree:
                    return False

        return True


if __name__=='__main__':
    if lc_lib_path.exists():
        sol = Solution()

        equations = ["a==b","b!=a"]
        r = sol.equationsPossible(equations)
        print(r)
        print('-'*30)

        equations = ["a==b","b==a"]
        r = sol.equationsPossible(equations)
        print(r)
        print('-'*30)

