
# load local leetcode lib
import pathlib

lc_lib_path = pathlib.Path('../lc_lib.py')
if lc_lib_path.exists():
    import sys
    sys.path.insert(0, '..')
    from lc_lib import *

from functools import lru_cache


class Solution:
    @lru_cache(1024)
    def uniquePaths(self, m: int, n: int) -> int:
        if m==1:
            return 1
        if n==1:
            return 1

        return self.uniquePaths(m-1, n)+self.uniquePaths(m, n-1)

if __name__=='__main__':
    if lc_lib_path.exists():
        s = Solution()
        r = s.uniquePaths(3, 7)
        print(r)
