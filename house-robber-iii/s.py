
# load local leetcode lib
import pathlib

lc_lib_path = pathlib.Path('../lc_lib.py')
if lc_lib_path.exists():
    import sys
    sys.path.insert(0, '..')
    from lc_lib import *

# Definition for a binary tree node.
# class TreeNode:
#     def __init__(self, val=0, left=None, right=None):
#         self.val = val
#         self.left = left
#         self.right = right
from functools import lru_cache


class Solution:
    def rob_recursive(self, root: TreeNode) -> int:
        @lru_cache(1024)
        def helper(root, rob_root_allowed=True):
            if not root:
                return 0

            left_max = helper(root.left)
            right_max = helper(root.right)

            if rob_root_allowed:
                return max([
                    left_max + right_max,
                    helper(root.right, rob_root_allowed=False) + \
                    helper(root.left, rob_root_allowed=False) + root.val,
                ])
            else:
                return left_max+right_max

        return helper(root)

    def rob(self, root):
        if not root:
            return 0

        cur_level_nodes = [(root,0,0)]
        next_level_nodes = []
        cur_level_max_rob = 0
        max_rob = 0
        while cur_level_nodes:
            for node,without_parent,with_parent in cur_level_nodes:

                without_node = max(without_parent, with_parent)
                with_node = without_parent + node.val
                cur_level_max_rob += max(without_node, with_node)

                if node.left:
                    next_level_nodes.append((node.left, without_node, with_node))
                if node.right:
                    next_level_nodes.append((node.right, without_node, with_node))

            cur_level_nodes = next_level_nodes
            next_level_nodes = []

            print(cur_level_max_rob)
            max_rob = cur_level_max_rob
            cur_level_max_rob = 0

        return max_rob

if __name__=='__main__':
    if lc_lib_path.exists():
        s = Solution()
        x = None # input
        r = s.method(x)
        print(r)
