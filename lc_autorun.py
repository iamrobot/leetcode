#!/usr/bin/env python

import logging
import os
import pathlib
import subprocess
import signal
import sys
import time
from collections import defaultdict
from subprocess import TimeoutExpired

from watchdog.events import LoggingEventHandler, PatternMatchingEventHandler
from watchdog.observers import Observer


class AutorunEventHandler(LoggingEventHandler):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.last_run = defaultdict(lambda:0)

    def on_modified(self, event):
        filepath = pathlib.Path(event.src_path).expanduser().absolute()
        if any([
            not filepath.suffix=='.py',
            filepath.parent==pathlib.Path('~/leetcode/'),
            time.time() - self.last_run[filepath] < 5,
        ]):
            return

        print_line(f'{filepath} changed, copying it now')
        p = subprocess.run(f'pbcopy < {filepath}', shell=True)

        print_line(f'{filepath} changed, running it now')
        self.last_run[filepath] = time.time()
        # sometimes it will fail to terminate, since the child process here is the shell process
        # so p.terminate() would terminate child shell process, but the python process is still 
        # running, so we use a process group, sending a terminate to all processes in the group
        # for details: https://stackoverflow.com/questions/4789837/how-to-terminate-a-python-subprocess-launched-with-shell-true
        p = subprocess.Popen(f'python {filepath}', preexec_fn=os.setsid, \
                cwd=filepath.parent, text=True, shell=True)
        try:
            p.wait(timeout=3)
        except TimeoutExpired:
            print_line('TimeoutExpired, force quit')
            #p.terminate()
            os.killpg(os.getpgid(p.pid), signal.SIGTERM)
            p.wait()
        print_line('*'*70)
        print_line('*'*70)
        print_line('*'*70)
        print_line('')

    def on_created(self, event):
        return
        print_line(event)

    def on_deleted(self, event):
        return
        print_line(event)

    def on_moved(self, event):
        return
        print_line(event)

if __name__ == "__main__":
    logging.basicConfig(level=logging.INFO,
                        format='%(asctime)s - %(message)s',
                        datefmt='%Y-%m-%d %H:%M:%S')
    root_dir = pathlib.Path('~/leetcode/').expanduser().absolute()
    path = root_dir
    event_handler = AutorunEventHandler()
    observer = Observer()
    observer.schedule(event_handler, path, recursive=True)
    observer.start()
    try:
        while observer.is_alive():
            observer.join(1)
    except KeyboardInterrupt:
        observer.stop()
    observer.join()
