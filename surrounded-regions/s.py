
# load local leetcode lib
import pathlib
lc_lib_path = pathlib.Path('../lc_lib.py')
if lc_lib_path.exists():
    import sys
    sys.path.insert(0, '..')
    from lc_lib import *

old_print = print
def print(*args, **kwargs):
    if lc_lib_path.exists():
        old_print(*args, **kwargs)

from collections import deque
class Solution:
    def solve(self, board: List[List[str]]) -> None:
        """
        Do not return anything, modify board in-place instead.
        """
        matrix = Matrix(board)
        m = matrix.m
        n = matrix.n

        def bfs_find_not_surrounded(pos):
            q = deque([pos])
            while q:
                pos = q.popleft()
                not_surrounded_set.add(pos)
                for neighbor in matrix.get_neighbors(pos):
                    i,j = neighbor
                    if board[i][j]=='O' and not neighbor in not_surrounded_set:
                        q.append(neighbor)
                        not_surrounded_set.add(neighbor)
                        
        all_O = set()
        not_surrounded_set = set()
        for pos,char in matrix:
            i,j = pos
            if char=='O':
                all_O.add((i,j))
            if not any([i==0, i==m-1, j==0, j==n-1]):
                continue
            if char=='X':
                continue
            bfs_find_not_surrounded((i,j))

        for pos in all_O:
            if pos not in not_surrounded_set:
                i,j = pos
                matrix[i][j] = 'X'


if __name__=='__main__':
    if lc_lib_path.exists():
        s = Solution()

        board = [["X","X","X","X"],["X","O","O","X"],["X","X","O","X"],["X","O","X","X"]]
        pprint(board) 
        r = s.solve(board)
        pprint(board) 
        print(r)
        print('-'*30)

