
# load local leetcode lib
import pathlib

lc_lib_path = pathlib.Path('../lc_lib.py')
if lc_lib_path.exists():
    import sys
    sys.path.insert(0, '..')
    from lc_lib import *

import bisect


class Solution:
    def searchRange(self, nums: List[int], target: int) -> List[int]:
        if not nums:
            return [-1,-1]

        target_index = bisect.bisect_left(nums, target)
        if target_index == len(nums) or nums[target_index]!=target:
            return [-1,-1]

        left = bisect.bisect_right(nums, target-1)
        right = bisect.bisect_left(nums, target+1)
        return [left, right-1]

if __name__=='__main__':
    if lc_lib_path.exists():
        s = Solution()
        x = [5,7,7,8,8,10]
        r = s.searchRange(x, 8)
        print(r)
        r = s.searchRange(x, 3)
        print(r)

        x = [2,2]
        r = s.searchRange(x, 3)
        print(r)
