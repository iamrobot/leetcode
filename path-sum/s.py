
# load local leetcode lib
import pathlib

lc_lib_path = pathlib.Path('../lc_lib.py')
if lc_lib_path.exists():
    import sys
    sys.path.insert(0, '..')
    from lc_lib import *

# Definition for a binary tree node.
# class TreeNode:
#     def __init__(self, val=0, left=None, right=None):
#         self.val = val
#         self.left = left
#         self.right = right
class Solution:
    def hasPathSum(self, root: TreeNode, sum: int) -> bool:
        def dfs(root, sum_before_root):
            sum_till_root = sum_before_root + root.val
            if root.left==None and root.right==None:
                return sum_till_root==sum
            else:
                if root.left:
                    left_result = dfs(root.left, sum_till_root)
                else:
                    left_result = False
                if root.right:
                    right_result = dfs(root.right, sum_till_root)
                else:
                    right_result = False

                return left_result or right_result

        if not root:
            return False

        return dfs(root, 0)

if __name__=='__main__':
    if lc_lib_path.exists():
        s = Solution()
        x = None # input
        r = s.method(x)
        print(r)
