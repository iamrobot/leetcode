
# load local leetcode lib
import pathlib

lc_lib_path = pathlib.Path('../lc_lib.py')
if lc_lib_path.exists():
    import sys
    sys.path.insert(0, '..')
    from lc_lib import *

old_print = print
def print(*args, **kwargs):
    if lc_lib_path.exists():
        old_print(*args, **kwargs)

from collections import Counter


class Solution:
    def maximumBinaryString(self, binary: str) -> str:
        l = [int(x) for x in binary]
        c = Counter(l)
        if c[0]<=1:
            return binary
        first_0 = l.index(0)
        ones_before_first_0 = first_0
        other_ones = c[1] - ones_before_first_0
        return '1'*first_0 + '1'*(c[0]-1) + '0' + '1'*other_ones

if __name__=='__main__':
    if lc_lib_path.exists():
        s = Solution()

        binary = "000110"
        r = s.maximumBinaryString(binary)
        print(r)
        print('-'*30)

