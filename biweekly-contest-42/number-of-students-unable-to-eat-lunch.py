
# load local leetcode lib
import pathlib

lc_lib_path = pathlib.Path('../lc_lib.py')
if lc_lib_path.exists():
    import sys
    sys.path.insert(0, '..')
    from lc_lib import *

old_print = print
def print(*args, **kwargs):
    if lc_lib_path.exists():
        old_print(*args, **kwargs)

from collections import Counter


class Solution:
    def countStudents(self, students: List[int], sandwiches: List[int]) -> int:
        students_counter = Counter(students)
        n = len(students)
        eat = 0
        while sandwiches:
            sandwiche = sandwiches[0]
            if students_counter[sandwiche]:
                students_counter[sandwiche] -= 1
                eat += 1
                sandwiches = sandwiches[1:]
            else:
                break
        return n-eat

if __name__=='__main__':
    if lc_lib_path.exists():
        s = Solution()

        students = [1,1,0,0]
        sandwiches = [0,1,0,1]
        r = s.countStudents(students,sandwiches)
        print(r)
        print('-'*30)

        students = [1,1,1,0,0,1]
        sandwiches = [1,0,0,0,1,1]
        r = s.countStudents(students,sandwiches)
        print(r)
        print('-'*30)

