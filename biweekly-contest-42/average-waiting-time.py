
# load local leetcode lib
import pathlib

lc_lib_path = pathlib.Path('../lc_lib.py')
if lc_lib_path.exists():
    import sys
    sys.path.insert(0, '..')
    from lc_lib import *

old_print = print
def print(*args, **kwargs):
    if lc_lib_path.exists():
        old_print(*args, **kwargs)

class Solution:
    def averageWaitingTime(self, customers: List[List[int]]) -> float:
        busy_time = 0
        wait_time = 0
        for at,pt in customers:
            print(at, pt, busy_time, wait_time)
            if at<busy_time:
                next_bust_time = busy_time + pt
                wait_time += next_bust_time - at
                busy_time = next_bust_time
            else:
                busy_time = at + pt
                wait_time += pt
            print(at, pt, busy_time, wait_time)
            print('')

        return wait_time / len(customers)
                    
if __name__=='__main__':
    if lc_lib_path.exists():
        s = Solution()

        customers = [[2,3],[6,3],[7,5],[11,3],[15,2],[18,1]]
        r = s.averageWaitingTime(customers)
        print(r)
        print('-'*30)
