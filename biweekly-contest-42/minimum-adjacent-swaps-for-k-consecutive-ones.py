
# load local leetcode lib
import pathlib

lc_lib_path = pathlib.Path('../lc_lib.py')
if lc_lib_path.exists():
    import sys
    sys.path.insert(0, '..')
    from lc_lib import *

old_print = print
def print(*args, **kwargs):
    if lc_lib_path.exists():
        old_print(*args, **kwargs)

class Solution:
    def minMoves(self, nums: List[int], k: int) -> int:
        pos_l = [i for i,num in enumerate(nums) if num==1]
        print('k:', k)
        print('pos_l:', pos_l)
        if k%2==0:
            j = (k-2)//2
        else:
            j = k//2

        print('j:', j)

        left_l = []
        right_l = []
        s_left = 0
        s_right = 0
        first_time = True
        allowed_center_pos_l = set(pos_l[j:len(pos_l)-j])
        print('allowed_center_pos_l:', allowed_center_pos_l)
        for i,pos in enumerate(pos_l):
            if pos not in allowed_center_pos_l:
                left_l.append(None)
                right_l.append(None)
                continue

            if first_time:
                left_dis = 0
                for a in range(j):
                    left_dis += pos_l[i-a]-pos_l[i-a-1]-1
                    s_left += left_dis

                right_dis = 0
                for a in range(j):
                    right_dis += pos_l[i+a+1]-pos_l[i+a]-1
                    s_right += right_dis

                first_time = False
            else:
                this_step_dis = pos_l[i]-pos_l[i-1]-1
                s_left -= left_dis
                s_left += this_step_dis*j
                left_dis += this_step_dis
                left_dis -= pos_l[i-j]-pos_l[i-j-1]-1

                s_right -= (this_step_dis)*j
                right_dis -= this_step_dis
                right_dis += pos_l[i+j] - pos_l[i+j-1] - 1
                s_right += right_dis

            left_l.append(s_left)
            right_l.append(s_right)
            

        print(left_l)
        print(right_l)
        minmoves = float('inf')
        for i,pos in enumerate(pos_l):
            if pos not in allowed_center_pos_l:
                continue
            if k%2==0:
                if i+1<len(pos_l) and right_l[i+1]!=None:
                    moves = left_l[i]+right_l[i+1]+(j+1)*(pos_l[i+1]-pos_l[i]-1)
                    minmoves = min(minmoves, moves)
            else:
                minmoves = min(minmoves, left_l[i]+right_l[i]) 
            
        return minmoves


if __name__=='__main__':
    if lc_lib_path.exists():
        s = Solution()

        nums = [1,0,0,0,0,0,1,1]
        k = 3
        r = s.minMoves(nums,k)
        print(r)
        print('-'*30)


        nums = [1,0,1,1,1,1,1,1,1,1,1,1,1,1,1]
        k = 14
        r = s.minMoves(nums,k)
        print(r)
        print('-'*30)

        nums = [1,0,0,1,0,1,1,1,0,1,1]
        k = 7
        r = s.minMoves(nums,k)
        print(r)
        print('-'*30)
