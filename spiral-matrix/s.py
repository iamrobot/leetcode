
# load local leetcode lib
import pathlib
lc_lib_path = pathlib.Path('../lc_lib.py')
if lc_lib_path.exists():
    import sys
    sys.path.insert(0, '..')
    from lc_lib import *

old_print = print_line
def print(*args, **kwargs):
    if lc_lib_path.exists():
        old_print(*args, **kwargs)

class Solution:
    def spiralOrder(self, matrix: List[List[int]]) -> List[int]:
        m = len(matrix)
        n = len(matrix[0])
        import itertools
        directions = itertools.cycle([
                (0,1),
                (1,0),
                (0,-1),
                (-1,0),
            ])
        direction = next(directions)
        i = j = 0
        result = []
        while True:
            result.append(matrix[i][j])
            matrix[i][j] = None
            for _ in range(2):
                inc_i, inc_j = direction
                next_i = i+inc_i
                next_j = j+inc_j
                if not(next_i==m or next_j==n or matrix[next_i][next_j]==None):
                    i = next_i
                    j = next_j
                    break
                direction = next(directions)
            else:
                return result

if __name__=='__main__':
    if lc_lib_path.exists():
        s = Solution()

        matrix = [[1,2,3],[4,5,6],[7,8,9]]
        r = s.spiralOrder(matrix)
        print(r)
        print('-'*30)

