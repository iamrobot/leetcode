
# load local leetcode lib
import pathlib
lc_lib_path = pathlib.Path('../lc_lib.py')
if lc_lib_path.exists():
    import sys
    sys.path.insert(0, '..')
    from lc_lib import *

old_print = print
def print(*args, **kwargs):
    if lc_lib_path.exists():
        old_print(*args, **kwargs)

# Definition for singly-linked list.
# class ListNode:
#     def __init__(self, val=0, next=None):
#         self.val = val
#         self.next = next
class Solution:
    def sortList(self, head: ListNode) -> ListNode:
        p = head
        l = []
        while p:
            l.append((p, p.val))
            p = p.next
        sorted_l = sorted(l, key=lambda x:x[-1])
        l = [x[0] for x in sorted_l]
        for i,p in enumerate(l):
            if i==len(l)-1:
                p.next=None
                return l[0]
            p.next = l[i+1]

if __name__=='__main__':
    if lc_lib_path.exists():
        s = Solution()

        head = [4,2,1,3]
        r = s.sortList(head)
        print(r)
        print('-'*30)

