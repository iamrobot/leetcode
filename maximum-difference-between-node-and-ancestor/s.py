
# load local leetcode lib
import pathlib
lc_lib_path = pathlib.Path('../lc_lib.py')
if lc_lib_path.exists():
    import sys
    sys.path.insert(0, '..')
    from lc_lib import *

old_print = print
def print(*args, **kwargs):
    if lc_lib_path.exists():
        old_print(*args, **kwargs)

# Definition for a binary tree node.
# class TreeNode:
#     def __init__(self, val=0, left=None, right=None):
#         self.val = val
#         self.left = left
#         self.right = right
class Solution:
    def maxAncestorDiff(self, root: Optional[TreeNode]) -> int:
        def helper(root, ancestor_max, ancestor_min):
            if root==None:
                return 0

            new_ancestor_max = max(ancestor_max, root.val)
            new_ancestor_min = min(ancestor_min, root.val)
            return max(
                    abs(helper(root.left, new_ancestor_max, new_ancestor_min)),
                    abs(helper(root.right, new_ancestor_max, new_ancestor_min)),
                    abs(ancestor_max - root.val),
                    abs(ancestor_min - root.val),
                    )

        return helper(root, root.val, root.val)

if __name__=='__main__':
    if lc_lib_path.exists():
        sol = Solution()

        root = [8,3,10,1,6,None,14,None,None,4,7,13]
        root = BinaryTree.from_list(root)
        r = sol.maxAncestorDiff(root)
        print(r)
        print('-'*30)

