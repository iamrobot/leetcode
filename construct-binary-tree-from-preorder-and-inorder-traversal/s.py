
# load local leetcode lib
import pathlib

lc_lib_path = pathlib.Path('../lc_lib.py')
if lc_lib_path.exists():
    import sys
    sys.path.insert(0, '..')
    from lc_lib import *

old_print = print
def print(*args, **kwargs):
    if lc_lib_path.exists():
        old_print(*args, **kwargs)

# Definition for a binary tree node.
# class TreeNode:
#     def __init__(self, val=0, left=None, right=None):
#         self.val = val
#         self.left = left
#         self.right = right
class Solution:
    def buildTree(self, preorder: List[int], inorder: List[int]) -> TreeNode:
        def helper(preorder, inorder):
            if len(preorder)==0:
                return None

            root_val = preorder[0]
            root = TreeNode(root_val)
            root_index = inorder.index(root_val)

            inorder_left_vals = inorder[:root_index]
            inorder_left_vals_set = set(inorder_left_vals)
            preorder_left_vals = [x for x in preorder if x in inorder_left_vals_set]
            left = helper(preorder_left_vals, inorder_left_vals)

            inorder_right_vals = inorder[root_index+1:]
            inorder_right_vals_set = set(inorder_right_vals)
            preorder_right_vals = [x for x in preorder if x in inorder_right_vals_set]
            right = helper(preorder_right_vals, inorder_right_vals)

            root.left = left
            root.right = right
            return root

        return helper(preorder, inorder)

if __name__=='__main__':
    if lc_lib_path.exists():
        s = Solution()

        preorder = [3,9,20,15,7]
        inorder = [9,3,15,20,7]
        r = s.buildTree(preorder,inorder)
        print(r)
        print('-'*30)

