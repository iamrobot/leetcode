
# load local leetcode lib
import pathlib
lc_lib_path = pathlib.Path('../lc_lib.py')
if lc_lib_path.exists():
    import sys
    sys.path.insert(0, '..')
    from lc_lib import *

old_print = print
def print(*args, **kwargs):
    if lc_lib_path.exists():
        old_print(*args, **kwargs)

import itertools
class Solution:
    def maxProfit(self, prices: List[int]) -> int:
        s = 0
        for p0, p1 in zip(prices[:-1], prices[1:]):
            if p1>p0:
                s += p1 - p0
        return s

if __name__=='__main__':
    if lc_lib_path.exists():
        sol = Solution()

        prices = [7,1,5,3,6,4]
        r = sol.maxProfit(prices)
        print(r)
        print('-'*30)

