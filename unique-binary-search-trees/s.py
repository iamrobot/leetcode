
# load local leetcode lib
import pathlib

lc_lib_path = pathlib.Path('../lc_lib.py')
if lc_lib_path.exists():
    import sys
    sys.path.insert(0, '..')
    from lc_lib import *

from functools import lru_cache


class Solution:
    @lru_cache
    def numTrees(self, n: int) -> int:
        if n<2:
            return 1
        if n==2:
            return 2

        num = 0
        for i in range(1, n+1):
            num += self.numTrees(i-1)*self.numTrees(n-i)
        
        return num

if __name__=='__main__':
    if lc_lib_path.exists():
        s = Solution()
        r = s.numTrees(3)
        print(r)
