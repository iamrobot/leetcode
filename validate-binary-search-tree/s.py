
# load local leetcode lib
import pathlib

lc_lib_path = pathlib.Path('../lc_lib.py')
if lc_lib_path.exists():
    import sys
    sys.path.insert(0, '..')
    from lc_lib import *

# Definition for a binary tree node.
# class TreeNode:
#     def __init__(self, val=0, left=None, right=None):
#         self.val = val
#         self.left = left
#         self.right = right
class Solution:
    def f(self, root: TreeNode) -> bool:
        if root.left:
            yield from self.f(root.left)
        yield root
        if root.right:
            yield from self.f(root.right)

    def isValidBST(self, root: TreeNode) -> bool:
        import math
        last_val = -math.inf
        for x in self.f(root):
            val = x.val
            if val<=last_val:
                return False
            last_val = val
        return True

if __name__=='__main__':
    if lc_lib_path.exists():
        s = Solution()
        s.method()
