
# load local leetcode lib
import pathlib

lc_lib_path = pathlib.Path('../lc_lib.py')
if lc_lib_path.exists():
    import sys
    sys.path.insert(0, '..')
    from lc_lib import *

class Solution:
    def generateParenthesis(self, n: int) -> List[str]:
        def helper(n):
            seen = set()
            if n==1:
                yield '()', [0,1,2]
            else:
                for s,insert_places in helper(n-1):
                    for i in insert_places:
                        out_s = s[:i] + '()' + s[i:]
                        if out_s not in seen:
                            seen.add(out_s)
                            yield out_s, [i,i+1,i+2] + [j if j<i else j+2 for j in insert_places]

        r = list(helper(n))
        return [i[0] for i in r]

if __name__=='__main__':
    if lc_lib_path.exists():
        s = Solution()
        x = 8# input
        r = s.generateParenthesis(x)
        print(r)
