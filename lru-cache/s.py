
# load local leetcode lib
import pathlib

lc_lib_path = pathlib.Path('../lc_lib.py')
if lc_lib_path.exists():
    import sys
    sys.path.insert(0, '..')
    from lc_lib import *

from collections import deque


class LRUCache:

    def __init__(self, capacity: int):
        self.capacity = capacity
        self.d = dict()
        self.step = 0
        self.last_access_map = dict()
        self.access_records = deque()
        

    def update_access(self, key):
        self.last_access_map[key] = self.step
        self.access_records.append((key, self.step))
        self.step += 1
        
    def get(self, key: int) -> int:
        if key not in self.d:
            return -1
        result = self.d[key]
        self.update_access(key)
        return result
        

    def put(self, key: int, value: int) -> None:
        if len(self.d)==self.capacity and key not in self.d:
            while True:
                to_remove_key, last_access = self.access_records.popleft()
                if last_access == self.last_access_map[to_remove_key]:
                    break
            self.d.pop(to_remove_key)
        self.d[key] = value
        self.update_access(key)
        


# Your LRUCache object will be instantiated and called as such:
# obj = LRUCache(capacity)
# param_1 = obj.get(key)
# obj.put(key,value)
        pass

if __name__=='__main__':
    if lc_lib_path.exists():
        s = LRUCache(2)
        x = [[2],[2,1],[1,1],[2,3],[4,1],[1],[2]]
        for i in x[1:]:
            print(s.d)
            print(i)
            print(s.access_records)
            if len(i)==2:
                print(s.put(i[0], i[1]))
            else:
                print(s.get(i[0]))

            print(s.d)
            print('')
