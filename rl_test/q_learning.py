#!/usr/bin/env python

import os
import sys
import gym
import random
import subprocess
import numpy as np
from tqdm import tqdm
from pprint import pprint
from time import sleep

def zeros(rows, cols):
    return [[0]*cols for _ in range(rows)]


def zeros_like(matrix):
    rows = len(matrix)
    if rows==0: return []
    cols = len(matrix[0])
    return [[0]*cols for _ in range(rows)]


def print_frames(frames):
    for i, frame in enumerate(frames):
        print('\n'*20)
        print(frame[0])
        print(f"Timestep: {i + 1}")
        print(f"State: {frame[1]}")
        print(f"Action: {frame[2]}")
        print(f"Reward: {frame[3]}")
        print('')
        sleep(.03)



env = gym.make("Taxi-v3").env
env.reset()

print(f'action space {env.action_space}')
print(f'state space {env.observation_space}')

lr = 0.1
discount = 0.5
epsilon = 0.9

q_table = zeros(env.observation_space.n, env.action_space.n)
q_table = np.zeros((env.observation_space.n, env.action_space.n))
for epoch in tqdm(range(10000)):
    state = env.reset()
    done = False
    while not done:
        if random.uniform(0,1)<epsilon:
            action = env.action_space.sample()
        else:
            action = np.argmax(q_table[state])
        next_state, reward, done, info = env.step(action)
        next_max_reward = np.max(q_table[next_state])
        q_table[state][action] = \
                (1-lr)*q_table[state][action] + \
                lr*(reward + discount*max(q_table[next_state]))

        state = next_state

pprint(q_table)

for i in range(30):
    steps = 0
    penalty_count = 0

    frames = []
    done = False

    state = env.reset()
    done = False
    while not done and steps<3000:
        action = np.argmax(q_table[state])
        state, reward, done, info = env.step(action)

        if reward==-10:
            penalty_count += 1

        frames.append([env.render(mode='ansi'), state, action, reward])

        steps += 1

    print(f'steps taken {steps}')
    print(f'penalty_count: {penalty_count}')

