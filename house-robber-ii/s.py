
# load local leetcode lib
import pathlib

lc_lib_path = pathlib.Path('../lc_lib.py')
if lc_lib_path.exists():
    import sys
    sys.path.insert(0, '..')
    from lc_lib import *

from functools import lru_cache


class Solution:
    def rob(self, nums: List[int]) -> int:

        def helper(nums):
            if len(nums)==0:
                return 0
            elif len(nums)<=2:
                return max(nums)

            m2, m1 = nums[0], max(nums[:2])
            cur_max = m1
            for num in nums[2:]:
                cur_max = max(m2+num, m1)
                m2 = m1
                m1 = cur_max
            return cur_max

        if not nums:
            return 0

        rob_0 = nums[0] + helper(nums[2:-1])
        no_rob_0 = helper(nums[1:])
        return max(rob_0, no_rob_0)

if __name__=='__main__':
    if lc_lib_path.exists():
        s = Solution()
        x = [1,2,3,1]# input
        r = s.rob(x)
        print(r)

        x = [2,3,2]# input
        r = s.rob(x)
        print(r)
