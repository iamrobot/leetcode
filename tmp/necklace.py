#!/usr/bin/env python

def cut(nums, k):
    if k==0:
        return False

    s = sum(nums)
    if s%k!=0:
        return False
    groups = [0]*k
    group_sum = s // k
    nums.sort(reverse=True)
    if nums[0]>group_sum:
        return False

    result = False

    def can_cut(i):
        nonlocal result
        if i==len(nums):
            result = True
            return

        current_num = nums[i]

        for j in range(k):
            if (j>0 and groups[j]==groups[j-1]) or groups[j]+current_num>group_sum:
                continue
            if groups[j] + current_num <= group_sum:
                groups[j] += current_num
                can_cut(i+1)
                groups[j] -= current_num

    can_cut(0)
    return result


k, _ = input().strip().split()
k = int(k)
nums = input().strip()
nums = [int(x) for x in nums.split()]
result = cut(nums, k)
if result:
    print_line('YES')
else:
    print_line('NO')
