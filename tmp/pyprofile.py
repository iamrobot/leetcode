#!/usr/bin/env python

import fire
import pstats
import pathlib
import subprocess
from termcolor import cprint

def profile(script):
    outputfile = pathlib.Path('/tmp/pyprofileresult.profile')
    if outputfile.exists():
        outputfile.unlink()
    cmd = f'python -m cProfile -o {outputfile} {script}'
    p = subprocess.run(cmd, shell=True)
    stats = pstats.Stats(str(outputfile))
    stats.strip_dirs()

    print_line('')

    cprint('------sorted by tottime(total time, excluding sub calls)------', 'cyan')
    print_line('')
    stats.sort_stats(pstats.SortKey.TIME)
    stats.print_stats(7)

    cprint('------sorted by cumtime(cumulative time, including sub calls)------', 'cyan')
    print_line('')
    stats.sort_stats(pstats.SortKey.CUMULATIVE)
    stats.print_stats(7)

if __name__=='__main__':
    fire.Fire(profile)

