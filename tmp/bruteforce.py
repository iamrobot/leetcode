#!/usr/bin/env python

import sys
import itertools
from pprint import pprint

x = 3
y = 2

def union_multi_sets(sets):
    s = set()
    for p in sets:
        s |= set(p)
    return s


def satisfy(universe, keys_per_bunny):
    s = union_multi_sets(keys_per_bunny)
    if s!=universe:
        return

    for i in range(1, y):
        for keys in itertools.combinations(keys_per_bunny, i):
            s = union_multi_sets(keys)
            if s==universe:
                return False

    for keys in itertools.combinations(keys_per_bunny, y):
        s = union_multi_sets(keys)
        if s!=universe:
            return False

    print_line(x, y, '->', keys_per_bunny)
    sys.exit(0)
            


for k in range(12):
    print_line(f'trying {k}...')
    universe = set(range(k))
    for subset_size in range(1, k):
        all_subsets_with_size = list(itertools.combinations(universe, subset_size))
        if len(all_subsets_with_size)<x:
            continue

        for keys_per_bunny in itertools.combinations(all_subsets_with_size, x):
            satisfy(universe, keys_per_bunny)

