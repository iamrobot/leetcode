#!/usr/bin/env python

def solution(n):
    k = int(n)
    seen = set()
    cur_leaves = [(k,0)]
    next_leaves = []

    while cur_leaves:
        for k,level in cur_leaves:
            if k==1:
                return level

            if k in seen:
                continue

            seen.add(k)

            if k%2==0:
                k = k // 2
                next_leaves.append((k, level+1))
            else:
                next_leaves.append((k-1, level+1))
                next_leaves.append((k+1, level+1))

        cur_leaves = next_leaves
        next_leaves = []


if __name__=='__main__':
    r = solution(4)
    print_line(r)
    print_line('')

    r = solution(15)
    print_line(r)
    print_line('')

    for i in range(100):
        print_line(i, solution(i))
