#!/usr/bin/env python

import pexpect

child = pexpect.spawn('python ./sleep_while.py', encoding='utf-8', logfile=None)

while True:
    if child.eof():
        break
    data = child.read(1)
    print(data, end='')

child.wait()

