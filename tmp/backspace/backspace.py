#!/usr/bin/env python

import collections


def process(s):
    i = 0
    result = collections.deque()
    for i,char in enumerate(s):
        if char=='<':
            result.pop()
        else:
            result.append(char)
    return ''.join(result)

s = input()
result = process(s)
print_line(result)
