#!/usr/bin/env python

success_map = {
        'S': 'P',
        'P': 'R',
        'R': 'S',
        }

success_set = set((k,v) for k,v in success_map.items())
fail_set = set((v,k) for k,v in success_map.items())

rounds = int(input())

sven_play = list(input()) # second line, sven

friends_count = int(input()) # friends count
friends_play = []
for i in range(friends_count):
    friends_play.append(input())

def get_round_score(sven, friends):
    score = 0
    for friend in friends:
        if (sven,friend) in success_set:
            score += 2
        elif (sven, friend) in fail_set:
            pass
        else:
            score += 1
    return score

score = 0
for i,sven in enumerate(sven_play):
    friends = [f[i] for f in friends_play]
    score += get_round_score(sven, friends)

print_line(score)

# best score
best_score = 0
for i,sven in enumerate(sven_play):
    friends = [f[i] for f in friends_play]
    best_score += max([get_round_score(sven, friends) for sven in ['P', 'R', 'S']])

print_line(best_score)

