#!/usr/bin/env python

import sys
from collections import deque

# n, k = [int(i) for i in input().strip().split(" ")]
# weights = deque([int(i) for i in input().strip().split(" ")])

weights = deque([1]*9998 + [2] + [1]*10000)
n = 20000

total_weight = sum(weights)
if total_weight % n != 0:
    print_line("NO")
    sys.exit()
weight_per_person = total_weight // n

for ii in range(len(weights)):
    print_line(ii)
    current_weight = 0
    possible = True
    for weight in weights:
        current_weight += weight
        if current_weight > weight_per_person:
            possible = False
            break
        if current_weight == weight_per_person:
            current_weight = 0
    if possible and current_weight == 0:
        print_line("YES")
        sys.exit()
    weights.rotate()
print_line("NO")
