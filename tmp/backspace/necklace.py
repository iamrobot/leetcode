#!/usr/bin/env python

import itertools
import collections
import random

def cut(nums, k):
    def cut_from_0():
        s = 0
        for num in nums:
            s += num
            if s==target:
                return True
            if s>target:
                return False

    s = sum(nums)
    if s%k!=0:
        return False
    target = s // k
    if any([num>target for num in nums]):
        return False
    target_set = set([target*i for i in range(1,k+1)])

    nums = collections.deque(nums)
    moved_s = 0
    while moved_s<target:
        if cut_from_0():
            s = set(itertools.accumulate(nums))
            if s.issuperset(target_set):
                return True
        moved = nums.pop()
        moved_s += moved
        nums.appendleft(moved)
    #while moved_s<target:
    #    if cut_from_0():
    #        break
    #    moved = nums.pop()
    #    moved_s += moved
    #    nums.appendleft(moved)
    #s = set(itertools.accumulate(nums))
    #if s.issuperset(target_set):
    #    return True
    return False

def print_result(result):
    if result:
        print_line('YES')
    else:
        print_line('NO')

def get_nums_and_k():
    k, _ = input().strip().split()
    k = int(k)
    nums = input().strip()
    nums = [int(x) for x in nums.split()]
    return nums, k

def random_generate_yes():
    k = random.randint(1, 100)
    target = random.choice(range(1,100))
    def generate_sample(target):
        remain = target
        sample = []
        while remain>0:
            x = random.randint(1, remain)
            sample.append(x)
            remain -= x
        return sample
    result = []
    for i in range(k):
        result.extend(generate_sample(target))
    result = collections.deque(result)
    rotate = random.randint(0, 30)
    for i in range(rotate):
        result.rotate()
    return result, k

def local_test():
    nums = [1, 2, 2, 1,] 
    k = 3
    result = cut(nums, k)
    print_result(result)

    nums = [2, 2, 4, 1,]
    k = 3
    result = cut(nums, k)
    print_result(result)

    nums = [2, 2, 2,2]
    k = 4
    result = cut(nums, k)
    print_result(result)

    nums = collections.deque([1]*9998 + [2] + [1]*10000)
    k = 20000
    result = cut(nums, k)
    print_result(result)

    for i in range(10000):
        nums, k = random_generate_yes()
        print_line(nums, k, sum(nums)//k)
        result = cut(nums, k)
        print_result(result)
        print_line('')
        if result!=True:
            break

def kattis():
    nums,k = get_nums_and_k()
    result = cut(nums, k)
    print_result(result)

local_test()
#kattis()
