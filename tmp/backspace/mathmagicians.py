#!/usr/bin/env python

s = 'RBRBRB'
s2  = 'BRBRBR'

def mapstobin(s):
    s = s.replace('R', '0', -1)
    s = s.replace('B', '1', -1)
    return s

print_line(mapstobin(s))
print_line(mapstobin(s2))

