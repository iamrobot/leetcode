#!/usr/bin/env python

import string

vowels = 'AEIOU'
others_without_L = [letter for letter in string.ascii_uppercase if letter!='L' and letter not in vowels]

def find_possible_fills(surrounds):
    possible_fills = set([-1, 1, 'L'])
    excluded_fills = set()
    for surround in surrounds:
        surround = [1 if e=='L' else e for e in surround]
        surround = [e for e in surround if e!='_']
        if sum(surround)==-2:
            excluded_fills.add(-1)
        if sum(surround)==2:
            excluded_fills.add('L')
            excluded_fills.add(1)
    return possible_fills - excluded_fills

def run(s):
    result = 1
    if '_' not in s:
        if 'L' in s:
            return 1
        else:
            return 0
    empty_index = s.index('_')
    surrounds = [
            s[max(0, empty_index-2):empty_index+1],
            s[max(0, empty_index-1):empty_index+2],
            s[empty_index:empty_index+3],
            ]
    possible_fills = find_possible_fills(surrounds)
    k = 0
    for possible_fill in possible_fills:
        if possible_fill==-1:
            filled = s[:]
            filled[empty_index] = -1
            k += result*len(vowels)*run(filled)
        elif possible_fill=='L':
            filled = s[:]
            filled[empty_index] = 'L'
            k += result*1*run(filled)
        elif possible_fill==1:
            filled = s[:]
            filled[empty_index] = 1
            k += result*len(others_without_L)*run(filled)
    result = k
    return result


s = input().strip()
masked_s = []
for letter in s:
    if letter=='L':
        masked_s.append('L')
        continue
    if letter=='_':
        masked_s.append('_')
        continue

    if letter in vowels:
        masked_s.append(-1)
    else:
        masked_s.append(1)

result = run(masked_s)
print_line(result)
