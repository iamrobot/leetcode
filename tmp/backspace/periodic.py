#!/usr/bin/env python


import itertools

def perioid_generator(subs):
    l = [subs]
    for i in range(len(subs)-1):
        subs = subs[-1]+subs[:-1]
        l.append(subs)
    return itertools.cycle(l)

def process(s):
    for i in range(1, len(s)+1):
        if len(s) % i != 0:
            continue
        n = i
        subs_perioid_generator = perioid_generator(s[:i])
        chunks = [s[j:j+n] for j in range(0, len(s), n)]
        for chunk,should_be_chunk in zip(chunks, subs_perioid_generator):
            if chunk!=should_be_chunk:
                break
        else:
            return i

s = input()
result = process(s)
print_line(result)
