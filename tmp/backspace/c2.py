#!/usr/bin/env python

import sys
import itertools
from collections import deque

n, k = [int(i) for i in input().strip().split(" ")]
weights = deque([int(i) for i in input().strip().split(" ")])

total_weight = sum(weights)
if total_weight % n != 0:
    print_line("NO")
    sys.exit()
weight_per_person = total_weight // n

target_set = set([weight_per_person*i for i in range(1,n+1)])

for ii in range(len(weights)):
    acc_sets = set(itertools.accumulate(weights))
    if acc_sets.issuperset(target_set):
        print_line("YES")
        sys.exit()
    weights.rotate()
print_line("NO")
