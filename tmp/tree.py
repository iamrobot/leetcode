#!/usr/bin/env python

def generate(n):
    width = 2*n-1
    tree = [['*' for _ in range(width)]]
    left = 0
    right = width-1
    for i in range(1,n):
        line = [' ' for _ in range(width)]
        left = left+1
        right = right-1
        line[left] = '*'
        line[right] = '*'
        tree.insert(0, line)

    for line in tree:
        print(''.join(line))

generate(5)
