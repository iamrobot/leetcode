#!/usr/bin/env python

import copy
import math
import time
import random
import itertools
import collections
import logging
from pprint import pprint


def trans_to_1_0(ca):
    return [[int(e) for e in x] for x in ca]


def random_shape(n_rows, n_cols):
    random.seed(time.time())
    n_elements = n_rows * n_cols
    elements = [0] * n_elements
    # too many zeros lead too many possible previous states
    # so we limit how many ones we put in, to test algorithm
    # performance when result is large
    n_ones = random.randrange(0, 3)
    elements_index = list(range(n_elements))
    ones_index = random.sample(elements_index, n_ones)
    for i in ones_index:
        elements[i] = 1

    m = [elements[j:j + n_cols] for j in range(0, n_elements, n_cols)]
    return m


def iter_shape(n_rows, n_cols):
    n_elements = n_rows * n_cols
    for n_ones in range(0, n_elements + 1):
        elements_index = list(range(n_elements))
        for ones_i in itertools.combinations(elements_index, n_ones):
            elements = [0] * n_elements
            for i in ones_i:
                elements[i] = 1

            m = [elements[j:j + n_cols] for j in range(0, n_elements, n_cols)]
            yield Grid.instance(m)


def m_to_vars(m):
    n_rows = len(m)
    n_cols = len(m[0])
    elements = tuple(itertools.chain(*m))
    return n_rows, n_cols, elements


class Prettydefaultdict(collections.defaultdict):
    def __str__(self):
        return dict(self).__str__()

    def __repr__(self):
        return self.__str__()


class Matrix:
    def __init__(self, m):
        #self.m = copy.deepcopy(m) # takes too much time, but it seems it's ok to omit 
                                   # this step here
        self.m = m
        self.shape = (len(self.m), len(self.m[0]))
        self.n_rows = self.shape[0]
        self.n_cols = self.shape[1]
        self.rows = self.m
        self.cols = [self.get_col(j) for j in range(self.n_cols)]

    def flatten(self):
        return list(itertools.chain(*self.m))

    def transpose(self):
        return self.cols

    def dot(self, another):
        self.n_rows, self.n_cols = self.shape
        another.n_rows, another.n_cols = another.shape

        if self.n_cols != another.n_rows:
            raise Exception('only MxN and NxQ matrix can be multiplied')

        result = [[None] * another.n_cols for _ in range(self.n_rows)]
        for i in range(self.n_rows):
            for j in range(another.n_cols):
                row = self.rows[i]
                col = another.cols[j]
                result[i][j] = sum([x * y for x, y in zip(row, col)])

        return Matrix(result)

    def __eq__(self, another):
        return self.shape == another.shape and \
            self.flatten() == another.flatten()

    def __sub__(self, another):
        if self.shape != another.shape:
            raise Exception(
                'Matrix subtraction requires two same shape matrix')

        new_m = Matrix.zeros_like(self)
        for i, j, v_self in self:
            v_another = another.m[i][j]
            new_m.m[i][j] = v_self - v_another
        return new_m

    def __iter__(self):
        '''
        iterate over element, returns i,j,value
        '''
        for i in range(self.n_rows):
            for j in range(self.n_cols):
                yield i, j, self[i][j]

    def __str__(self):
        s = []
        for row in self.rows:
            s.append([str(e) for e in row])
        return pformat(s)

    def __getitem__(self, i):
        return self.rows[i]

    def get_col(self, j):
        return [row[j] for row in self.m]


class Boundary:
    def __init__(self, up, down, left, right):
        self.up = up
        self.down = down
        self.left = left
        self.right = right
        self.data = (up, down, left, right)

    def __eq__(self, other):
        return self.data == other.data

    def __hash__(self):
        return hash(self.data)

    def __str__(self):
        return ' '.join([str(i) for i in self.data])

    def __repr__(self):
        return self.__str__()

    def down_join(self, other):
        return Boundary(
            self.up,
            other.down,
            self.left + other.left[1:],
            self.right + other.right[1:]
        )

    def righ_join(self, other):
        return Boundary(
            self.up + other.up[1:],
            self.down + other.down[1:],
            self.left,
            other.right
        )


class Grid:
    instances = dict()
    vi_map = None
    bits = None

    def __init__(self, m):
        self.m = m
        self.n_rows, self.n_cols, self.elements = m_to_vars(m)

        self.boundaries = Prettydefaultdict(lambda: 0)

        self.up_boundaries_set = set()
        self.down_boundaries_set = set()
        self.left_boundaries_set = set()
        self.right_boundaries_set = set()

        self.up_boundaries_map = collections.defaultdict(set)
        self.down_boundaries_map = collections.defaultdict(set)
        self.left_boundaries_map = collections.defaultdict(set)
        self.right_boundaries_map = collections.defaultdict(set)

    def add_boundary(self, boundary, count):
        self.boundaries[boundary] += count

        self.up_boundaries_set.add(boundary.up)
        self.down_boundaries_set.add(boundary.down)
        self.left_boundaries_set.add(boundary.left)
        self.right_boundaries_set.add(boundary.right)

        self.up_boundaries_map[boundary.up].add(boundary)
        self.down_boundaries_map[boundary.down].add(boundary)
        self.left_boundaries_map[boundary.left].add(boundary)
        self.right_boundaries_map[boundary.right].add(boundary)

    def step(self):
        m = self.m
        n_rows = self.n_rows
        n_cols = self.n_cols
        next_n_rows = n_rows - 1
        next_n_cols = n_cols - 1
        next_m = [[0] * next_n_cols for _ in range(next_n_rows)]
        for i in range(next_n_rows):
            for j in range(next_n_cols):
                four_elements = m[i][j], m[i][j +
                                              1], m[i + 1][j], m[i + 1][j + 1]
                if sum(four_elements) == 1:
                    next_m[i][j] = 1

        return Grid.instance(next_m)

    def right_join(self, other):
        # takes too much time when intersection is too large(1024)
        # using matrix multiplication can simplify the process
        # so if matrix multiplication can be simplified, what about this?

        joined_ca = Grid.instance([[]])

        for ib in self.right_boundaries_set.intersection(
                other.left_boundaries_set):
            for b1 in self.right_boundaries_map[ib]:
                c1 = self.boundaries[b1]
                for b2 in other.left_boundaries_map[ib]:
                    c2 = other.boundaries[b2]

                    b_joined = b1.righ_join(b2)
                    c_joined = c1 * c2
                    joined_ca.add_boundary(b_joined, c_joined)
        return joined_ca

    def down_join(self, other):
        joined_m = self.m + other.m
        joined_ca = Grid.instance(joined_m)
        for ib in self.down_boundaries_set.intersection(
                other.up_boundaries_set):
            for b1 in self.down_boundaries_map[ib]:
                c1 = self.boundaries[b1]
                for b2 in other.up_boundaries_map[ib]:
                    c2 = other.boundaries[b2]

                    b_joined = b1.down_join(b2)
                    c_joined = c1 * c2
                    joined_ca.add_boundary(b_joined, c_joined)

        return joined_ca

    @property
    def up(self):
        return tuple(self.m[0])

    @property
    def down(self):
        return tuple(self.m[-1])

    @property
    def left(self):
        return tuple([row[0] for row in self.m])

    @property
    def right(self):
        return tuple([row[-1] for row in self.m])

    def h_split(self):
        mid = self.n_rows // 2
        up_m = self.m[:mid]
        down_m = self.m[mid:]
        return Grid.instance(up_m), Grid.instance(down_m)

    def v_split(self):
        # split to cols, not half
        cols_ca = []
        for j in range(self.n_cols):
            col = [[row[j], ] for row in self.m]
            col_ca = Grid.instance(col)
            cols_ca.append(col_ca)
        return cols_ca

    def l2r_map_matrix(self):
        if self.n_cols > 1:
            raise Exception('only col CA can be converted to matrix')
        bits = Grid.bits
        matrix = [[0] * (2**bits) for _ in range(2**bits)]
        vi_map = Grid.vi_map
        for boundary in self.boundaries:
            i = vi_map[boundary.left]
            j = vi_map[boundary.right]
            matrix[i][j] = 1

        return matrix

    def __str__(self):
        return '\n'.join([str(row) for row in self.m]) + '\n'

    def __repr__(self):
        return '-'.join([str(row) for row in self.m])

    def __eq__(self, other):
        return all([
            self.n_cols == other.n_cols,
            self.n_rows == other.n_rows,
            self.elements == other.elements,
        ])

    def __hash__(self):
        return hash((self.n_rows, self.n_cols, self.elements))

    @staticmethod
    def instance(m):
        if m == [[]]:
            return Grid([[]])

        key = m_to_vars(m)
        if key in Grid.instances:
            instance = Grid.instances[key]
        else:
            instance = Grid(m)
            Grid.instances[key] = instance
        return instance

    @staticmethod
    def build_vector_to_int_map(bits):
        def binary_string_to_tuple(bs):
            return tuple([int(x) for x in list(bs)])
        Grid.bits = bits

        vectors = [bin(i)[2:].zfill(bits) for i in range(0, 2**bits)]
        vectors = [binary_string_to_tuple(bs) for bs in vectors]
        indexes = range(2**bits)
        vi_map = dict(zip(vectors, indexes))
        Grid.vi_map = vi_map


def split_and_rejoin(ca):
    if len(ca.boundaries.keys()) > 0:
        return ca

    if ca.n_cols > 1:
        cols_ca = ca.v_split()
        cols_ca = [split_and_rejoin(x) for x in cols_ca]

        m_l = [col_ca.l2r_map_matrix() for col_ca in cols_ca]
        m0 = Matrix(m_l[0])
        m0 = [
            [sum(m0.get_col(i)) for i in range(m0.n_cols)],
        ]
        result_m = m0

        def mm(m1, m2):
            m1 = Matrix(m1)
            m2 = Matrix(m2)
            return m1.dot(m2).m
        for i in range(1, len(m_l)):
            result_m = mm(result_m, m_l[i])
        return sum(result_m[0])

    elif ca.n_rows > 1:
        up_ca, down_ca = ca.h_split()
        up_ca = split_and_rejoin(up_ca)
        down_ca = split_and_rejoin(down_ca)
        return up_ca.down_join(down_ca)
    else:
        return ca


def solution(g):

    Grid.instances = dict()

    # cache 1-by-1 matrix
    for ca in iter_shape(2, 2):
        next_ca = ca.step()  # 1 by 1
        b = Boundary(ca.up, ca.down, ca.left, ca.right)
        next_ca.add_boundary(b, 1)

    g = trans_to_1_0(g)
    target_ca = Grid.instance(g)

    # samller memory footprint using int as dict keys
    Grid.build_vector_to_int_map(target_ca.n_rows + 1)

    return split_and_rejoin(target_ca)


if __name__ == '__main__':
    import sys

    g = [[True, False, True], [False, True, False], [True, False, True]]
    r = solution(g)
    print_line(r)

    g = [
        [
            True, True, False, True, False, True, False, True, True, False], [
            True, True, False, False, False, False, True, True, True, False], [
                True, True, False, False, False, False, False, False, False, True], [
                    False, True, False, False, False, False, True, True, False, False]]
    r = solution(g)
    print_line(r)

    g = [
        [
            True, False, True, False, False, True, True, True], [
            True, False, True, False, False, False, True, False], [
                True, True, True, False, False, False, True, False], [
                    True, False, True, False, False, False, True, False], [
                        True, False, True, False, False, True, True, True]]
    r = solution(g)
    print_line(r)

    g = random_shape(9, 50)
    print_line(g)
    r = solution(g)
    print_line(r)
