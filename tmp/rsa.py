#!/usr/bin/env python

import random

def gen_primes():
    primes = [2]
    yield 2
    i = 3
    while True:
        for prime in primes:
            if i%prime==0:
                break
        else:
            primes.append(i)
            yield i

        i += 1

primes = []
for i,p in enumerate(gen_primes()):
    primes.append(p)
    if i>200:
        break

def coprime(fi):
    # simply choose a prime less than fi works
    primes_less_than_fi = [p for p in primes if p<fi]
    return random.choice(primes_less_than_fi)


p,q = random.sample(primes, 2)
# n
n = p*q
# fi
fi = (p-1)*(q-1)
# e, coprime with fi
e = coprime(fi)
# d, d*e % fi == 1
for d in range(2, fi):
    if (d*e)%fi==1:
        break

print_line(f'(e,n): {e} {n}')
print_line(f'(d,n): {d} {n}')
plain = random.randint(1, n)
encrypted = (plain**e)%n
decrypted = (encrypted**d)%n
print_line(plain, encrypted, decrypted)
