#!/usr/bin/env python

import random

def gen_primes():
    primes = [2]
    yield 2
    i = 3
    while True:
        for prime in primes:
            if i%prime==0:
                break
        else:
            primes.append(i)
            yield i

        i += 1

primes = []
for i,p in enumerate(gen_primes()):
    primes.append(p)
    if i>200:
        break

def coprime(fi):
    # simply choose a prime less than fi works
    primes_less_than_fi = [p for p in primes if p<fi]
    return random.choice(primes_less_than_fi)

def gcd(a,b):
    while b != 0:
        a, b = b, a % b
    return a

def primRoots(modulo):
    roots = []
    required_set = set(num for num in range (1, modulo) if gcd(num, modulo) == 1)

    for g in range(1, modulo):
        actual_set = set(pow(g, powers) % modulo for powers in range (1, modulo))
        if required_set == actual_set:
            roots.append(g)
    return roots

if __name__ == "__main__":
    p = 17
    primitive_roots = primRoots(p)
    print_line(primitive_roots)
