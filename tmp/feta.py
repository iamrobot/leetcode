#!/usr/bin/env python

def topological_sort(graph):
    def dfs(i):
        if i in visited:
            return
        visited.add(i)
        for neighbor in adjs[i]:
            dfs(neighbor)
        l.append(i)

    l = []
    n = len(graph)
    visited = set()
    for i in range(n):
        dfs(i)

    l.reverse()
    print_line(l)

adjs = [
        [],
        [0],
        [0],
        ]
topological_sort(adjs)
