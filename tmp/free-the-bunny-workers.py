#!/usr/bin/env python

import math
import itertools
import collections


def n_choose_k(n, k):
    f = math.factorial
    return f(n) // f(k) // f(n - k)


def helper(num_buns, num_required):
    n_keys = n_choose_k(num_buns, num_required - 1)
    key_copies = num_buns - (num_required - 1)
    keys_per_bunny = n_keys * key_copies // num_buns

    bunnies = list(range(num_buns))
    missing_key_d = collections.defaultdict(set)

    for i, just_require_one_more in enumerate(itertools.combinations(bunnies, num_required-1)):
        for bunny in just_require_one_more:
            missing_key_d[bunny].add(n_keys - 1 - i)

    all_keys = set(range(n_keys))
    bunny_keys = [sorted(list(all_keys-missing_key_d[i])) for i in range(num_buns)]
    return bunny_keys


def solution(num_buns, num_required):
    r = helper(num_buns, num_required)
    return r


if __name__ == '__main__':
    r = solution(5, 3)
    print_line(r)
    assert r == [
        [0, 1, 2, 3, 4, 5],
        [0, 1, 2, 6, 7, 8],
        [0, 3, 4, 6, 7, 9],
        [1, 3, 5, 6, 8, 9],
        [2, 4, 5, 7, 8, 9]]

    r = solution(2, 1)
    print_line(r)
    assert r == [[0], [0]]

    r = solution(4, 4)
    print_line(r)
    assert r == [[0], [1], [2], [3]]
