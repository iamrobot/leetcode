#!/usr/bin/env python

import time
import copy

class Situation:
    times = None

    def __init__(self, pos, taken_bunnies, time_left):
        self.pos = pos
        self.taken_bunnies = taken_bunnies
        self.time_left = time_left

        nrows = len(Situation.times)
        self.bunnies = range(1, nrows-1)

    def __le__(self, other):
        return all([
                self.pos == other.pos,
                self.taken_bunnies <= other.taken_bunnies,
                self.time_left <= other.time_left,
                ])

    def __str__(self):
        #return f'{self.pos} {self.taken_bunnies} {self.time_left}\n'
        return ''

    def __repr__(self):
        return self.__str__()

    def to(self, next_pos):
        cur_pos = self.pos

        taken_bunnies = copy.deepcopy(self.taken_bunnies)
        if next_pos in self.bunnies:
            taken_bunnies.add(next_pos)

        time_left = self.time_left - Situation.times[cur_pos][next_pos]

        return Situation(next_pos, taken_bunnies, time_left)


def detect_negative_cycle_by_brute_force(m):
    # just brute force
    import itertools
    nodes = list(range(len(m)))
    n_nodes = len(nodes)
    for k in range(2, n_nodes+1):
        for p in itertools.permutations(nodes, k):
            cycle = list(p)
            cycle.append(cycle[0])
            cycle_sum = sum([m[u][v] for u,v in zip(cycle[:-1], cycle[1:])])
            if cycle_sum<0:
                return True
    return False


def detect_negative_cycle_by_bellman_ford(m):
    import math
    nodes = list(range(len(m)))
    n_nodes = len(nodes)
    dist = [float('inf') for _ in range(n_nodes)]
    dist[0] = 0
    for _ in range(n_nodes**2):
        for u in nodes:
            for v in nodes:
                edge = m[u][v]
                d = dist[u] + edge 
                if  d < dist[v]:
                    dist[v] = d

    for u in nodes:
        for v in nodes:
            edge = m[u][v]
            d = dist[u] + edge 
            if  d < dist[v]:
                return True

    return False



def helper(times, times_limit):
    Situation.times = times

    nrows = len(times)
    if nrows<=2:
        return []

    bunnies = range(1, nrows-1)
    best_situation = Situation(nrows-1, set(bunnies), 0)

    #if detect_negative_cycle(times):
    if detect_negative_cycle_by_bellman_ford(times):
        return sorted([x-1 for x in bunnies])

    seen = []
    not_seen = [Situation(0, set(), times_limit),]

    while not_seen:
        cur_situation = not_seen.pop(0)
        if any([cur_situation<=s for s in seen]):
            continue

        seen = [s for s in seen if not s<=cur_situation]

        seen.append(cur_situation)

        for next_pos in range(nrows):
            next_situation = cur_situation.to(next_pos)
            if best_situation <= next_situation:
                return sorted([x-1 for x in bunnies])
            not_seen.append(next_situation)

    exit_situations = [s for s in seen if s.pos==nrows-1 and s.time_left>=0]

    if exit_situations == []:
        return []

    most_taken_bunnies = max([len(s.taken_bunnies) for s in exit_situations])
    exit_situations_with_most_bunnies = [s for s in exit_situations if len(s.taken_bunnies)==most_taken_bunnies]

    result = sorted(exit_situations_with_most_bunnies, key=lambda s:sum(s.taken_bunnies))[0]
    
    return sorted([x-1 for x in result.taken_bunnies])


def solution(times, times_limit):
    # time.sleep(10**8) # timeout also shows failed, so we don't know whether it's wrong answer or timed-out.
    # can the algorithm time out when bunnies are at most 5? Note that it does take pretty long to verify.
    # any improvement possible?

    # detect negative cycle should be an improvement since if negative cicle exists, then all bunnies can be
    # saved no matter what.

    # hmm, it does work. next possible improvement: use bellman-ford algorithm instead of brute-force to detect 
    # negative cycle

    r = helper(times, times_limit)
    return r

if __name__=='__main__':
    r = solution([
        [0, 2, 2, 2, -1], 
        [9, 0, 2, 2, -1], 
        [9, 3, 0, 2, -1], 
        [9, 3, 2, 0, -1], 
        [9, 3, 2, 2, 0]], 

        1)
    # [1, 2]
    print_line(r)

    r = solution([
        [0, 1, 1, 1, 1], 
        [1, 0, 1, 1, 1], 
        [1, 1, 0, 1, 1], 
        [1, 1, 1, 0, 1], 
        [1, 1, 1, 1, 0]], 

        3)
    # [0, 1]
    print_line(r)

    # this might be the problem
    r = solution([
        [0, 9999, 1, 1, 1], 
        [1, 0, 1, 1, 1], 
        [1, 9999, 0, 1, -1], 
        [1, 9999, 1, 0, 1], 
        [1, 9999, -1, 1, 0]], 

        0)
    # [0, 1]
    print_line(r)
