#!/usr/bin/env bash

remoteworkingdir="/tmp/tmprun/"
ssh kali mkdir -p $remoteworkingdir
scp -r *.py kali:$remoteworkingdir
ssh kali /usr/bin/python2 "$remoteworkingdir/t.py"
