#!/usr/bin/env python

import copy
import math
import fractions
import itertools
from pprint import pprint, pformat


def fix_transition_matrix(m):
    transient_states = []
    absorb_states = []

    # find transient_states and absorb_states
    for i,row in enumerate(m):
        s = sum(row)
        if s == 0:
            absorb_states.append(i)
        else:
            transient_states.append(i)

    # turn all elements to fractions
    for i,row in enumerate(m):
        for j,e in enumerate(row):
            m[i][j] = fractions.Fraction(e)

    # normalize probabilities
    for i,row in enumerate(m):
        s = sum(row)
        if s == 0:
            m[i][i] = fractions.Fraction(1)
        else:
            for j,e in enumerate(row):
                m[i][j] = e / s
            
    return transient_states, absorb_states

def get_Q_R(transient_states=[], absorb_states=[], m=[]):
    # trans to QR form
    Q = Matrix.zeros((len(transient_states), len(transient_states)))
    R = Matrix.zeros((len(transient_states), len(absorb_states)))
    for i_q,i in enumerate(transient_states):
        for j_q,j in enumerate(transient_states):
            Q[i_q][j_q] = m[i][j]
    for i_r,i in enumerate(transient_states):
        for j_r,j in enumerate(absorb_states):
            R[i_r][j_r] = m[i][j]

    return Q, R

    
def gcd(x, y):
    while y:
        x, y = y, x % y
    return x


def lcm(dl):
    '''
    dl: list of integers(I only considered positive integers)
    '''
    result = dl[0]
    for d in dl[1:]:
        _gcd = gcd(result, d)
        result = result*d//_gcd
    return result 


class Matrix:
    def __init__(self, m):
        self.m = copy.deepcopy(m)
        self.rows = self.m
        self.shape = (len(self.m), len(self.m[0]))
        self.nrows = self.shape[0]
        self.ncols = self.shape[1]
            
    def flatten(self):
        return list(itertools.chain(*self.m))

    @property
    def cols(self):
        _cols = []
        for j in range(self.ncols):
            col = [row[j] for row in self.rows]
            _cols.append(col)
        return _cols


    def transpose(self):
        return Matrix(cols)


    def row_div(self, row_i, d):
        row = self[row_i]
        for i in range(self.ncols):
            row[i] /= d


    def row_minus_row(self, row0_i, row1_i, mul):
        row0 = self[row0_i]
        row1 = self[row1_i]
        for i in range(self.ncols):
            row0[i] -= row1[i]*mul


    def inv(self):
        '''
        most basic matrix inverse algorithm using gauss elimination
        Assume square matrix, and inverse-able
        '''

        I = Matrix.eye(self.nrows)
        for j in range(self.ncols):
            v0 = self[j][j]
            for i in range(j+1, self.nrows):
                v1 = self[i][j]
                mul = v1 / v0
                self.row_minus_row(i, j, mul)
                I.row_minus_row(i, j, mul)
        
        for j in range(self.ncols-1, 0, -1):
            v0 = self[j][j]
            for i in range(j-1, -1, -1):
                v1 = self[i][j]
                mul = v1 / v0
                self.row_minus_row(i, j, mul)
                I.row_minus_row(i, j, mul)

        for i in range(self.nrows):
            v = self[i][i]
            self.row_div(i, v)
            I.row_div(i, v)

        return I
                

    def dot(self, another):
        self.nrows, self.ncols = self.shape
        another.nrows, another.ncols = another.shape

        if self.ncols!=another.nrows:
            raise Exception('only MxN and NxQ matrix can be multiplied')

        result = [[None]*another.ncols for _ in range(self.nrows)]
        for i in range(self.nrows):
            for j in range(another.ncols):
                row = self.rows[i]
                col = another.cols[j]
                result[i][j] = sum([x*y for x,y in zip(row,col)])

        return Matrix(result)


    def __eq__(self, another):
        return self.shape==another.shape and \
                self.flatten()==another.flatten()


    def __sub__(self, another):
        if self.shape != another.shape:
            raise Exception('Matrix subtraction requires two same shape matrix')

        new_m = Matrix.zeros_like(self)
        for i,j,v_self in self:
            v_another = another.m[i][j]
            new_m.m[i][j] = v_self - v_another
        return new_m


    def __iter__(self):
        '''
        iterate over element, returns i,j,value
        '''
        for i in range(self.nrows):
            for j in range(self.ncols):
                yield i, j, self[i][j]


    def __str__(self):
        s = []
        for row in self.rows:
            s.append([str(e) for e in row])
        return pformat(s)


    def __getitem__(self, i):
        return self.rows[i]


    def lu_decompose(self):
        '''
        Assume square matrix, and no zero on diagonal,
        '''
        lower = Matrix.eye(self.nrows)
        upper = Matrix(self.m)
        for j in range(self.ncols-1):
            v0 = self[j][j]
            for i in range(j+1, self.nrows):
                v1 = self[i][j]
                lv = v1 / v0
                lower[i][j] = lv
                for k in range(self.ncols):
                    upper[i][k] -= upper[j][k]*lv

        return lower, upper


    @staticmethod
    def zeros(shape):
        nrows, ncols = shape
        m = [[0]*ncols for _ in range(nrows)]
        return Matrix(m)

    @staticmethod
    def zeros_like(another):
        return Matrix.zeros(another.shape)

    @staticmethod
    def ones(shape):
        nrows, ncols = shape
        m = [[1]*ncols for _ in range(nrows)]
        return Matrix(m)

    @staticmethod
    def eye(dim):
        if not isinstance(dim, int):
            raise Exception('eye only support integer as dim')
        shape = (dim, dim)
        matrix = Matrix.zeros(shape)
        for i,row in enumerate(matrix.m):
            row[i] = fractions.Fraction(1)
        return matrix


def solution(m):
    transient_states, absorb_states = fix_transition_matrix(m)

    if 0 in absorb_states:
        return [1]+[0]*(len(absorb_states)-1)+[1]

    Q, R = get_Q_R(transient_states, absorb_states, m)

    I = Matrix.eye(Q.nrows)
    N = I - Q
    B = N.inv().dot(R)
    absorb_p_from_0 = B[0]

    common_denominator = lcm([x.denominator for x in absorb_p_from_0])

    result = [x.numerator*(common_denominator//x.denominator) for x in absorb_p_from_0]

    result.append(common_denominator)

    return result

if __name__=='__main__':
    m = [
      [0],
    ]
    r = solution(m)
    print_line(r)

    m = [
      [0, 0, 0],
      [0, 0, 0],
      [0, 0, 0],
    ]
    r = solution(m)
    print_line(r)

    m = [
      [0, 0, 0, 0, 0],
      [6, 0, 4, 0, 0],
      [0, 6, 0, 4, 0],
      [0, 0, 6, 0, 4],
      [0, 0, 0, 0, 0],
    ]
    r = solution(m)
    print_line(r)

    m = [
      [0,1,0,0,0,1],  # s0, the initial state, goes to s1 and s5 with equal probability
      [4,0,0,3,2,0],  # s1 can become s0, s3, or s4, but with different probabilities
      [0,0,0,0,0,0],  # s2 is terminal, and unreachable (never observed in practice)
      [0,0,0,0,0,0],  # s3 is terminal
      [0,0,0,0,0,0],  # s4 is terminal
      [0,0,0,0,0,0],  # s5 is terminal
    ]
    r = solution(m)
    print_line(r)

    m = [[0, 2, 1, 0, 0], [0, 0, 0, 3, 4], [0, 0, 0, 0, 0], [0, 0, 0, 0,0], [0, 0, 0, 0, 0]]
    r = solution(m)
    print_line(r)
