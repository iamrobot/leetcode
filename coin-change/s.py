
# load local leetcode lib
import pathlib
lc_lib_path = pathlib.Path('../lc_lib.py')
if lc_lib_path.exists():
    import sys
    sys.path.insert(0, '..')
    from lc_lib import *

old_print = print
def print(*args, **kwargs):
    if lc_lib_path.exists():
        old_print(*args, **kwargs)

import functools
class Solution:
    def coinChange(self, coins: List[int], amount: int) -> int:
        inf = float('inf')
        l = [inf for _ in range(amount+1)]
        l[0] = 0
        for i in range(amount+1):
            if l[i]<inf:
                for coin in coins:
                    if i+coin<=amount:
                        l[i+coin] = min(l[i+coin], l[i]+1)
        if l[-1]==inf:
            return -1
        return l[-1]


if __name__=='__main__':
    if lc_lib_path.exists():
        sol = Solution()

        coins = [1,2,5]
        amount = 11
        r = sol.coinChange(coins,amount)
        print(r)
        print('-'*30)

        coins = [2]
        amount = 3
        r = sol.coinChange(coins,amount)
        print(r)
        print('-'*30)

        coins = [186,419,83,408]
        amount = 6249
        r = sol.coinChange(coins,amount)
        print(r)
        print('-'*30)

        coins = [125,146,125,252,226,25,24,308,50]
        amount = 8402
        r = sol.coinChange(coins,amount)
        print(r)
        print('-'*30)

