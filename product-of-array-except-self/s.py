
# load local leetcode lib
import pathlib

lc_lib_path = pathlib.Path('../lc_lib.py')
if lc_lib_path.exists():
    import sys
    sys.path.insert(0, '..')
    from lc_lib import *

class Solution:
    def productExceptSelf(self, nums: List[int]) -> List[int]:
        import itertools
        import operator
        import functools
        zeros = [num for num in nums if num==0]
        if len(zeros)>1:
            return [0]*len(nums)

        product = functools.reduce(operator.mul, nums)
        non_zeros = [num for num in nums if num!=0]
        non_zero_product = functools.reduce(operator.mul, non_zeros)

        result = []
        for num in nums:
            if num==0:
                result.append(non_zero_product)
            else:
                result.append(product // num)

        return result

        
if __name__=='__main__':
    if lc_lib_path.exists():
        s = Solution()
        nums = [1,2,3,4]
        r = s.productExceptSelf(nums)
        print(r)
