
# load local leetcode lib
import pathlib
lc_lib_path = pathlib.Path('../lc_lib.py')
if lc_lib_path.exists():
    import sys
    sys.path.insert(0, '..')
    from lc_lib import *

old_print = print
def print(*args, **kwargs):
    if lc_lib_path.exists():
        old_print(*args, **kwargs)

"""
# Definition for a Node.
class Node:
    def __init__(self, val: int = 0, left: 'Node' = None, right: 'Node' = None, next: 'Node' = None):
        self.val = val
        self.left = left
        self.right = right
        self.next = next
"""

class Solution:
    def connect(self, root: 'Node') -> 'Node':
        def dfs(root):
            if root and root.left:
                left_root = root.left
                right_root = root.right
                while left_root:
                    left_root.next = right_root
                    left_root = left_root.right
                    right_root = right_root.left
                dfs(root.left)
                dfs(root.right)

        dfs(root)
        return root

if __name__=='__main__':
    if lc_lib_path.exists():
        sol = Solution()

        k = sum([2**i for i in range(5)])
        root = BinaryTree.from_list(list(range(k)))
        r = sol.connect(root)
        print(r)
        print('-'*30)

