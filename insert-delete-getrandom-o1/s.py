
# load local leetcode lib
import pathlib
lc_lib_path = pathlib.Path('../lc_lib.py')
if lc_lib_path.exists():
    import sys
    sys.path.insert(0, '..')
    from lc_lib import *

old_print = print
def print(*args, **kwargs):
    if lc_lib_path.exists():
        old_print(*args, **kwargs)

import random
class RandomizedSet:

    def __init__(self):
        """
        Initialize your data structure here.
        """
        self.nums = []
        self.pos = {}
        

    def insert(self, val: int) -> bool:
        """
        Inserts a value to the set. Returns true if the set did not already contain the specified element.
        """
        if val in self.pos:
            return False
        else:
            self.nums.append(val)
            self.pos[val] = len(self.nums)-1
            return True
        

    def remove(self, val: int) -> bool:
        """
        Removes a value from the set. Returns true if the set contained the specified element.
        """
        if val in self.pos:
            val_idx = self.pos[val]
            last_val = self.nums[-1]
            self.nums[val_idx] = last_val
            self.pos[last_val] = val_idx
            self.nums.pop(-1)
            self.pos.pop(val)
            return True
        else:
            return False
        

    def getRandom(self) -> int:
        """
        Get a random element from the set.
        """
        idx = random.randrange(0, len(self.nums))
        return self.nums[idx]


# Your RandomizedSet object will be instantiated and called as such:
# obj = RandomizedSet()
# param_1 = obj.insert(val)
# param_2 = obj.remove(val)
# param_3 = obj.getRandom()
        pass

if __name__=='__main__':
    if lc_lib_path.exists():
        sol = RandomizedSet()
        for i in range(1,10):
            sol.insert(i)
        sol.remove(7)
        sol.getRandom()
        print(sol.getRandom())
