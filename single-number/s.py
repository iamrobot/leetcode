
# Definition for singly-linked list.
class ListNode:
    def __init__(self, x):
        self.val = x
        self.next = None

# Definition for a binary tree node.
class TreeNode:
    def __init__(self, val=0, left=None, right=None):
        self.val = val
        self.left = left
        self.right = right

from typing import *


class Solution:
    def singleNumber(self, nums: List[int]) -> int:
        pass


if __name__=='__main__':
    s = Solution()
