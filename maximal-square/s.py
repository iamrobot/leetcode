
# load local leetcode lib
import pathlib
lc_lib_path = pathlib.Path('../lc_lib.py')
if lc_lib_path.exists():
    import sys
    sys.path.insert(0, '..')
    from lc_lib import *

old_print = print
def print(*args, **kwargs):
    if lc_lib_path.exists():
        old_print(*args, **kwargs)

from copy import copy
from itertools import chain
class Solution:
    def maximalSquare(self, matrix: List[List[str]]) -> int:
        m = len(matrix)
        n = len(matrix[0])
        for i in range(m):
            for j in range(n):
                matrix[i][j] = int(matrix[i][j])

        if sum(chain(*matrix))==0:
            return 0

        mn_min = min(m,n)
        max_l = 1
        for k in range(1, mn_min):
            found = False
            for i in range(m-k):
                for j in range(n-k):
                    if not all([
                        matrix[i][j],
                        matrix[i+1][j],
                        matrix[i][j+1],
                        matrix[i+1][j+1]
                       ]):
                        matrix[i][j] = 0 
                    else:
                        found = True
            if found: 
                max_l = k+1
            else:
                break
        
        return (max_l)**2

if __name__=='__main__':
    if lc_lib_path.exists():
        s = Solution()

        matrix = [["1","0","1","0","0"],["1","0","1","1","1"],["1","1","1","1","1"],["1","0","0","1","0"]]
        r = s.maximalSquare(matrix)
        print(r)
        print('-'*30)

        matrix = [['0']]
        r = s.maximalSquare(matrix)
        print(r)
        print('-'*30)

        matrix = [['1']]
        r = s.maximalSquare(matrix)
        print(r)
        print('-'*30)

        matrix = [[1,1,1],[1,1,1],[1,1,1]]
        r = s.maximalSquare(matrix)
        print(r)
        print('-'*30)

