
# load local leetcode lib
import pathlib

lc_lib_path = pathlib.Path('../lc_lib.py')
if lc_lib_path.exists():
    import sys
    sys.path.insert(0, '..')
    from lc_lib import *

class Solution:
    def maxProfit(self, prices: List[int]) -> int:
        import math
        def f(prices):
            if len(prices)==0:
                return 0, 0, 0
            if len(prices)==1:
                x = prices[0]
                return x, x, 0
            if len(prices)==2:
                x1, x2 = prices[0], prices[1]
                return min(x1, x2), max(x1, x2), max(0, x2-x1)

            _min = math.inf
            _max = -math.inf
            for price in prices:
                if price<_min:
                    _min = price
                if price>_max:
                    _max = price
            length = len(prices)
            left_min, _, left_max_profit = f(prices[:length//2])
            _, right_max, right_max_profit = f(prices[length//2:])
            return _min, _max, max(left_max_profit, right_max_profit, right_max-left_min, 0)

        _, _, max_profit = f(prices)
        return max(0, max_profit)

if __name__=='__main__':
    if lc_lib_path.exists():
        s = Solution()
        prices = [7,1,5,3,6,4]
        r = s.maxProfit(prices)
        print(r)
        prices = []
        r = s.maxProfit(prices)
        print(r)
