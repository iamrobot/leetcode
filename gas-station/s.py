
# load local leetcode lib
import pathlib
lc_lib_path = pathlib.Path('../lc_lib.py')
if lc_lib_path.exists():
    import sys
    sys.path.insert(0, '..')
    from lc_lib import *

old_print = print_line
def print(*args, **kwargs):
    if lc_lib_path.exists():
        old_print(*args, **kwargs)

from itertools import accumulate, cycle
from sortedcontainers import SortedList
class Solution:
    def canCompleteCircuit(self, gas: List[int], cost: List[int]) -> int:
        n = len(gas)
        remain = [i-j for i,j in zip(gas, cost)]
        print(remain)
        acc_sums = list(accumulate(remain+remain))
        print(acc_sums)
        cur_window = SortedList()
        prev_gas = 0
        for i in range(n):
            if i==0:
                cur_window.update(acc_sums[:n])
            else:
                cur_window.remove(acc_sums[i-1])
                cur_window.add(acc_sums[i+n])
            if cur_window[0]-prev_gas>=0:
                return i
            if i>0:
                prev_gas = acc_sums[i]

        return -1

if __name__=='__main__':
    if lc_lib_path.exists():
        s = Solution()

        gas = [1,2,3,4,5]
        cost = [3,4,5,1,2]
        r = s.canCompleteCircuit(gas,cost)
        print(r)
        print('-'*30)

        gas = [3,3,4]
        cost = [3,4,4]
        r = s.canCompleteCircuit(gas,cost)
        print(r)
        print('-'*30)

