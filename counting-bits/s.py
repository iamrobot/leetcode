
# load local leetcode lib
import pathlib

lc_lib_path = pathlib.Path('../lc_lib.py')
if lc_lib_path.exists():
    import sys
    sys.path.insert(0, '..')
    from lc_lib import *

class Solution:
    def countBits(self, num: int) -> List[int]:
        results = [0]
        end = 2**0
        for i in range(1,num+1):
            if i==end:
                results.append(1)
                end = end*2
            else:
                results.append(results[i-end]+1)

        return results

if __name__=='__main__':
    if lc_lib_path.exists():
        s = Solution()
        x = 16
        r = s.countBits(x)
        print(list(range(x+1)))
        print(r)
        x = 5
        r = s.countBits(x)
        print(list(range(x+1)))
        print(r)
